"""Test the different path finders."""

import cv2

from idp13.detect import contours
from idp13.strategy import (
    sweep_path,
    minimum_path as mp,
    test_pathfinder as pf
)

frame = cv2.imread('../../images/datasets/3/2.png')
frame = cv2.flip(frame, 1)
left_i = int(0.22 * frame.shape[1])
right_i = int(0.09 * frame.shape[1])
frame = frame[:, left_i:-right_i]

targets, paths = pf.test_find_next_target_point()
length = 0
frame = contours.CircleContours().draw(frame, targets)
for path in paths:
    length += path.length
    frame = cv2.line(
        frame, (int(path.p1.x), int(path.p1.y)),
        (int(path.p2.x), int(path.p2.y)), (0, 255, 0), 2
    )
print('Total pf length: {}'.format(length))

ar = pf.Arena()
ar.untested_cells = targets
t = sweep_path.SmartSweepPath(ar, pf.Robot())
length = 0
for path in t.paths:
    frame = cv2.line(
        frame, (int(path.p1.x), int(path.p1.y)),
        (int(path.p2.x), int(path.p2.y)), (0, 0, 255), 3
    )
    length += path.length
print('Total sweep length: {}'.format(length))

path = mp.MinimumPath(ar, pf.Robot()).path
length = 0
for e in path.elements:
    length += e.length
    frame = cv2.line(
        frame, (int(e.p1.x), int(e.p1.y)),
        (int(e.p2.x), int(e.p2.y)), (255, 0, 0), 2
    )
print('Total length: {}'.format(length))

frame = cv2.resize(
    frame, (int(frame.shape[1] / 2), int(frame.shape[0] / 2))
)
frame = cv2.flip(frame, 1)
cv2.putText(
    frame, '10', (10, frame.shape[0] - 10),
    cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2
)
cv2.imshow('f', frame)
cv2.waitKey(0)
