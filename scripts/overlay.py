"""Create the overlay for a video."""

import datetime

import cv2

from idp13.detect import (
    arena,
    contours,
    robot,
    vectors as vec,
)
from idp13.strategy import minimum_path as mp


video = '../../videos/run1.mp4'
real_time = 100

vc = cv2.VideoCapture(video)
vc.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
vc.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
n_frames = int(vc.get(cv2.CAP_PROP_FRAME_COUNT))
secs_per_frame = real_time / n_frames


def get_frame():
    """Get the next frame."""
    frame = vc.read()[1]
    if frame is not None:
        frame = cv2.flip(frame, 1)
        left_i = int(0.22 * frame.shape[1])
        right_i = int(0.09 * frame.shape[1])
        bottom_i = int(0.09 * frame.shape[0])
        return frame[:-bottom_i, left_i:-right_i]
        # return frame[:, left_i:-right_i]


def get_robot(img):
    """Get a Robot from an image."""
    return robot.Robot().find(img)


def draw_arena(img, ar):
    """Draw an arena on an image."""
    img = contours.CircleContours().draw(
        img=img, centres=ar.u_untested_cells, colour=(255, 0, 0)
    )
    img = contours.CircleContours().draw(
        img=img, centres=ar.u_active_cells, colour=(0, 0, 255)
    )
    return img


def draw_number(img, arena):
    """Draw the number of cells detected on the image."""
    n = str(len(arena.u_untested_cells) + len(arena.u_active_cells))
    cv2.putText(
        img, n, (10, img.shape[0] - 10),
        cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2
    )
    return img


def draw_run_time(img, dt):
    """Draw the time since started on the image."""
    if dt > datetime.timedelta(minutes=6):
        colour = (255, 0, 0)
    if dt > datetime.timedelta(minutes=5):
        colour = (255, 102, 0)
    elif dt > datetime.timedelta(minutes=4):
        colour = (255, 255, 0)
    else:
        colour = (0, 255, 0)
    cv2.putText(
        img, str(dt), (10, 80),
        cv2.FONT_HERSHEY_SIMPLEX, 1, colour, 2
    )
    return img


def draw_path(img, path):
    """Draw a path on the image."""
    for e in path.elements:
        img = cv2.line(
            img, (int(e.p1.x), int(e.p1.y)),
            (int(e.p2.x), int(e.p2.y)), (0, 255, 0), 1
        )
    return img


def draw_target(img, target):
    """Draw the target point."""
    return contours.CircleContours().draw(
        img=img, centres=target, colour=(255, 255, 255)
    )


def draw_robot(img, rbt):
    """Draw the robot on and image."""
    img = contours.CircleContours().draw(
        img=img, centres=rbt.u_position, colour=(0, 0, 255)
    )
    try:
        direction = (2 * list(rbt.markers)[0].length) * rbt.u_direction
    except IndexError:
        return img
    try:
        direction_line = vec.LineSegment(
            rbt.u_position, rbt.u_position + direction
        )
    except TypeError:
        return img
    img = contours.LineContours().draw(
        img=img, lines=direction_line, colour=(0, 0, 255)
    )
    return img


def draw_on_frame(frame, n, ar, rbt, path, target):
    """Draw the necessary points on a frame."""
    frame = draw_run_time(
        frame, datetime.timedelta(seconds=(n * secs_per_frame))
    )
    frame = draw_arena(frame, ar.queue())
    frame = draw_robot(frame, rbt.queue())
    frame = draw_path(frame, path)
    frame = draw_target(frame, target)
    return frame


frame = get_frame()

vw = cv2.VideoWriter(
    '{}_overlay.mp4'.format(video[:-4]),
    cv2.VideoWriter_fourcc(*'MP4V'),
    10, (frame.shape[1], frame.shape[0])
)

ini_arena = arena.get_default_arena(frame)
ini_robot = get_robot(frame)
path = mp.MinimumPath(ini_arena, ini_robot).path
points = (
    path.points
    + [vec.Point(p) for p in ini_arena.safe_area_targets[:2]]
    + [ini_robot._position]
)
path_i = 0

i = 0
while frame is not None:
    rbt = get_robot(frame)
    try:
        target = points[path_i]
    except IndexError:
        target = points[-1]
    if rbt._position is not None and rbt._position.distance(target) < 25:
        path_i += 1
    if rbt._position is not None and rbt._direction is not None:
        rbt.draw(frame, draw_position=False)
    frame = draw_on_frame(frame, i, ini_arena, rbt, path, target)
    vw.write(frame)
    frame = get_frame()
    i += 1
