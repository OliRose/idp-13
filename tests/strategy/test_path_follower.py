"""Test idp13.strategy.path_follower."""

import euclid3 as eu

from idp13.strategy import constants
from idp13.strategy import path_follower


class DummyRobot:
    """Mock robot object."""

    def __init__(self, position=None, direction=None):
        """Create the :obj:`DummyRobot`."""
        self._position = position
        self._direction = direction

    @property
    def position(self):
        """:obj:`euclid3.Point2`: Get the robot position."""
        return self._position

    @property
    def direction(self):
        """:obj:`euclid3.Vector2`: Get the robot direction."""
        return self._direction


test_robot = DummyRobot(eu.Point2(1, 7), eu.Vector2(1, 2).normalize())
test_target = eu.Point2(3, 5)

# print(test_robot.direction)
# print(test_robot.position)

test_path_follower = path_follower.PathFollower(test_robot, test_target)


# Test limit_wheel_speed function
assert(path_follower.limit_wheel_speed(300) == constants.PID.wheel_speed_max)
assert(path_follower.limit_wheel_speed(-300) == -constants.PID.wheel_speed_max)
assert(path_follower.limit_wheel_speed(100) == 100)
assert(path_follower.limit_wheel_speed(-100) == -100)
assert(path_follower.limit_wheel_speed(20) == constants.PID.wheel_speed_min)
assert(path_follower.limit_wheel_speed(-20) == -constants.PID.wheel_speed_min)


# Test wheel_velocity function
exp = (
    (5 - 2*constants.Robot.centre_to_wheel),
    (5 + 2*constants.Robot.centre_to_wheel)
)
assert(path_follower.wheel_velocities(5, 2) == exp)

# Test desired_speed functions
assert(path_follower.desired_speed(200) == 200)
assert(path_follower.desired_speed(150) == 200)
assert(path_follower.desired_speed(100) == 150)
assert(path_follower.desired_speed(0) == 50)


# Test measured_speed functions
recent_robots = [
    DummyRobot(),
    DummyRobot(),
    DummyRobot(),
    DummyRobot(),
    DummyRobot()
]
recent_times = [0, 0.1, 0.3, 0.4, 0.5]
assert(path_follower.measured_speed(recent_robots, recent_times) == 0)
recent_robots = [
    DummyRobot(),
    DummyRobot(eu.Point2(1, 2), eu.Vector2(1, 2).normalize()),
    DummyRobot(eu.Point2(1, 3)),
    DummyRobot(),
    DummyRobot(eu.Point2(2, 5), eu.Vector2(2, 3).normalize())
]
assert(type(
    path_follower.measured_speed(recent_robots, recent_times)
) in [int, float])


recent_times = [0, 0.1]
recent_robots = [
    DummyRobot(eu.Point2(0, 0)),
    DummyRobot(eu.Point2(5, 0), eu.Vector2(1, 0).normalize())
]
assert(path_follower.measured_speed(recent_robots, recent_times) > 0)
