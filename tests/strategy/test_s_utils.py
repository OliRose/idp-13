"""Test the utils module."""

import math
import pytest

from euclid3 import Vector2

from idp13.strategy import utils


def test_signed_angle_between():
    """Test signed_angle_between."""
    simple_vector = Vector2(1, 1)
    other_vector = Vector2(-0.5, -1)
    angle = utils.signed_angle_between(simple_vector, other_vector)
    assert angle == pytest.approx(-3 * math.pi / 4 - math.atan(0.5))


def test_rotate():
    """Test rotate."""
    simple_vector = Vector2(1, 1)
    rotated = utils.rotate(simple_vector, math.pi / 2)
    assert rotated.x == pytest.approx(-1) and rotated.y == pytest.approx(1)
