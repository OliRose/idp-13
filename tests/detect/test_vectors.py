"""Test idp13.detect.vectors."""

__author__ = 'Oli Rose'

import random
from collections import defaultdict

import numpy as np
import pytest

from idp13.detect import vectors as vec


def assert_xy_close(v, x, y):
    """Check v.x and v.y are close to x and y."""
    assert(v.x == pytest.approx(x))
    assert(v.y == pytest.approx(y))
    np.testing.assert_allclose(v._v, np.array([x, y]))


def assert_vectors_close(v1, v2, allow_opposite=False):
    """Check that two vectors are approx the same."""
    if allow_opposite:
        if v1.dot(v2) < 0:
            v2 = -v2
    assert(v1.x == pytest.approx(v2.x))
    assert(v1.y == pytest.approx(v2.y))
    np.testing.assert_allclose(v1._v, v2._v)


class RandomList:
    """Get random points."""

    def __init__(self, n=1, **kwargs):
        """Setup the RandomList."""
        self.n = n
        self.kwargs = defaultdict(lambda: None, kwargs)
        # Create a list of any fixed values
        self._fixed = self.kwargs['fixed'] or []
        if not isinstance(self._fixed, list):
            self._fixed = [self._fixed]

    def int(self, v1=100, v2=None):
        """Get a random int."""
        if v2 is None:
            min_v, max_v = sorted([v1, -v1])
        else:
            min_v, max_v = sorted([v1, v2])
        return random.randrange(min_v, max_v)

    @property
    def ints(self):
        """Get n ints."""
        return self._fixed + [self.int() for _ in range(self.n)]

    def float(self, v1=100, v2=None):
        """Get a random float."""
        if v2 is None:
            min_v, max_v = sorted([v1, -v1])
        else:
            min_v, max_v = sorted([v1, v2])
        return min_v + random.random() * (max_v - min_v)

    @property
    def floats(self):
        """Get n floats."""
        return self._fixed + [self.float() for _ in range(self.n)]

    @property
    def ints_and_floats(self):
        """Get n ints and n floats."""
        return self.ints + self.floats

    @property
    def xys(self):
        """Get a list of n random 2-tuples for x,y."""
        return (
            self._fixed + [(self.float(), self.float()) for _ in range(self.n)]
        )

    @property
    def basevectors(self):
        """Get a list of n BaseVectors."""
        return (
            [vec._BaseVector(p) for p in self._fixed]
            + [vec._BaseVector(xy) for xy in self.xys]
        )

    @property
    def points(self):
        """Get a list of n Points."""
        return (
            [vec.Point(p) for p in self._fixed]
            + [vec.Point(xy) for xy in self.xys]
        )

    @property
    def vectors(self):
        """Get a list of n Vectors."""
        return (
            [vec.Vector(v) for v in self._fixed]
            + [vec.Vector(xy) for xy in self.xys]
        )

    @property
    def points_and_vectors(self):
        """Get a list of n Points and n Vectors."""
        return self.points + self.vectors

    @property
    def lines(self):
        """Get a list of n Lines."""
        return (
            [vec.Line(self.points[i], self.vectors[i]) for i in range(self.n)]
        )

    @property
    def poss_linelike(self):
        """Get a list of initialised possible Line-like objects."""
        p1 = vec.Point()
        p2 = vec.Point()
        v = vec.Vector()
        return [
            vec.Point(), vec.Line(p1, v),
            vec.Ray(p1, v), vec.LineSegment(p1, p2)
        ]

    @property
    def linelike_types(self):
        """Get a list of Line-like types."""
        return [vec.Point, vec.Line, vec.Ray, vec.LineSegment]


@pytest.mark.parametrize('p1', RandomList().poss_linelike)
@pytest.mark.parametrize('p2', RandomList().poss_linelike)
@pytest.mark.parametrize('i_type', RandomList().linelike_types)
def test__get_pair_by_type(p1, p2, i_type):
    """Test vectors._get_pair_by_type."""
    if isinstance(p1, i_type) or isinstance(p2, i_type):
        assert(isinstance(vec._get_pair_by_type(p1, p2, i_type)[0], i_type))
    else:
        with pytest.raises(TypeError):
            vec._get_pair_by_type(p1, p2, i_type)


@pytest.mark.parametrize('p1', RandomList().poss_linelike)
@pytest.mark.parametrize('p2', RandomList().poss_linelike)
@pytest.mark.parametrize('i_type', RandomList().linelike_types)
def test__one_of_pair(p1, p2, i_type):
    """Test vectors._one_of_pair."""
    if isinstance(p1, i_type) or isinstance(p2, i_type):
        assert(vec._one_of_pair(p1, p2, i_type) is True)
    else:
        assert(vec._one_of_pair(p1, p2, i_type) is False)


class Test_Base:
    """Test vectors._Base."""

    def test_distance(self):
        """Test vectors._Base.distance."""
        pass    # TODO


class Test_BaseVector:
    """Test vectors._BaseVector."""

    @pytest.mark.parametrize('xy', RandomList(fixed=(0, 0)).xys)
    def test_create(self, xy):
        """Test creating a BaseVector."""
        bvs = []
        bvs.append(vec._BaseVector(x=xy[0], y=xy[1]))
        bvs.append(vec._BaseVector(xy))
        bv = vec._BaseVector()
        bv.x, bv.y = xy[0], xy[1]
        bvs.append(bv)
        bvs.append(vec._BaseVector(bv))
        for bv in bvs:
            assert_xy_close(bv, xy[0], xy[1])

    @pytest.mark.parametrize('cast', [('int', int), ('round', round)])
    @pytest.mark.parametrize('bv', RandomList().basevectors)
    def test_cast(self, cast, bv):
        """Test the type casting of BaseVector."""
        x, y = cast[1](bv.x), cast[1](bv.y)
        bv_cast = getattr(bv, cast[0])()
        assert(bv_cast.x == x)
        assert(bv_cast.y == y)
        assert(bv_cast.x == int(bv_cast.x))
        assert(bv_cast.y == int(bv_cast.y))

    @pytest.mark.parametrize('bv1', RandomList().basevectors)
    @pytest.mark.parametrize('bv2', RandomList().basevectors)
    def test_cmp(self, bv1, bv2):
        """Test the comparison operators."""
        if bv1.x == bv2.x and bv1.y == bv2.y:
            # Ensure the vectors are different
            bv1.x = bv2.x - 1
        assert(bv1 == bv1)
        assert(bv2 == bv2)
        assert(not bv1 == bv2)
        assert(not bv2 == bv1)
        assert(bv1 != bv2)
        assert(bv2 != bv1)
        assert(not bv1 != bv1)
        assert(not bv2 != bv2)
        bv2 = bv1
        assert(bv1 == bv2)
        assert(bv2 == bv1)

    @pytest.mark.parametrize('s', RandomList(2).ints_and_floats)
    @pytest.mark.parametrize('bv', RandomList(2).basevectors)
    def test_operators(self, s, bv):
        """Test the mathematical operators."""
        neg_bv = vec._BaseVector(-bv.x, -bv.y)
        mul_bv = vec._BaseVector(bv.x * s, bv.y * s)
        tdiv_bv = vec._BaseVector(bv.x / s, bv.y / s)
        fdiv_bv = vec._BaseVector(bv.x // s, bv.y // s)
        assert(neg_bv == -bv)
        assert(mul_bv == bv * s)
        assert(mul_bv == s * bv)
        assert(tdiv_bv == bv / s)
        assert(fdiv_bv == bv // s)
        with pytest.raises(NotImplementedError):
            bv * bv
        with pytest.raises(NotImplementedError):
            bv / bv
        with pytest.raises(NotImplementedError):
            bv // bv


class TestPoint:
    """Test vectors.Point."""

    @pytest.mark.parametrize('xy', RandomList(fixed=(0, 0)).xys)
    def test_create(self, xy):
        """Test creating a Point."""
        p = vec.Point()
        p.p = xy
        assert_xy_close(p, xy[0], xy[1])
        assert(p.p[0] == p.x)
        assert(p.p[1] == p.y)

    @pytest.mark.parametrize('p1', RandomList(fixed=(0, 0)).points)
    @pytest.mark.parametrize('p2', RandomList().points_and_vectors)
    def test_operators(self, p1, p2):
        """Test the mathematical operators."""
        # Addition
        add_p = vec.Point(p1.x + p2.x, p1.y + p2.y)
        assert(add_p == p1 + p2)
        assert(add_p == p2 + p1)
        assert(isinstance(p1 + p2, vec.Point))
        assert(isinstance(p2 + p1, vec.Point))
        with pytest.raises(TypeError):
            p1 + 1
        # Subtraction
        if isinstance(p2, vec.Point):
            sub_p1 = vec.Vector(p1.x - p2.x, p1.y - p2.y)
            sub_p2 = vec.Vector(p2.x - p1.x, p2.y - p1.y)
            assert(isinstance(p1 - p2, vec.Vector))
            assert(isinstance(p2 - p1, vec.Vector))
        elif isinstance(p2, vec.Vector):
            sub_p1 = vec.Point(p1.x - p2.x, p1.y - p2.y)
            sub_p2 = vec.Point(p2.x - p1.x, p2.y - p1.y)
            assert(isinstance(p1 - p2, vec.Point))
            assert(isinstance(p2 - p1, vec.Point))
        assert(sub_p1 == p1 - p2)
        assert(sub_p2 == p2 - p1)
        with pytest.raises(TypeError):
            p1 - 1


class TestVector:
    """Test vectors.Vector."""

    @pytest.mark.parametrize('xy', RandomList(fixed=(0, 0)).xys)
    def test_create(self, xy):
        """Test creating a Vector."""
        v = vec.Vector()
        v.v = xy
        assert_xy_close(v, xy[0], xy[1])
        assert(v.v[0] == v.x)
        assert(v.v[1] == v.y)

    @pytest.mark.parametrize('v1', RandomList(fixed=(0, 0)).vectors)
    @pytest.mark.parametrize('v2', RandomList().points_and_vectors)
    def test_operators(self, v1, v2):
        """Test the mathematical operators."""
        # Addition
        if isinstance(v2, vec.Point):
            add_p = vec.Point(v1.x + v2.x, v1.y + v2.y)
            assert(isinstance(v1 + v2, vec.Point))
        elif isinstance(v2, vec.Vector):
            add_p = vec.Vector(v1.x + v2.x, v1.y + v2.y)
            assert(isinstance(v1 + v2, vec.Vector))
        assert(add_p == v1 + v2)
        assert(add_p == v2 + v1)
        with pytest.raises(TypeError):
            v1 + 1
        # Subtraction
        if isinstance(v2, vec.Point):
            sub_p1 = vec.Point(v1.x - v2.x, v1.y - v2.y)
            sub_p2 = vec.Point(v2.x - v1.x, v2.y - v1.y)
            assert(isinstance(v1 - v2, vec.Point))
            assert(isinstance(v2 - v1, vec.Point))
        elif isinstance(v2, vec.Vector):
            sub_p1 = vec.Vector(v1.x - v2.x, v1.y - v2.y)
            sub_p2 = vec.Vector(v2.x - v1.x, v2.y - v1.y)
            assert(isinstance(v1 - v2, vec.Vector))
            assert(isinstance(v2 - v1, vec.Vector))
        assert(sub_p1 == v1 - v2)
        assert(sub_p2 == v2 - v1)
        with pytest.raises(TypeError):
            v1 - 1

    @pytest.mark.parametrize('v', RandomList(fixed=(0, 0)).vectors)
    def test_unit(self, v):
        """Test vectors.Vector.unit."""
        v_unit = v.unit()
        assert(isinstance(v_unit, vec.Vector))
        if v.x == pytest.approx(0) and v.y == pytest.approx(0):
            assert(v_unit == v)
            assert(v_unit.length == pytest.approx(0))
        else:
            unit_v = vec.Vector(v.x / v.length, v.y / v.length)
            assert(v_unit.length == pytest.approx(1))
            np.testing.assert_allclose(v_unit.v, unit_v.v)

    @pytest.mark.parametrize('v', RandomList(fixed=(0, 0)).vectors)
    def test_dot_perp(self, v):
        """Test vectors.Vector.perp and vectors.Vector.dot."""
        assert(v.dot(v.perp()) == pytest.approx(0))
        assert(v.perp().dot(v) == pytest.approx(0))


class TestLine:
    """Test vectors.Line."""

    @pytest.mark.parametrize('p', RandomList().points)
    @pytest.mark.parametrize('v', RandomList().vectors)
    def test_create(self, p, v):
        """Test creating a Vector."""
        line = vec.Line(p, p + v)
        assert_vectors_close(line.p, vec.Point(p))
        assert_vectors_close(line.v, vec.Point(v))


class TestLineSegment:
    """Test vectors.LineSegment."""

    @pytest.mark.parametrize('p', RandomList().points)
    @pytest.mark.parametrize('v', RandomList().vectors)
    @pytest.mark.parametrize('length', RandomList().ints_and_floats)
    def test_create(self, p, v, length):
        """Test creating a Vector."""
        d = vec.Line(p, v)
        # Without length
        p1, p2 = p, p + v
        ss = []
        ss.append(vec.LineSegment(p1, p2, 1))
        ss.append(vec.LineSegment(p, v))
        ss.append(vec.LineSegment(d))
        for s in ss:
            assert_vectors_close(s.p1, p1)
            assert_vectors_close(s.p2, p2)
            assert_vectors_close(s.p, p)
            assert_vectors_close(s.v, v)
            assert(s.length == pytest.approx(abs(v.length)))
        # With length
        p1, p2 = p, p + v.unit() * length
        ss = []
        ss.append(vec.LineSegment(p, v, length))
        ss.append(vec.LineSegment(d, p2, length=length))
        for s in ss:
            assert_vectors_close(s.p1, p1)
            assert_vectors_close(s.p2, p2)
            assert_vectors_close(s.p, p)
            assert_vectors_close(s.v, v.unit() * length)
            assert(s.length == pytest.approx(abs(length)))
        with pytest.raises(TypeError):
            vec.LineSegment(p1, 1)
        with pytest.raises(TypeError):
            vec.LineSegment(1, p1)


class TestRectangle:
    """Test vectors.Rectangle."""

    def test_create(self):
        """Test creating a Rectangle."""
        corners = [
            vec.Point(0, 0), vec.Point(4, 1),
            vec.Point(4, 0), vec.Point(0, 1)
        ]
        r = vec.Rectangle(corners)
        assert_xy_close(r.centre, 2, 0.5)
        assert_vectors_close(r.long_side, vec.Vector(4, 0), True)
        assert_vectors_close(r.short_side, vec.Vector(0, 1), True)
