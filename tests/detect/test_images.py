"""Test idp13.detect.images."""

__author__ = 'Oli Rose'

import cv2
import numpy as np

from idp13.detect import images


test_img = cv2.cvtColor(cv2.imread('tests/detect/images/test_images.png'),
                        cv2.COLOR_BGR2HSV)


def test_get_range():
    """Test images.get_range."""
    exp = cv2.imread('tests/detect/images/test_images_res1.png',
                     cv2.IMREAD_GRAYSCALE)
    _, exp = cv2.threshold(exp, 150, 255, cv2.THRESH_BINARY)
    res = images.get_range(test_img,
                           np.array([50, 200, 50]),
                           np.array([70, 220, 255]))
    assert np.count_nonzero(cv2.subtract(res, exp)) == 0


def test_morph_open():
    """Test images.morph_open."""
    exp = cv2.imread('tests/detect/images/test_images_res2.png')
    res = images.morph_open(test_img, 5)
    assert np.count_nonzero(cv2.subtract(res, exp)) == 0
