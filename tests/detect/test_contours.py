"""Test idp13.detect.contours."""

__author__ = 'Oli Rose'

import cv2
import numpy as np

from idp13.detect import (
    contours,
    vectors as vec,
)


test_img = cv2.cvtColor(cv2.imread('tests/detect/images/test_contours.png'),
                        cv2.COLOR_BGR2GRAY)
expc = [
    np.array([[[3, 3]], [[3, 97]], [[97, 97]], [[97, 3]]], dtype=np.int32),
    np.array([[[9, 10]], [[10, 9]], [[42, 9]], [[43, 10]], [[43, 40]],
              [[42, 41]], [[10, 41]], [[9, 40]]], dtype=np.int32),
]


def test_find_contours():
    """Test contours.find_contours."""
    c = contours.find_contours(test_img)
    diff = [c[i] - expc[i] for i in range(len(c))]
    assert sum(np.count_nonzero(d) for d in diff) == 0


def test_CircleContours_find():
    """Test contours.CircleContours.find."""
    exp = [vec.Point(50, 50), vec.Point(26, 25)]
    centres = contours.CircleContours(test_img).centres
    assert centres == exp


# def test_RectangleContours_find():
#     """Test contours.RectangleContours.find."""
#     exp = [(3, 3, 95, 95), (9, 9, 35, 33)]
#     rects = contours.RectangleContours(test_img).rects
#     assert rects == exp


# def test_LineContours_find():
#     """Test contours.LineContours.find."""
#     exp = [contours.Line(-644, 767, 745, -670),
#            contours.Line(-642, 769, 746, -669)]
#     lines = contours.LineContours(test_img, n=2).lines
#     assert lines == exp
