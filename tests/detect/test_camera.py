"""Test idp13.detect.camera.

When connected to the webcam, more testing is performed.
When not connected to the webcam, testing is reduced
but still runs.
"""

__author__ = 'Oli Rose'

import cv2
import pytest

from idp13.detect import camera


@pytest.fixture(scope='module')
def c():
    """Create a new camera object."""
    with camera.Camera() as new_c:
        yield new_c


def test_brightness(c):
    """Test getting and setting the brightness."""
    assert c.brightness == camera.Camera.defaults['brightness']
    c.brightness = 10
    assert c.brightness == 10
    # TODO - If any constraint placed on brightness, test here
    if c.is_open():
        assert c._vc.get(cv2.CAP_PROP_BRIGHTNESS) == 10


def test_saturation(c):
    """Test getting and setting the saturation."""
    assert c.saturation == camera.Camera.defaults['saturation']
    c.saturation = 10
    assert c.saturation == 10
    # TODO - If any constraint placed on saturation, test here
    if c.is_open():
        assert c._vc.get(cv2.CAP_PROP_SATURATION) == 10


def test_contrast(c):
    """Test getting and setting the contrast."""
    assert c.contrast == camera.Camera.defaults['contrast']
    c.contrast = 10
    assert c.contrast == 10
    # TODO - If any constraint placed on contrast, test here
    if c.is_open():
        assert c._vc.get(cv2.CAP_PROP_CONTRAST) == 10


def test_read(c):
    """Test reading a frame."""
    frame = c.read()
    if c.is_open():
        pass        # TODO
    else:
        assert frame is None
