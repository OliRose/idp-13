/**
* @file arduino.ino
* @brief Main arduino program
* @author Oli Rose
*/

#include "src/comms/comms.h"
#include "src/comms/message.pb.h"
#include "src/hardware/hardware.h"


/**
* @brief Set up the hardware and the comms link (including handshake)
*/
void setup() {
    // Setup the hardware
    hardware::setup();
    // Set up the serial link and handshake
    Serial.begin(57600);
    while (!Serial) {}
    comms::handshake();
}

/**
* @brief Main loop to process messages from the PC
*/
void loop() {
    bool result = comms::process_Message();
    if (result) {
        delay(100);
        comms::write_Response();
    }
}
