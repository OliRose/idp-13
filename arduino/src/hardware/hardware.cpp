/**
* @file src/hardware/hardware.cpp
* @brief Setup and control the arduino hardware (sensors and motors)
* @author Oli Rose
*/

#include "hardware.h"

// Arduino libraries
#include "Arduino.h"

// Motor control
#include <Adafruit_MotorShield.h>
#include <Servo.h>

// Protocol buffers
#include "../comms/message.pb.h"


namespace hardware {

namespace {
    // Sensor data
    volatile bool hall;
    volatile uint8_t safe;
    volatile uint8_t active;
    // Motors and servos
    Adafruit_MotorShield AFMS;
    Adafruit_DCMotor* left_wheel;
    Adafruit_DCMotor* right_wheel;
    Servo flipper;
    Servo door;
}

/***********************************************************************
*
* INITIALIZE HARDWARE
*
***********************************************************************/
void setup() {
    // Setup the LEDs
    pinMode((int)Leds::kRed, OUTPUT);
    pinMode((int)Leds::kAmber, OUTPUT);
    pinMode((int)Leds::kGreen, OUTPUT);
    digitalWrite((int)Leds::kRed, LOW);
    digitalWrite((int)Leds::kAmber, LOW);
    digitalWrite((int)Leds::kGreen, LOW);
    // Setup the sensors
    pinMode((int)Sensors::kHall, INPUT);
    pinMode((int)Sensors::kTouch1, INPUT);
    pinMode((int)Sensors::kTouch2, INPUT);
    // Attach the interrupts
    int pin = digitalPinToInterrupt((int)Sensors::kHall);
    attachInterrupt(pin, ISR_hall, RISING);
    pin = digitalPinToInterrupt((int)Sensors::kTouch1);
    attachInterrupt(pin, ISR_touch1, RISING);
    pin = digitalPinToInterrupt((int)Sensors::kTouch2);
    attachInterrupt(pin, ISR_touch2, RISING);
    // Setup the sensors data
    hall = false;
    safe = 0;
    active = 0;
    // Setup the motors
    AFMS.begin();
    left_wheel = AFMS.getMotor(LEFT_WHEEL_PORT);
    right_wheel = AFMS.getMotor(RIGHT_WHEEL_PORT);
    // Setup the servos
    flipper.attach(FLIPPER_PIN);
    flipper.write(FLIPPER_ANGLE);
    door.attach(DOOR_PIN);
    door.write(DOOR_ANGLE);
}

/***********************************************************************
*
* LEDs
*
***********************************************************************/
void control_led(Leds_Option led, bool value) {
    Leds pin;
    switch (led) {
        case Leds_Option_RED : {
            pin = Leds::kRed;
            break;
        }
        case Leds_Option_AMBER : {
            pin = Leds::kAmber;
            break;
        }
        case Leds_Option_GREEN : {
            pin = Leds::kGreen;
            break;
        }
        default : break;
    }
    if (value) {
        digitalWrite((int)pin, HIGH);
    } else {
        digitalWrite((int)pin, LOW);
    }
}

/***********************************************************************
*
* MOTORS
*
***********************************************************************/
void control_wheel(Motors_Wheel wheel,
                   Motors_Direction direction,
                   int8_t speed) {
    if (speed == 0) {
        control_led(Leds_Option_AMBER, false);
    } else {
        control_led(Leds_Option_AMBER, true);
    }
    switch (wheel) {
        case Motors_Wheel_LEFT : {
            control_left_wheel(direction, speed);
            break;
        }
        case Motors_Wheel_RIGHT : {
            control_right_wheel(direction, speed);
            break;
        }
        default : break;
    }
}

namespace {

void control_left_wheel(Motors_Direction direction, int8_t speed) {
    // Control the left wheel motor
    left_wheel->setSpeed(speed);
    // Set the correct direction (accounting for reversed motors)
    switch (direction) {
        case Motors_Direction_FORWARD : {
            left_wheel->run(FORWARD);
            break;
        }
        case Motors_Direction_BACKWARD : {
            left_wheel->run(BACKWARD);
            break;
        }
        case Motors_Direction_STOP : {
            left_wheel->run(RELEASE);
            break;
        }
        default : break;
    }
}

void control_right_wheel(Motors_Direction direction, int8_t speed) {
    // Control the right wheel motor
    right_wheel->setSpeed(speed);
    // Set the correct direction (accounting for reversed motors)
    switch (direction) {
        case Motors_Direction_FORWARD : {
            right_wheel->run(FORWARD);
            break;
        }
        case Motors_Direction_BACKWARD : {
            right_wheel->run(BACKWARD);
            break;
        }
        case Motors_Direction_STOP : {
            right_wheel->run(RELEASE);
            break;
        }
        default : break;
    }
}

}

/***********************************************************************
*
* SERVOS
*
***********************************************************************/
void control_servo(Servos_Option option, int16_t angle) {
    switch (option) {
        case Servos_Option_FLIPPER : {
            if (angle + FLIPPER_ANGLE > FLIPPER_MAX) {
                angle = FLIPPER_MAX;
            } else if (angle + FLIPPER_ANGLE < FLIPPER_MIN) {
                angle = FLIPPER_MIN;
            } else {
                angle += FLIPPER_ANGLE;
            }
            flipper.write(angle);
            break;
        }
        case Servos_Option_DOOR : {
            if (angle > DOOR_MAX) {
                angle = DOOR_MAX;
            } else if (angle < DOOR_MIN) {
                angle = DOOR_MIN;
            }
            door.write(DOOR_ANGLE - angle);
            break;
        }
        default : break;
    }
    if (option == Servos_Option_DOOR && angle == 0) {
        control_led(Leds_Option_RED, false);
    }
}

/***********************************************************************
*
* STATUS
*
***********************************************************************/
uint8_t get_safe() {
    return safe;
}

uint8_t get_active() {
    return active;
}

void reset_counts() {
    active = 0;
    safe = 0;
}

/***********************************************************************
*
* SENSORS
*
***********************************************************************/
void ISR_hall() {
    hall = true;
}
void ISR_touch1() {
    // Reset the hall sensor (and shut the door?)
    hall = false;
    // control_servo(Servos_Option_FLIPPER, 0);
    GREEN_ON;
}
void ISR_touch2() {
    if (hall) {
        // This is an active cell
        active++;
        control_servo(Servos_Option_FLIPPER, -35);
    } else {
        // Safe cell
        safe++;
        RED_ON;
        control_servo(Servos_Option_FLIPPER, 45);
    }
    GREEN_OFF;
}

}  // namespace hardware
