/**
* @file src/hardware/hardware.h
* @brief Setup and control the arduino hardware (sensors and motors)
* @author Oli Rose
*/

#ifndef ARDUINO_HARDWARE_HARDWARE_H_
#define ARDUINO_HARDWARE_HARDWARE_H_

// Protocol buffers
#include "../comms/message.pb.h"

#define RED_ON PORTH = PORTH | B00001000    ///< Turn the red LED on
#define RED_OFF PORTH = PORTH & B11110111   ///< Turn the red LED off
#define AMBER_ON PORTH = PORTH | B00010000  ///< Turn the amber LED on
#define AMBER_OFF PORTH = PORTH & B11101111 ///< Turn the amber LED off
#define GREEN_ON PORTE = PORTE | B00001000  ///< Turn the green LED on
#define GREEN_OFF PORTE = PORTE & B11110111 ///< Turn the green LED off

/**
* @brief Hardware control namespace
*/
namespace hardware {

/**
* @brief Initialise the hardware
*/
void setup();

/***********************************************************************
* LEDs
***********************************************************************/
/**
* @brief Pin numbers for the LEDs
*/
enum class Leds {
    kRed = 6,
    kAmber = 7,
    kGreen = 5
};

/**
* @brief Control the LEDs
* @param led Which LED to control
* @param value Turn the LED on when true
*/
void control_led(Leds_Option led, bool value);

/***********************************************************************
* MOTORS
***********************************************************************/
static const int LEFT_WHEEL_PORT = 1;
static const int RIGHT_WHEEL_PORT = 2;

/**
* @brief Control a wheel
* @param wheel Which wheel to control
* @param direction The direction to run the wheel
* @param speed The speed (0-255)
*/
void control_wheel(
    Motors_Wheel wheel,
    Motors_Direction direction,
    int8_t speed
);
namespace {
    void control_left_wheel(Motors_Direction direction, int8_t speed);
    void control_right_wheel(Motors_Direction direction, int8_t speed);
}

/***********************************************************************
* SERVOS
***********************************************************************/
static const int FLIPPER_PIN = 10;
static const int FLIPPER_ANGLE = 65;  ///< Starting angle (and zero angle)
static const int FLIPPER_MIN = 30;
static const int FLIPPER_MAX = 100;

static const int DOOR_PIN = 9;
static const int DOOR_ANGLE = 180;  ///< Starting angle (and zero angle)
static const int DOOR_MIN = 0;
static const int DOOR_MAX = 180;

/**
* @brief Turn a servo to a certain angle (within certain bounds)
* @param option Which servo to control
* @param angle The angle to set the servo to
*/
void control_servo(Servos_Option option, int16_t angle);

/***********************************************************************
* STATUS
***********************************************************************/
/// @name Cell counts
/// @{
/**
* @return Number of safe cells detected
*/
uint8_t get_safe();
/**
* @return Number of active cells detected
*/
uint8_t get_active();
/**
* @brief Reset the number of cells counted
*/
void reset_counts();
/// @}

/***********************************************************************
* SENSORS
***********************************************************************/
/**
* @brief Pin numbers for the sensors
*/
enum class Sensors {
    kHall = 3,
    kTouch1 = 2,
    kTouch2 = 18
};

/// @name ISRs
/// @{
/**
* @brief Hall effect sensor triggered interrupt
*/
void ISR_hall();
/**
* @brief First line detection sensor triggered interrupt
*/
void ISR_touch1();
/**
* @brief Second line detection sensor triggered interrupt
*/
void ISR_touch2();
/// @}

}  // namespace hardware

#endif
