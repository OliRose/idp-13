/**
* @file src/comms/comms.h
* @brief Arduino <-> PC communication using Protocol Buffers
* @author Oli Rose
*/

#ifndef ARDUINO_COMMS_COMMS_H_
#define ARDUINO_COMMS_COMMS_H_

// Protocol buffers
#include "message.pb.h"


namespace comms {

const int BAUD_RATE = 57600;  ///< Baud rate for serial link
const int BUFFER_SIZE = 255;  ///< RX and TX buffer size


/// @name Handshake
/// @{
/**
* @brief Handshake with the PC (block until successful)
*/
void handshake();
/**
* @brief Perform a single handshake attempt
* @return Whether the handshake was successful
*/
bool attempt_handshake();
/**
* @brief Check whether a message is a handshake and of a certain type
* @param message The message to check
* @param type The type of handshake expected
* @return Whether the message is a handshake and of correct type
*/
bool check_Handshake(Message message, Status type);
/**
* @brief Create a handshake message
* @param type The type of handshake message to create
* @return Handshake message to be sent
*/
Message create_Handshake(Status type);
/// @}


/// @name High level message
/// @{
/**
* @brief Read and process a message from the PC
* @param timeout The timeout for read_Message
* @return Whether a response is expected from the arduino
*/
bool process_Message(uint16_t timeout = 1000);
/**
* @brief Write a response message with the latest cell counts
*/
void write_Response();
/// @}


/// @name Low level message
/// @{
/**
* @brief Stop waiting for a read timeout
*/
void stop_read();
/**
* @brief Read a message from the PC
* @param timeout The timeout for reading from the serial connection
* @return Message read from the PC (INVALID status if timed out)
*/
Message read_Message(uint16_t timeout = 1000);
/**
* @brief Write a message to the serial link
* @param message
*/
void write_Message(Message message);
/// @}


/// @name Hardware
/// @{
/**
* @brief Process a motor control message
* @param message
*/
void process_Motors(Motors message);
/**
* @brief Process a servo control message
* @param message
*/
void process_Servos(Servos message);
/**
* @brief Process an LED control message
* @param message
*/
void process_Leds(Leds message);
/// @}

}  // namespace comms

#endif
