/**
* @file src/comms/comms.cpp
* @brief Arduino <-> PC communication using Protocol Buffers
* @author Oli Rose
*/

#include "comms.h"

// Arduino libraries
#include "Arduino.h"

// Protocol buffers
#include <pb.h>
#include <pb_decode.h>
#include <pb_encode.h>
#include "message.pb.h"

// Hardware access
#include "../hardware/hardware.h"

namespace comms {

namespace {
    volatile bool _stop_read = false;
}

/***********************************************************************
*
* HANDSHAKE
*
***********************************************************************/
void handshake() {
    bool h_res = attempt_handshake();
    while (!h_res) {
        h_res = attempt_handshake();
    }
}

bool attempt_handshake() {
    // Attempt a handshake
    if (!check_Handshake(read_Message(1000), Status_ASKING)) {
        // Message received was not ASKING
        return false;
    }
    delay(10);
    // Write a RESPONDING response once asked
    write_Message(create_Handshake(Status_RESPONDING));
    if (!check_Handshake(read_Message(1000), Status_COMPLETED)) {
        // Message received was not COMPLETED
        return false;
    }
    delay(10);
    // Write a COMPLETED message to finish handshake
    write_Message(create_Handshake(Status_COMPLETED));
    return true;
}

bool check_Handshake(Message message, Status status) {
    /* Check if a message is a handshake of a certain type */
    return (message.which_type == Message_handshake_tag &&
            message.type.handshake.type == status);
}

Message create_Handshake(Status status) {
    /* Create a handshake message of a certain type */
    Message message = Message_init_zero;
    message.which_type = Message_handshake_tag;
    message.type.handshake.type = status;
    return message;
}

/***********************************************************************
*
* HIGH LEVEL MESSAGE FUNCTIONS
*
***********************************************************************/
bool process_Message(uint16_t timeout) {
    // Wait for a Call message and process it
    Message message = read_Message(timeout);
    if (message.which_type != Message_call_tag) {
        // Invalid message type
        return false;
    }
    switch (message.type.call.status) {
        case Status_RESPONDING : {
            // Response requested
            return true;
        }
        case Status_ASKING :{
            switch (message.type.call.which_type) {
                case Call_motors_tag : {
                    process_Motors(message.type.call.type.motors);
                    break;
                }
                case Call_servos_tag : {
                    process_Servos(message.type.call.type.servos);
                    break;
                }
                case Call_leds_tag : {
                    process_Leds(message.type.call.type.leds);
                    break;
                }
                default : break;
            }
        }
        // Any other Call types are invalid
        default : break;
    }
    return false;
}

void write_Response() {
    // Create a response message
    Message message = Message_init_zero;
    message.which_type = Message_response_tag;
    message.type.response.safe = hardware::get_safe();
    message.type.response.active = hardware::get_active();
    // hardware::reset_counts();
    write_Message(message);
}

/***********************************************************************
*
* LOW LEVEL MESSAGE FUNCTIONS
*
***********************************************************************/
void stop_read() {
    _stop_read = true;
}

Message read_Message(uint16_t timeout) {
    // Read a Message
    _stop_read = false;
    uint8_t buffer[BUFFER_SIZE] = {'\0'};
    // Create an NONE Call to be returned if read fails
    Message message = Message_init_zero;
    message.which_type = Message_call_tag;
    message.type.call.status = Status_NONE;
    // Setup the timeout
    unsigned long timeout_end = millis() + timeout;
    // Wait for a message
    while (Serial.available() == 0) {
        if (millis() > timeout_end || _stop_read) {
            // Timed out so return the INVALID Call
            return message;
        }
    }
    // Find the length and wait until enough bytes available
    uint8_t length = Serial.read();
    // Wait until enough bytes available (or timed out)
    while (Serial.available() < length) {
        if (millis() > timeout_end || _stop_read) {
            // Timed out so return the INVALID Call
            return message;
        }
    }
    // Read the data in and process as a message
    for (uint8_t i = 0; i < length; i++) {
        buffer[i] = Serial.read();
    }
    // Process message
    pb_istream_t stream = pb_istream_from_buffer(buffer, length);
    pb_decode(&stream, Message_fields, &message);
    return message;
}

void write_Message(Message message) {
    // Write a Response message
    uint8_t buffer[BUFFER_SIZE] = {'\0'};
    pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));
    pb_encode(&stream, Message_fields, &message);
    // Write the length followed by the data
    Serial.write(stream.bytes_written);
    for (uint8_t i = 0; i < stream.bytes_written; i++) {
        Serial.write(buffer[i]);
    }
}

/***********************************************************************
*
* HARDWARE FUNCTIONS
*
***********************************************************************/
void process_Motors(Motors message) {
    // Process a motors message
    for (uint8_t i = 0; i <= _Motors_Wheel_MAX; i++) {
        hardware::control_wheel(
            message.motors[i].wheel,
            message.motors[i].direction,
            message.motors[i].speed
        );
    }
}

void process_Servos(Servos message) {
    // Process a servos message
    for (uint8_t i = 0; i <= _Servos_Option_MAX; i++) {
        hardware::control_servo(message.servos[i].option, message.servos[i].angle);
    }
}

void process_Leds(Leds message) {
    // Process a leds message
    for (uint8_t i = 0; i <= _Leds_Option_MAX; i++) {
        hardware::control_led(message.leds[i].option, message.leds[i].value);
    }
}

}  // namespace comms
