"""Utility functions and classes."""

__author__ = 'Oli Rose'

import argparse
import collections
import logging
import os
import threading
import time
from datetime import datetime


logger = logging.getLogger('idp13.{}'.format(__name__))
logger.setLevel(logging.DEBUG)


class StoppableThread(threading.Thread):
    """New :obj:`Thread` class which can be told to stop.

    Also implements a setup function, a standard timed loop and a
    concluding function.

    Args:
        name (:obj:`str`): Name to give the thread
        loop_time (:obj:`float`): The time to run the loop at
            intervals of

    Attributes:
        name (:obj:`str`): Name of the thread
        q (:obj:`DefaultDeque`): An output queue from this
            thread with a max length of 1

    """

    def __init__(self, name='StoppableThread', loop_time=0.1):
        """Create the :obj:`StoppableThread`."""
        super().__init__(name=name)
        self.name = name
        logger.debug('Creating new thread: {}'.format(self.name))
        self.q = DefaultDeque(maxlen=1)
        self._stop_event = threading.Event()
        self._loop_time = loop_time

    def start(self):
        """Start the thread."""
        logger.debug('Starting thread: {}'.format(self.name))
        super().start()

    def stop(self):
        """Stop the thread."""
        logger.debug('Stopping thread: {}'.format(self.name))
        self._stop_event.set()

    def _choose_f(self, f):
        """Choose a function to run (:obj:`f`, :obj:`self.loop`, ``pass``)."""
        if f is True:
            return self.loop
        elif f is None:

            def pass_f():
                """Pass."""
                pass
            return pass_f
        else:
            return f

    def timed_loop(self, f=True, stop_f=lambda: False):
        """Run a loop for this function at ``loop_time`` intervals.

        This function will run the specific function chosen (see below)
        continuously until one of the following:

            * Thread stopped
            * Function being called returns False
            * Stop function returns True

        Args:
            f (`function`, optional): Function to run in a loop,
                default is ``self.loop``. ``False`` runs ``pass``
            stop_f (`function`, optional): Function that will be True
                when the loop should stop

        """
        res = True
        while (not self._stop_event.is_set() and not stop_f()
               and res is not False):
            res = self._choose_f(f)()
            time.sleep(self._loop_time)

    def setup(self):
        """Empty setup function."""
        pass

    def loop(self):
        """Dummy loop function."""
        raise NotImplementedError

    def finish(self):
        """Empty finish function."""
        pass

    def _run_check_args(self, f, f_args=None, f_kwargs=None):
        """Run the function checking which args to pass in."""
        if f is None:
            return None
        if f_args is None and f_kwargs is None:
            return f()
        elif f_args is not None and f_kwargs is None:
            return f(*f_args)
        elif f_args is None and f_kwargs is not None:
            return f(**f_kwargs)
        else:
            return f(*f_args, **f_kwargs)

    def wait_for(self, f, update=None, f_args=None, f_kwargs=None,
                 update_args=None, update_kwargs=None):
        """Wait for a function to return True.

        Args:
            f (:obj:`function`): Function to test whether completed,
                returns True when completed
            update (:obj:`function`, optional): Function to run between
                each check to update values
            f_args (:obj:`list`, optional): Args to pass to ``f``
            f_kwargs (:obj:`dict`, optional): Kwargs to pass to ``f``
            update_args (:obj:`list`, optional): Args to pass to
                ``update``
            update_kwargs (:obj:`dict`, optional): Kwargs to pass to
                ``update``

        """
        while (not self._stop_event.is_set()
               and not self._run_check_args(f, f_args, f_kwargs)):
            self._run_check_args(update, update_args, update_kwargs)
            time.sleep(0.1)

    def run(self):
        """Run the setup, loop and finish functions."""
        self.setup()
        if self._stop_event.is_set():
            return
        self.timed_loop()
        if self._stop_event.is_set():
            return
        self.finish()


class Queueable:
    """Class that can be converted to immutable tuple.

    Stores the class attributes in a :obj:`collections.namedtuple`
    object which is a thread safe immutable object.
    If a child class defines ``self.queue_attrs`` these attributes will
    be those stored (can include ``@property`` attributes). If not, the
    atrtributes in ``self.__dict__`` will be stored. To combine the two
    behaviours, add ``'__dict__'`` to ``self.queue_attrs``.

    """

    def queue(self):
        """Convert this to its queueable form."""
        try:
            attrs = self.queue_attrs
            if '__dict__' in attrs:
                attrs.remove('__dict__')
                attrs.extend([k for k in self.__dict__])
        except AttributeError:
            attrs = [k for k in self.__dict__]
        Queued = collections.namedtuple(
            'Queued{}'.format(self.__class__.__name__),
            ['u{}'.format(a) if a.startswith('_') else a for a in attrs]
        )
        q_obj = Queued(
            *[getattr(self, attr) for attr in attrs]
        )
        return q_obj


class DefaultDeque(collections.deque):
    """Deque class that returns a defult value when empty.

    Args:
        default: The value to return if the queue contents are
            requested while the queue is empty
        *args: Directly passed to :obj:`collections.deque()`
        **kwargs: Directly passed to :obj:`collections.deque()`
            (e.g. ``maxlen=1``)

    """

    def __init__(self, default=None, *args, **kwargs):
        """Create the deque."""
        self.default = default
        super().__init__(*args, **kwargs)

    def pop(self):
        """Pop method that returns the default if the queue is empty."""
        try:
            return super().pop()
        except IndexError:
            return self.default

    def __getitem__(self, *args, **kwargs):
        try:
            return super().__getitem__(*args, **kwargs)
        except IndexError:
            return self.default


def add_logging_level(name, num):
    """Add a new level to logging.

    Adapted from: https://stackoverflow.com/a/35804945

    """
    if hasattr(logging, name):
        raise AttributeError(
            '{} already defined in logging module'.format(name)
        )
    if hasattr(logging, name.lower()):
        raise AttributeError(
            '{} already defined in logging module'.format(name.lower())
        )
    if hasattr(logging.getLoggerClass(), name.lower()):
        raise AttributeError(
            '{} already defined in logger class'.format(name.lower())
        )

    def log_level(self, message, *args, **kwargs):
        """Log at the new level."""
        if self.isEnabledFor(num):
            self._log(num, message, args, **kwargs)

    def log_root(message, *args, **kwargs):
        """Log the new level to the root logger."""
        logging.log(num, message, *args, **kwargs)

    logging.addLevelName(num, name)
    setattr(logging, name, num)
    setattr(logging.getLoggerClass(), name.lower(), log_level)
    setattr(logging, name.lower(), log_root)


def setup_logging(verbose=False, no_logging=False):
    """Set up logging for the main program.

    Args:
        verbose (:obj:`bool`): Whether to include verbose logging
        no_logging (:obj:`bool`) Whether to turn off logging

    Returns:
        :obj:`logging.Logger`: Main logger for the program (called ``idp13``)

    """
    logger.debug('Setting up logging')
    add_logging_level('VERBOSE', logging.DEBUG - 5)
    if no_logging is True:
        logger.info('Logging disabled')
        logging.disable(logging.WARNING)
        return logging.getLogger('main')
    if not os.path.isdir('logs'):
        os.mkdir('logs')
    if not os.path.isdir('logs/full'):
        os.mkdir('logs/full')
    if not os.path.isdir('logs/sparse'):
        os.mkdir('logs/sparse')
    formatter = logging.Formatter(
        '%(asctime)s:%(threadName)-15s:%(name)-35s:%(levelname)-8s:%(message)s'
    )
    full_log_handler = logging.FileHandler(
        'logs/full/{}.log'.format(datetime.now().strftime('%Y%m%dT%H%M%SZ'))
    )
    full_log_handler.setFormatter(formatter)
    full_log_handler.setLevel(logging.DEBUG)
    sparse_log_handler = logging.FileHandler(
        'logs/sparse/{}.log'.format(datetime.now().strftime('%Y%m%dT%H%M%SZ'))
    )
    sparse_log_handler.setFormatter(formatter)
    sparse_log_handler.setLevel(logging.INFO)
    main_logger = logging.getLogger('idp13')
    try:
        main_logger.setLevel(logging.VERBOSE)
    except AttributeError:
        main_logger.setLevel(logging.DEBUG)
    main_logger.addHandler(full_log_handler)
    main_logger.addHandler(sparse_log_handler)
    if verbose:
        if not os.path.isdir('logs/verbose'):
            os.mkdir('logs/verbose')
        verbose_log_handler = logging.FileHandler(
            'logs/verbose/{}.log'.format(
                datetime.now().strftime('%Y%m%dT%H%M%SZ')
            )
        )
        verbose_log_handler.setFormatter(formatter)
        try:
            verbose_log_handler.setLevel(logging.VERBOSE)
        except AttributeError:
            verbose_log_handler.setLevel(logging.DEBUG)
        main_logger.addHandler(verbose_log_handler)
    return main_logger


def get_parser(description='IDP13'):
    """Get an argument parser for the likely args.

    Further arguments can be added afterwards

    Args:
        description (:obj:`str`): Description for the help message

    Returns:
        :obj:`argparse.ArgumentParser`

    """
    logger.debug('Setting up argument parser')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-p', '--port', type=str, default='COM3',
                        help='COM port number for the arduino')
    parser.add_argument('-c', '--camera', type=int, default=0,
                        help='Camera number')
    parser.add_argument('--restart', action='store_true',
                        help='Store this as the first run')
    parser.add_argument('-s', '--sweep', action='store_true',
                        help='Use the default sweep path')
    parser.add_argument('-g', '--gui', action='store_true',
                        help='Run with a GUI (if applicable)')
    parser.add_argument('--height', type=int, default=0,
                        help='Maximum height of the frame to display')
    parser.add_argument('--flip', action='store_true',
                        help='Whether to flip the frame')
    parser.add_argument('--no-overlay', action='store_true',
                        help='Do not display the arena overlay')
    parser.add_argument('--no-robot', action='store_true',
                        help='Do not display the robot overlay')
    parser.add_argument('--no-logging', action='store_true',
                        help='Disable logging for this run')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Enable verbose logging output')
    parser.add_argument('--video', action='store_true',
                        help='Output the frames to a video (in ../images/)')
    parser.add_argument('--party', action='store_true',
                        help='Party at the end!')
    parser.add_argument('-w', '--white', type=int, nargs='+',
                        help='The values to use for the white threshold')
    return parser


def sign(value):
    """Get the sign of a value.

    Args:
        value (:obj:`float`): An object that can be compared to
            an integer

    Returns:
        :obj:`int`:
            * -1 if value < 0
            * 0 if value == 0
            * 1 if value > 0

    """
    if value == 0:
        return 0
    elif value > 0:
        return 1
    else:
        return -1
