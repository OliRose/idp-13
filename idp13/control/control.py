"""Main control code.

This module implements the main control functionality for the
system, including setup, creating strategy and implementing the
control loop.

"""

__author__ = 'Oli Rose'

import logging
import threading
import time

import euclid3 as eu

from idp13 import utils
from idp13.comms import comms
from idp13.detect import arena, camera, robot
from idp13.strategy import minimum_path, path_follower, sweep_path


logger = logging.getLogger('idp13.{}'.format(__name__))
try:
    logger.setLevel(logging.VERBOSE)
except AttributeError:
    logger.setLevel(logging.DEBUG)
    logger.verbose = logger.debug

SAFE_X = 0.7              # * frame width
SAFE_Y = [0.33, 0.66]     # * frame height


class ControlThread(utils.StoppableThread):
    """Main control thread.

    Args:
        time_available (:obj:`datetime.timedelta`): The time left
            before the robot should return to home
        camera_n (:obj:`int`): Camera index
        port (:obj:`str`): COM port of the arduino
        sweep (:obj:`bool`): True to use the :mod:`idp13.strategy.sweep_path`
            path (default is to use :mod:`idp13.strategy.minimum_path`)
        white_thresh (:obj:`array` of :obj:`int`): The thresholds to use
            to find the robot in each frame (will test each threshold on
            every frame and use the best)
        video_out (:obj:`str`, optional): Path to save an output video to
        party (:obj:`bool`): True for the robot to celebrate at the end

    """

    def __init__(self, time_available, camera_n=0,
                 port='COM3', sweep=False, white_thresh=None,
                 video_out=None, party=False):
        """Create the :obj:`ControlThread`."""
        super().__init__(name='ControlThread')
        self.vc = camera.Camera(n=camera_n, res=(1920, 1080), flipped=True,
                                left_margin=0.22, right_margin=0.09,
                                video_out=video_out)
        frame = self.vc.read()
        while (frame is None and not self._stop_event.is_set()):
            frame = self.vc.read()
        self.frame_width = frame.shape[1]
        self.frame_height = frame.shape[0]
        logger.info('Camera set up')
        logger.debug('Frame size: ({}, {})'.format(
            self.frame_width, self.frame_height)
        )
        self.camera_t = camera.CameraThread(self.vc)
        self.robot_t = robot.RobotThread(
            self.camera_t.q, white_thresh=white_thresh
        )
        self.link = comms.Link(port=port)
        logger.info('Link set up on {}'.format(port))
        self.link_t = comms.LinkThread(self.link)
        self.arena_t = arena.ArenaThread(self.camera_t.q, self.link_t)
        self.sensors_q = self.link_t.q
        self.end_timer = threading.Timer(
            time_available.total_seconds(), self.return_to_home
        )
        logger.info('Timer set up for {}s'.format(
            time_available.total_seconds())
        )
        self.end_event = threading.Event()
        # Dummy for the path follower thread
        self.path_f_t = None
        # Check whether to use the pathfinder or a simple sweep
        if sweep:
            self.sweep = sweep_path.SweepPath()
            self.loop = self.sweep_loop
        self.party = party
        self.path = None
        self.arena = None

    def stop(self):
        """Stop the program."""
        self.end_event.set()
        self._stop_event.set()
        self.end_timer.cancel()
        self._try_stop_thread(self.path_f_t)
        self.stop_robot()
        time.sleep(1)
        self._try_stop_thread(self.camera_t)
        self._try_stop_thread(self.arena_t)
        self._try_stop_thread(self.robot_t)
        self._try_stop_thread(self.link_t)
        self.vc._close()
        self.link.close()
        super().stop()

    def _try_stop_thread(self, t):
        """Try to stop a thread and block until it dies."""
        try:
            if t.is_alive():
                t.stop()
            t.join()
        except (AttributeError, RuntimeError):
            pass

    def _wait_robot_detection(self):
        """Wait for the robot detection to start up."""
        if (self.end_event.is_set() or self._stop_event.is_set()
                or self.robot_t.q[0] is not None):
            return True

    def setup(self):
        """Set the thread up."""
        self.camera_t.start()
        self.robot_t.start()
        self.link_t.start()
        self.end_timer.start()
        # Wait for the robot detection to start
        if self.robot_t.q[0] is None:
            logger.debug('Waiting for first robot detection')
        self.wait_for(self._wait_robot_detection)
        if self.end_event.is_set() or self._stop_event.is_set():
            # Timer timed out so it is time to return to home
            return
        self.initial_position = self.robot_t.q[0].position
        logger.debug('Initial robot detection completed ({})'.format(
            self.initial_position)
        )
        self.detect_arena()
        self.path = minimum_path.MinimumPath(
            self.arena, self.robot_t.q[0]).path
        self.path_i = 0
        for i in range(100):
            self.link_t.close_door()

    def loop(self):
        """Main control loop while the robot is following the path."""
        self.stop_robot()
        try:
            target = self.path.points[self.path_i]
        except IndexError:
            return False
        self.path_i += 1
        logger.info('Next target: {}'.format(target))
        self.path_follower(target)
        self.wait_for_path_follower()
        return True

    def sweep_loop(self):
        """Sweep control loop.

        Used if :obj:`ControlThread` created with ``sweep==True``

        """
        self.stop_robot()
        try:
            target = self.sweep.next_point()
        except IndexError:
            return False
        logger.info('Next sweep point: {}'.format(target))
        self.path_follower(target)
        self.wait_for_path_follower()
        return True

    def finish(self):
        """Finish the loop."""
        self.return_to_home(party=False)

    def detect_arena(self, hardcoded=False):
        """Store the arena data and start the arena detection thread.

        Args:
            hardcoded (:obj:`bool`): True to use the hard-coded arena
                values from :obj:`idp13.detect.arena`. Else move the
                robot to two locations and detect the arena based on
                those two images

        """
        self.detect_points = [
            eu.Point2(SAFE_X * self.frame_width, y * self.frame_height)
            for y in SAFE_Y
        ]
        logger.debug('Arena safe points are {}'.format(self.detect_points))
        if hardcoded:
            logger.info('Using hard-coded arena data')
            initial_arena = arena.get_default_arena(self.camera_t.q[0])
        else:
            logger.info('Running arena detection routine')
            self.path_follower(self.detect_points[0])
            self.wait_for_path_follower()
            if self.end_event.is_set():
                return
            # Store the first image
            img1 = self.camera_t.q[0]
            logger.debug('First arena detection image captured')
            self.path_follower(self.detect_points[1])
            self.wait_for_path_follower()
            if self.end_event.is_set():
                return
            # Get the second image and process them to identify the arena
            img2 = self.camera_t.q[0]
            logger.debug('Second arena detection image captured')
            initial_arena = arena.detect(img1, img2)
        # Set the arena object and start the arena detection thread
        self.arena = initial_arena.queue()
        self.arena_t.set_initial_arena(initial_arena)
        self.safe_area_targets = initial_arena.safe_area_targets
        self.arena_t.start()

    def path_follower(self, target, orient_only=False):
        """Create a thread to make the robot move to target.

        Args:
            target (:obj:`idp13.detect.vectors.Point`): The target point
                (measured in pixels on the image) for the robot to
                move to
            orient_only (:obj:`bool`): True for the robot to only
                orient itself towards the target and not move

        Returns:
            :obj:`idp13.strategy.path_follower.PathFollower`

        """
        logger.info('Creating path follower to {}'.format(target))
        self.q.append(target)
        try:
            # Make sure we only have one path follower
            self.path_f_t.stop()
            logger.debug('Old path follower killed')
        except AttributeError:
            pass
        self.path_f_t = None
        target = eu.Point2(target.x, target.y)
        self.path_f_t = path_follower.PathFollower(
            self.robot_t.q, target, orient_only
        )
        self.link_t.swap_command_q(self.path_f_t.q)
        # Start the thread and the command queue
        self.path_f_t.start()
        logger.debug('New path follower created')
        return self.path_f_t

    def wait_for_path_follower(self):
        """Wait until the path follower finishes or a cell is detected."""
        logger.debug('Waiting for a path follower to complete')
        self.wait_for(lambda: not self.path_f_t.is_alive())

    def stop_robot(self):
        """Stop the robot."""
        logger.info('Stopping robot')
        self.link_t.pause()
        self.link_t.stop_robot()

    def restart_robot(self):
        """Restart the robot."""
        logger.info('Restarting robot')
        self.link_t.unpause()

    def drop_off_cells(self):
        """Drop off any collected cells."""
        logger.info('Dropping off collected cells')
        self.path_follower(self.safe_area_targets[0])
        self.wait_for_path_follower()
        self.path_follower(self.safe_area_targets[1])
        self.wait_for_path_follower()
        self.link_t.open_door()
        self.path_follower(self.safe_area_targets[2])
        self.wait_for_path_follower()
        self.path_follower(self.safe_area_targets[3], orient_only=True)
        self.wait_for_path_follower()
        self.link_t.close_door()

    def return_to_home(self, party=False):
        """Make the robot return to home, either at timeout or when done."""
        # Make sure this function is not called twice
        self.end_timer.cancel()
        self.end_event.set()
        self.stop_robot()
        self._try_stop_thread(self.path_f_t)
        self.drop_off_cells()
        logger.info('Returning to home')
        if party and self.party:
            self.path_follower(self.detect_points[0])
            self.wait_for_path_follower()
            self.link_t.open_door()
            self.link_t.leds(True, True, True)
            time.sleep(1)
            self.link_t.close_door()
            self.link_t.leds(False, False, False)
            time.sleep(1)
            self.link_t.leds(True, True, True)
            time.sleep(1)
            self.link_t.leds(False, False, False)
        self.path_follower(self.initial_position)
        self.wait_for_path_follower()
        self.path_follower(
            eu.Point2(0, self.initial_position.y), orient_only=True
        )
        self.wait_for_path_follower()
        self.stop()
