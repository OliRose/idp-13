"""Main program and GUI display.

This module uses :obj:`idp13.control.control` to run the main control
loop and also implements a graphical display. See above for command
line arguments if run as main script.

"""

__author__ = 'Oli Rose'

import os
import threading
import time
import datetime

import cv2

if __name__ == '__main__':
    # Set up the logging for the whole system if run as __main__
    from idp13 import utils

    args = utils.get_parser('Run the GUI').parse_args()
    logger = utils.setup_logging(
        verbose=args.verbose, no_logging=args.no_logging
    )
else:
    # Set up logging for this module only, if being imported
    import logging

    logger = logging.getLogger('idp13.{}'.format(__name__))
    try:
        logger.setLevel(logging.VERBOSE)
    except AttributeError:
        logger.setLevel(logging.DEBUG)
        logger.verbose = logger.debug

from idp13 import detect
from idp13.control import control

ISO_8601 = '%Y%m%dT%H%M%SZ'
MINS_SAFE = 5


class GUI:
    """GUI class."""

    def __init__(self):
        """Create the :obj:`GUI`."""
        passed = False
        if not args.restart:
            try:
                with open('.run_time', 'r') as f:
                    self.run_time = datetime.datetime.strptime(
                        f.read(), ISO_8601
                    )
                logger.info('Time read from file: {}'.format(self.run_time))
                if (datetime.datetime.now() - self.run_time
                        > datetime.timedelta(minutes=MINS_SAFE)):
                    input(
                        'More than 5 minutes have passed, '
                        'press any key to restart timer...'
                    )
                    passed = False
                else:
                    passed = True
            except Exception:
                # File doesn't exist or is invalid
                logger.warning('No .run_time file found or is invalid')
                passed = False
        if not passed:
            self.run_time = datetime.datetime.now()
            with open('.run_time', 'w') as f:
                f.write(self.run_time.strftime(ISO_8601))
        time_available = (
            datetime.timedelta(minutes=MINS_SAFE)
            - (datetime.datetime.now() - self.run_time)
        )
        if args.video:
            # Create the video output folder
            folder = '../images/{}'.format(
                self.run_time.strftime('%Y%m%dT%H%M%SZ')
            )
            if not os.path.isdir(folder):
                os.mkdir(folder)
            video_out = '{}/video'.format(folder)
        else:
            video_out = False
        self.control_t = control.ControlThread(
            time_available=time_available,
            camera_n=args.camera, port=args.port,
            sweep=args.sweep, white_thresh=args.white,
            video_out=video_out, party=args.party
        )
        self.no_overlay = args.no_overlay
        self.no_robot = args.no_robot
        self.camera_q = self.control_t.camera_t.q
        self.arena_q = self.control_t.arena_t.q
        self.robot_q = self.control_t.robot_t.q
        self.path = None

    def run(self):
        """Run the code (with or without a GUI)."""
        if args.gui:
            self._run_gui()
        else:
            input('Press enter to stop...')

    def _run_gui(self):
        """Run with a GUI."""
        i = 0
        while True:
            if not self.control_t.is_alive():
                break
            try:
                frame = self.camera_q[0].copy()
                frame_copy = frame.copy()
            except AttributeError:
                frame = None
            arena = self.control_t.arena
            robot = self.robot_q[0]
            target = self.control_t.q[0]
            if self.path is None and self.control_t.path is not None:
                self.path = self.control_t.path.copy()
            if frame is None:
                continue
            if not self.no_overlay and arena is not None:
                frame = self.draw_arena(frame, arena)
            if not self.no_overlay and target is not None:
                frame = self.draw_target(frame, target)
            if not self.no_overlay and self.path is not None:
                frame = self.draw_path(frame, self.path)
            if not self.no_robot and robot is not None:
                frame = self.draw_robot(frame, robot)
            if args.height != 0 and frame.shape[0] > args.height:
                scale = args.height / frame.shape[0]
                frame = cv2.resize(
                    frame,
                    (int(frame.shape[1] * scale), int(frame.shape[0] * scale))
                )
            if args.flip:
                frame = cv2.flip(frame, 1)
            if not self.no_overlay and arena is not None:
                frame = self.draw_number(frame, arena)
            frame = self.draw_run_time(frame, self.run_time)
            cv2.imshow('Frame', frame)
            k = cv2.waitKey(1)
            if k & 0xFF == ord('s'):
                cv2.destroyAllWindows()
                break
            elif k & 0xFF == ord('c'):
                i += 1
                folder = '../images/{}'.format(
                    self.run_time.strftime('%Y%m%dT%H%M%SZ')
                )
                if not os.path.isdir(folder):
                    os.mkdir(folder)
                cv2.imwrite('{}/{}.png'.format(folder, i), frame_copy)
            time.sleep(0.01)

    def __enter__(self):
        """Enter the context manager."""
        self.control_t.start()
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        """Exit the context manager."""
        self.control_t.stop()
        while len(threading.enumerate()) > 1:
            time.sleep(0.1)
            logger.debug('Living threads: {}'.format(threading.enumerate()))

    def draw_arena(self, img, arena):
        """Draw arena data on an image."""
        img = detect.contours.RectangleContours().draw(
            img=img, rects=arena.safe_area, colour=(0, 255, 0)
        )
        img = detect.contours.CircleContours().draw(
            img=img, centres=arena.u_untested_cells, colour=(255, 0, 0)
        )
        img = detect.contours.CircleContours().draw(
            img=img, centres=arena.u_active_cells, colour=(0, 0, 255)
        )
        img = detect.contours.LineContours().draw(
            img=img, lines=arena.red_line, colour=(0, 0, 255)
        )
        img = detect.contours.LineContours().draw(
            img=img, lines=arena.cell_line, colour=(0, 0, 0)
        )
        img = detect.contours.CircleContours().draw(
            img=img, centres=arena.start_zones, colour=(0, 0, 0)
        )
        img = detect.contours.LineContours().draw(
            img=img, lines=arena.u_start_line, colour=(30, 30, 30)
        )
        img = detect.contours.LineContours().draw(
            img=img, lines=arena.u_start_cross, colour=(30, 30, 30)
        )
        return img

    def draw_number(self, img, arena):
        """Draw the number of cells detected on the image."""
        n = str(len(arena.u_untested_cells) + len(arena.u_active_cells))
        cv2.putText(
            img, n, (10, img.shape[0] - 10),
            cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2
        )
        return img

    def draw_run_time(self, img, run_time):
        """Draw the time since started on the image."""
        dt = datetime.datetime.now() - run_time
        if dt > datetime.timedelta(minutes=6):
            colour = (255, 0, 0)
        if dt > datetime.timedelta(minutes=5):
            colour = (255, 102, 0)
        elif dt > datetime.timedelta(minutes=4):
            colour = (255, 255, 0)
        else:
            colour = (0, 255, 0)
        cv2.putText(
            img, str(dt), (10, 80),
            cv2.FONT_HERSHEY_SIMPLEX, 1, colour, 2
        )
        return img

    def draw_path(self, img, path):
        """Draw a path on the image."""
        for e in path.elements:
            img = cv2.line(
                img, (int(e.p1.x), int(e.p1.y)),
                (int(e.p2.x), int(e.p2.y)), (0, 255, 0), 1
            )
        return img

    def draw_target(self, img, target):
        """Draw the target point on an image."""
        if self.control_t.q[0] is not None:
            logger.verbose('Current target: {}'.format(self.control_t.q[0]))
            img = detect.contours.CircleContours().draw(
                img=img, centres=target, colour=(255, 255, 255)
            )
        return img

    def draw_robot(self, img, robot):
        """Draw the robot data on an image."""
        img = detect.contours.CircleContours().draw(
            img=img, centres=robot.u_position, colour=(0, 0, 255)
        )
        try:
            direction = (2 * list(robot.markers)[0].length) * robot.u_direction
        except IndexError:
            return img
        try:
            direction_line = detect.vectors.LineSegment(
                robot.u_position, robot.u_position + direction
            )
        except TypeError:
            return img
        img = detect.contours.LineContours().draw(
            img=img, lines=direction_line, colour=(0, 0, 255)
        )
        return img


if __name__ == '__main__':
    with GUI() as g:
        g.run()
