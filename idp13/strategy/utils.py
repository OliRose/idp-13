"""Utility functions.

This module implements functions for manipulating the euclid3 module's vectors.

"""

import logging
import math

import euclid3 as eu


logger = logging.getLogger('idp13.{}'.format(__name__))
try:
    logger.setLevel(logging.VERBOSE)
except AttributeError:
    logger.setLevel(logging.DEBUG)
    logger.verbose = logger.debug


def rotate(vector, angle):
    """Return the rotation of a vector by a number of radians anticlockwise.

    Args:
        vector (:obj:`euclid3.Vector2`): Original vector
        angle (:obj:`float`): Angle of rotation (anticlockwise) in radians

    Returns:
        :obj:`euclid3.Vector2`: Rotated vector

    """
    c = math.cos(angle)
    s = math.sin(angle)
    return eu.Vector2(vector.x * c - vector.y * s, vector.x * s + vector.y * c)


def signed_angle_between(a, b):
    """Return the anticlockwise angle (between -pi and pi) from a to b.

    Args:
        a (:obj:`euclid3.Vector2`): Start vector
        b (:obj:`euclid3.Vector2`): End vector

    Returns:
        :obj:`float`:
        Angle (in radians) anticlockwise from a to b, between -pi and pi

    """
    abs_angle = a.angle(b)
    z_component = a.x * b.y - a.y * b.x
    angle = abs_angle if z_component > 0 else -abs_angle
    logger.verbose('signed_angle_between: {}'.format(angle))
    return angle
