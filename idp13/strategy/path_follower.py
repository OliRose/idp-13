"""PID control for the robot following a straight path."""

import collections
import logging
import math
import time

from simple_pid import PID

from idp13 import utils
from idp13.strategy import constants, utils as s_utils


logger = logging.getLogger('idp13.{}'.format(__name__))
try:
    logger.setLevel(logging.VERBOSE)
except AttributeError:
    logger.setLevel(logging.DEBUG)
    logger.verbose = logger.debug

Output = collections.namedtuple(
    'Output', ['left', 'right']
)

ANGLE_TOL = math.pi / 16
ARRIVED_TOL = 25

# For averaging calcualted velocity
NUMBER_DATAPOINTS = 50


class PathFollower(utils.StoppableThread):
    """Follow a path to the target.

    Object created in a separate thread by the main module that uses PID
    control to continuously update the robot's wheel velocities

    """

    def __init__(self, robot_q, target, orient_only=False):
        """Create the thread."""
        self.robot_q = robot_q
        self.target = target
        self.angular_controller = PID(
            constants.PID.angular_kp, constants.PID.angular_kd,
            constants.PID.angular_ki
        )
        self.linear_controller = PID(
            constants.PID.linear_kp, constants.PID.linear_kd,
            constants.PID.linear_ki
        )
        self.orient_only = orient_only
        super().__init__(name='PathFollower')

    def run(self):
        """Run improved path follower."""
        # Log robots and ties for calculating velocity
        recent_robots = [None]*NUMBER_DATAPOINTS
        recent_times = [None]*NUMBER_DATAPOINTS

        while not self._stop_event.is_set():
            time.sleep(0.01)

            # Update recent_robots and recent_times
            # Most recent is recent_robots[-1]
            recent_robots.append(self.robot_q[0])
            recent_robots = recent_robots[1:]

            recent_times.append(time.time())
            recent_times = recent_times[1:]

            # Stop the robot if detection fails causeing robot to return None
            if recent_robots[-1] is None:
                logger.verbose('Robot detection failed - stopping robot')
                self.q.append(Output(0, 0))
                continue

            robot_to_target = recent_robots[-1].position.connect(self.target)
            distance_error = robot_to_target.length
            angle_error = s_utils.signed_angle_between(
                recent_robots[-1].direction, robot_to_target.v
            )
            logger.verbose('New direction: {}'.format(robot_to_target))
            logger.verbose('distance_error: {}'.format(distance_error))
            logger.verbose('angle_error: {}'.format(angle_error))

            if distance_error < ARRIVED_TOL:
                # The robot has arrived at the target
                logger.info('Arrived at target: {}'.format(self.target))
                break

            # Calcualte angular and linear velocities with PID controllers
            linear_velocity = 0
            angular_velocity = self.angular_controller(-angle_error)
            angular_velocity = (
                angular_velocity if angular_velocity is not None else 0
            )

            if abs(angle_error) < ANGLE_TOL:
                if self.orient_only:
                    break
                # Also calculate linear velocity
                desired_velocity = desired_speed(distance_error)
                speed_error = (
                    desired_velocity
                    - measured_speed(recent_robots, recent_times)
                )
                linear_velocity = self.linear_controller(-speed_error)
                linear_velocity = (
                    linear_velocity if linear_velocity is not None else 0
                )
                linear_velocity += desired_velocity

            # Calculate wheel velocities
            left_wheel_velocity, right_wheel_velocity = wheel_velocities(
                linear_velocity, angular_velocity
            )

            # Limit wheel wheel velocities
            left_wheel_velocity = limit_wheel_speed(left_wheel_velocity)
            right_wheel_velocity = limit_wheel_speed(right_wheel_velocity)

            # Output wheel velocties
            self.q.append(Output(left_wheel_velocity, right_wheel_velocity))


def limit_wheel_speed(wheel_velocity):
    """Apply wheel speed limits."""
    if abs(wheel_velocity) > constants.PID.wheel_speed_max:
        limited_wheel_velocity = constants.PID.wheel_speed_max
    elif abs(wheel_velocity) < constants.PID.wheel_speed_min:
        limited_wheel_velocity = constants.PID.wheel_speed_min
    else:
        return wheel_velocity
    limited_wheel_velocity *= utils.sign(wheel_velocity)
    return limited_wheel_velocity


def wheel_velocities(linear_velocity, angular_velocity):
    """Calculate the wheel speeds.

    Calculates each wheel speed based on the linear and angular
    velocities then applies the speed limits.

    """
    left_wheel_velocity = (
        linear_velocity
        - angular_velocity * constants.Robot.centre_to_wheel
    )
    right_wheel_velocity = (
        linear_velocity
        + angular_velocity * constants.Robot.centre_to_wheel
    )

    return left_wheel_velocity, right_wheel_velocity


def desired_speed(distance):
    """Calculate intended linear speed based on distance from target."""
    high_speed = 200
    slow_speed = 50
    decelerate_distance = 150

    if distance > decelerate_distance:
        return high_speed
    else:
        return (
            slow_speed
            + (high_speed - slow_speed) * (distance / decelerate_distance)
        )


def measured_speed(recent_robots, recent_times):
    """Calculate the current speed of the robot.

    Uses a data set of the robots most recent positions.

    """
    n_datapoints = min(
        [NUMBER_DATAPOINTS, len(recent_robots), len(recent_times)]
    )
    # Calcualte speeds using each of the datapoints
    speeds = []
    for n in range(n_datapoints - 1):
        try:
            # Might not handle case where recent_robots[-1] is a poor result
            # recent_robots[-1] should never be None loop continues
            dir1 = recent_robots[n].position.connect(
                recent_robots[-1].position).v
            dir2 = recent_robots[-1].direction
            dt = recent_times[-1] - recent_times[n]
            speeds.append(dir1.dot(dir2) / dt)
        except AttributeError:
            # If camera detection failed and recent_robots contain NoneTypes
            pass

    if len(speeds) == 0:
        return 0

    # Average the calcualted speeds
    total = 0
    for speed in speeds:
        total += speed
    average_speed = total/len(speeds)
    logger.verbose('Calculated speed: {}'.format(average_speed))
    return average_speed
