"""Constants used by the strategy modules.

This module contains constants used by the strategy modules, in relevant
classes. Most are measured or set empirically.

"""

pixels_per_mm = 0.529


class PID:
    """PID constants."""

    angular_kp = 1.5
    angular_kd = 0.1
    angular_ki = 0.0

    linear_kp = 0.7
    linear_kd = 0.0
    linear_ki = 0.0

    wheel_speed_min = 40
    wheel_speed_max = 255


class Robot:
    """Constants relating to the robot."""

    width = 294.0 * pixels_per_mm   # 2 * 146.55mm
    length = 340.0 * pixels_per_mm
    wheels_to_front = 100.0 * pixels_per_mm
    sensor_to_front = 123.0 * pixels_per_mm  # First ultrasound sensor
    centre_to_wheel = 145 * pixels_per_mm  # Need to measure accurately
    capacity = 6


class Table:
    """Constants relating to the table."""

    # sidelength = 2400 * pixels_per_mm
    minimum_cell_to_edge_distance = 100 * pixels_per_mm


class MinimumPath:
    """Constants for the minimum path finder."""

    margin = 0.25
    included = 0.8
    extend = 0.25
    length_weight = 1
    angle_weight = 1
    red_line_x = 645


class SweepPath:
    """Constants for the sweep path."""

    separation = 0.9
    margin = 0.3
