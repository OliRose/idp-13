"""New min path algorithm."""

import collections

from idp13.detect import vectors as vec
from idp13.strategy import constants, pathfinder


TempRobot = collections.namedtuple(
    'TempRobot', ['position', 'direction']
)


class Path:
    """A full path between targets."""

    def __init__(self, points):
        """Create the path from a list of points."""
        self.points = points
        self.elements = [
            points[i].connect(self.points[i + 1])
            for i in range(len(self.points) - 1)
        ]

    @property
    def length(self):
        """Get the total length travelled."""
        return sum([e.length for e in self.elements])

    @property
    def angle(self):
        """Get the total angle turned."""
        angle = 0
        for i in range(len(self.elements) - 1):
            angle += (
                self.elements[i].angle(self.elements[i + 1])
            )
        return angle

    @property
    def weight(self):
        """Get the total weight."""
        return (
            constants.MinimumPath.length_weight * self.length
            + constants.MinimumPath.angle_weight * self.angle
        )

    def add_point(self, point, start=False):
        """Add a point to the :obj:`Path`."""
        if start:
            self.points.insert(0, point)
            self.elements.insert(
                0, vec.LineSegment(self.points[0], self.points[1])
            )
        else:
            self.points.append(point)
            self.elements.append(
                vec.LineSegment(self.points[-2], self.points[-1])
            )

    def extend(self, path):
        """Extend this path with another path."""
        for p in path.points:
            self.add_point(p)

    def copy(self):
        """Get a copy of this object."""
        return Path(self.points.copy())


def min_path(targets, prev_path=None, final_point=None):
    """Find the minimum path covering the targets."""
    if targets == []:
        return prev_path
    paths = []
    for t in targets:
        new_path = prev_path.copy()
        new_path.add_point(_limit_point(t))
        new_targets = _sweep_filter(
            new_path.points[-2], new_path.points[-1], targets
        )
        assert(len(new_targets) < len(targets))
        p = min_path(new_targets, new_path, final_point)
        paths.append(p)
    test_paths = []
    for i, p in enumerate(paths):
        new_p = p.copy()
        new_p.add_point(vec.Point(final_point))
        test_paths.append((i, new_p))
    i = sorted(test_paths, key=lambda p: p[1].length)[0][0]
    return paths[i]


def _limit_point(point):
    """Limit a point to remain inside the danger area."""
    red_line_limit = (
        constants.MinimumPath.red_line_x - 0.4 * constants.Robot.width
    )
    if point.x > red_line_limit:
        point.x = red_line_limit
    if point.x < 0.5 * constants.Robot.width:
        point.x = constants.Robot.width
    return point


def _sweep_filter(p1, p2, targets):
    """Remove any targets that will be swept up."""
    sweep = vec.Rectangle.from_line(
        p1.connect(p2).extend(
            constants.MinimumPath.extend * constants.Robot.length
        ),
        constants.MinimumPath.included * constants.Robot.width
    )
    new_targets = []
    for target in targets:
        if not sweep.contains(target):
            new_targets.append(target)
    return new_targets


class MinimumPath:
    """Create the minimum path."""

    def __init__(self, arena, robot):
        """Create the :obj:`MinimumPath`."""
        self.width = constants.MinimumPath.included * constants.Robot.width
        self.cells = [vec.Point(c) for c in arena.untested_cells]
        self.arena = arena
        self.max_x = max(
            self.arena.edges['right'].p1.x,
            self.arena.edges['left'].p1.x
        )
        self.max_y = max(
            self.arena.edges['top'].p1.y,
            self.arena.edges['bottom'].p1.y
        )
        self.path = Path([vec.Point(robot.position)])
        corner_cell = self.check_corner()
        while corner_cell is not None:
            self.add_point(corner_cell)
            corner_cell = self.check_corner()
        self.add_point(vec.Point(self.arena.cell_line_targets[0]))
        self.add_point(vec.Point(self.arena.cell_line_targets[1]))
        self.path = min_path(
            self.cells, self.path, self.arena.safe_area_targets[0]
        )
        temp_robot = TempRobot(
            position=self.path.points[-1].euclidify(),
            direction=self.path.points[-2].connect(
                self.path.points[-1]).v.euclidify()
        )
        pf = pathfinder.find_clear_route(
            temp_robot, arena, self.arena.safe_area_targets[0]
        )
        print(pf)
        for p in pf:
            self.path.add_point(vec.Point(p))

    def add_point(self, point):
        """Add a point."""
        self.path.add_point(point)
        self.cells = _sweep_filter(
            self.path.points[-2], self.path.points[-1], self.cells
        )

    def check_corner(self):
        """Check the corner for cells."""
        close_corner = vec.Point(
            self.max_x,
            constants.MinimumPath.margin * constants.Robot.length
        )
        far_corner = vec.Point(
            constants.MinimumPath.margin * constants.Robot.width,
            constants.MinimumPath.margin * constants.Robot.length
        )
        corner_poly = vec.Polygon([
            close_corner,
            self.arena.cell_line_targets[0],
            far_corner
        ])
        corner_cells = []
        for cell in self.cells:
            if corner_poly.contains(cell):
                corner_cells.append(cell)
        if corner_cells == []:
            return None
        else:
            return sorted(corner_cells, key=lambda c: c.y)[0]
