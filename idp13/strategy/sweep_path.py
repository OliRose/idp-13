"""Set up a sweep path."""

import euclid3 as eu
import numpy as np

from idp13.detect import (
    contours,
    vectors as vec
)
from idp13.strategy import constants


class SweepPath:
    """Generate the values for the sweep."""

    def __init__(self, img_shape=(1080, 1326), red_line_x=645):
        """Create the object."""
        margin = constants.SweepPath.margin
        separation = constants.SweepPath.separation
        p = [
            np.array([
                0.7 * constants.Robot.width,
                margin * constants.Robot.length], dtype=int
            ),
            np.array([
                0.6 * constants.Robot.width,
                img_shape[0] * 0.65], dtype=int
            ),
            np.array([
                0.7 * constants.Robot.width,
                img_shape[0] - margin * constants.Robot.length], dtype=int
            ),
        ]
        r = int((img_shape[1] - red_line_x) / (1.2 * constants.Robot.width))
        for i in range(0, r):
            p.append(p[-1] + np.array(
                [separation * constants.Robot.width, 0], dtype=int
            ))
            p.append(p[-1] + np.array(
                [0, 2 * margin * constants.Robot.length - img_shape[0]],
                dtype=int
            ))
            p.append(p[-1] + np.array(
                [separation * constants.Robot.width, 0], dtype=int
            ))
            p.append(p[-1] - np.array(
                [0, 2 * margin * constants.Robot.length - img_shape[0]],
                dtype=int
            ))
        p = p[:9]
        self.points = [eu.Point2(i[0], i[1]) for i in p]
        self.i = -1

    def next_point(self):
        """Get the next point."""
        self.i += 1
        return self.points[self.i]


class SmartSweepPath:
    """Create a smart sweep path.

    Args:
        arena (:obj:`idp13.detect.arena.Arena`)
        robot (:obj:`idp13.detect.robot.Robot`)
        margin (:obj:`float`): The safety margin on the edges of
            the camera/table
        included (:obj:`float`): The proportion of the robot width
            regarded as included in each sweep

    """

    def __init__(self, arena, robot, margin=0.25, included=0.9):
        """Create the :obj:`SmartSweepPath`."""
        self.margin = margin
        self.included = included
        self.u_cells = [vec.Point(c) for c in arena.untested_cells]
        self.t_cells = []
        self.arena = arena
        self.max_x = max(
            self.arena.edges['right'].p1.x,
            self.arena.edges['left'].p1.x
        )
        self.max_y = max(
            self.arena.edges['top'].p1.y,
            self.arena.edges['bottom'].p1.y
        )
        self.paths = []
        # Set initial positions (collect known cells)
        self.targets = [vec.Point(robot.position)]
        corner_cell = self.check_corner()
        while corner_cell is not None:
            self.add_target(corner_cell)
            corner_cell = self.check_corner()
        self.add_target(vec.Point(58, 405))
        self.add_target(vec.Point(58, 1000))
        while len(self.u_cells) > 0:
            self.add_next_targets()
        self.add_target(vec.Point(robot.position))

    @property
    def _u_cells_x(self):
        """Get the cells sorted by x value."""
        return sorted(self.u_cells, key=lambda c: c.x)

    @property
    def _u_cells_y(self):
        """Get the cells sorted by y value."""
        return sorted(self.u_cells, key=lambda c: c.y)

    def add_target(self, target):
        """Add a new target point and process the effects."""
        self.targets.append(target)
        self.update_cells()

    def update_cells(self):
        """Update the two cell lists to track any collected on a path."""
        path = self.targets[-2].connect(
            self.targets[-1]).extend(constants.Robot.length * 0.1)
        self.paths.append(path)
        r = vec.Rectangle.from_line(
            path, self.included * constants.Robot.width
        )
        old_u_cells = self.u_cells.copy()
        for cell in old_u_cells:
            if r.contains(cell):
                self.u_cells.remove(cell)
                self.t_cells.append(cell)

    def add_next_targets(self):
        """Add the next two target points for the robot."""
        if len(self.u_cells) == 0:
            print('Completed')
            return
        elif len(self.u_cells) == 1:
            self.add_target(self.u_cells[0])
        else:
            if self.targets[-1].y > self.max_y * 0.5:
                # Head for the highest y value cell
                self.add_target(self._u_cells_y[-1])
            else:
                # Lowest y value cell
                self.add_target(self._u_cells_y[0])
            # Now head for the highest x value cell
            self.add_target(self._u_cells_x[-1])

    def check_corner(self):
        """Check the corner for cells."""
        corner = vec.Polygon([
            (self.margin * constants.Robot.width,
             self.margin * constants.Robot.length),
            (58, 405),
            (self.targets[-1])])
        corner_cells = []
        for cell in self.u_cells:
            if corner.contains(cell):
                corner_cells.append(cell)
        if corner_cells == []:
            return None
        else:
            return sorted(corner_cells, key=lambda c: c.y)[0]


if __name__ == '__main__':
    import cv2

    img = cv2.imread('C:/IDP/images/datasets/3/2.png')
    img = cv2.flip(img, 1)
    left_i = int(0.22 * img.shape[1])
    right_i = int(0.09 * img.shape[1])
    img = img[:, left_i:-right_i]
    img = contours.CircleContours().draw(
        img=img, centres=SweepPath().points, colour=(0, 0, 255)
    )
    img = cv2.resize(img, (int(img.shape[1] / 2), int(img.shape[0] / 2)))
    cv2.imshow('A', img)
    cv2.waitKey(0)
