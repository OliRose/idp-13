"""Path generation from spatial data.

This module implements a function for avoiding cells near the red line when
returning from the dangerous zone.

"""

import logging
import math
import numpy as np

import euclid3 as eu

from idp13.strategy import constants as cons
from idp13.strategy import utils as ut


logger = logging.getLogger('idp13.{}'.format(__name__))
try:
    logger.setLevel(logging.VERBOSE)
except AttributeError:
    logger.setLevel(logging.DEBUG)
    logger.verbose = logger.debug


def _find_clear_route_from_clear(start, arena, target, possible_obstacles,
                                 safe_radius):
    """Attempt to return a list of points which, if followed, will avoid
    obstacles. The final point is the target. The robot starts away from
    obstacles"""
    # Assume the function is used only for returning home, i.e. that there is
    # no need to distinguish a target cell from an obstacle.

    robot_to_target = eu.LineSegment2(start, target)
    path_obstacles = []

    for cell in possible_obstacles:
        if robot_to_target.distance(cell) < safe_radius:
            path_obstacles.append(cell)

    # Direct route
    if len(path_obstacles) <= 0:
        return[target]

    obstacle_circles = []
    for cell in possible_obstacles:
        obstacle_circles.append(eu.Circle(cell, safe_radius))

    # Put out discrete rays at regular angles from both the robot and the
    # target. Halve the angle if it fails?

    robot_rays = []
    target_rays = []
    to_red_line = start.connect(arena.red_line).v
    # 10 degree separation, -90 to +80. @ USER: Change to suit
    angle_range = np.linspace(-math.pi/2, math.pi/2, 18, endpoint=False)

    for angle in angle_range:
        ray = eu.Line2(start, ut.rotate(to_red_line, angle))

        collision = False

        for obstacle in obstacle_circles:
            if ray.intersect(obstacle) is not None:
                collision = True
                break

        if not collision:
            robot_rays.append(ray)

    for angle in angle_range:
        ray = eu.Line2(target, ut.rotate(-to_red_line, angle))

        collision = False

        for obstacle in obstacle_circles:
            if ray.intersect(obstacle) is not None:
                collision = True
                break

        if not collision:
            target_rays.append(ray)

    waypoints = []

    for target_ray in target_rays:
        for robot_ray in robot_rays:
            intersection = target_ray.intersect(robot_ray)
            if intersection is None:  # e.g. parallel lines
                break
            if _traversable(intersection, arena):
                waypoints.append(intersection)

    if len(waypoints) <= 0:
        return [target]  # Replace with further processing?
    if len(waypoints) == 1:
        return [waypoints[0], target]

    # Find the shortest path
    waypoints_and_distances = []

    for waypoint in waypoints:
        distance = start.distance(waypoint) + target.distance(waypoint)

        waypoints_and_distances.append((waypoint, distance))

    waypoint = min(waypoints_and_distances, key=lambda t: t[1])[0]

    return [waypoint, target]


def find_clear_route(robot, arena, target):
    """Attempt to return a list of points which, if followed, will avoid
    obstacles near the red line. The final point is the target. The robot will
    begin by retreating from any obstacles near its start position in a
    direction parallel to the red line.

    Args:
        robot (:obj:`collections.namedtuple`): Container for the robot's pose
        arena (:obj:`detect.arena.arena`): Container for data such as the
            locations of edges and cells
        target (:obj:`euclid3.Point2`): Desired desitination

    Returns:
        :obj:`list` of :obj:`euclid3.Point2`:
        List of waypoints to follow in order, including the target but not the
        startpoint

    """
    cells = arena.untested_cells + arena.active_cells
    safe_radius = 0.625 * cons.Robot.width
    possible_obstacles = []

    for cell in cells:
        if cell.distance(arena.red_line) < cons.Robot.length:
            possible_obstacles.append(cell)
    if not _obstacle_near_point(robot.position, possible_obstacles,
                                safe_radius):
        return _find_clear_route_from_clear(robot.position, arena, target,
                                            possible_obstacles, safe_radius)

    # If there is an obstacle nearby...
    # @ USER: Change length to suit
    increment_vector = arena.red_line.v.normalized() * safe_radius / 2
    # Make this vector point toward the top
    dp = increment_vector.dot(robot.position.connect(arena.edges["top"]).v)
    increment_vector = increment_vector if dp > 0 else -increment_vector

    possible_waypoints = []

    all_waypoints_below_top = True
    all_waypoints_above_bottom = True
    i = 1

    while all_waypoints_below_top or all_waypoints_above_bottom:
        if all_waypoints_below_top:
            # Waypoint above the robot
            possible_waypoint = robot.position + i * increment_vector

            if not _traversable(possible_waypoint, arena):  # Inefficient?
                all_waypoints_below_top = False
            else:
                if not _obstacle_near_point(possible_waypoint,
                                            possible_obstacles, safe_radius):
                    possible_waypoints.append(possible_waypoint)

        if all_waypoints_above_bottom:
            # Waypoint below the robot
            possible_waypoint = robot.position - i * increment_vector

            if not _traversable(possible_waypoint, arena):  # Inefficient?
                all_waypoints_above_bottom = False
            else:
                if not _obstacle_near_point(possible_waypoint,
                                            possible_obstacles, safe_radius):
                    possible_waypoints.append(possible_waypoint)

        if len(possible_waypoints) == 1:
            return possible_waypoints + \
                _find_clear_route_from_clear(possible_waypoints[0], arena,
                                             target, possible_obstacles,
                                             safe_radius)
        elif len(possible_waypoints) == 2:
            # Choose whichever requires the least spinning
            dp = robot.direction.dot(increment_vector)
            waypoint = (
                possible_waypoints[0] if dp > 0 else possible_waypoints[1]
            )
            return [waypoint] + \
                _find_clear_route_from_clear(waypoint, arena, target,
                                             possible_obstacles, safe_radius)
        elif len(possible_waypoints) > 0:
            logger.error("Error finding clear route; likely a bug.")
            return _find_clear_route_from_clear(robot.position, arena, target,
                                                possible_obstacles,
                                                safe_radius)
        i += 1

    logger.warning("No safe path found. Change increment_vector?")
    return _find_clear_route_from_clear(robot.position, arena, target,
                                        possible_obstacles, safe_radius)


def _traversable(point, arena):
    """Return True iff the point can be reached by the robot."""
    for key, edge in arena.edges.items():
        if point.distance(edge) < cons.Robot.length / 2:
            return False

    downwards = arena.edges['top'].p1.connect(arena.edges["bottom"]).v

    if downwards.dot(arena.edges["top"].connect(point).v) < 0:
        return False
    if downwards.dot(arena.edges["bottom"].connect(point).v) > 0:
        return False

    rightwards = arena.edges['left'].p1.connect(arena.edges["right"]).v

    if rightwards.dot(arena.edges["left"].connect(point).v) < 0:
        return False
    if rightwards.dot(arena.edges["right"].connect(point).v) > 0:
        return False

    return True


def _obstacle_near_point(point, possible_obstacles, safe_radius):

    for cell in possible_obstacles:
        if cell.distance(point) < safe_radius:
            return True
