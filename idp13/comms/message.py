"""Arduino communication messages.

This module contains classes and helper functions for creating messages
to send to the arduino or for processing received messages. The message
definition is contained in `message.proto` and compiled to
:mod:`message_pb2`.

"""

__author__ = 'Oli Rose'

import logging
import os
import sys

# Make sure nanopb_pb2 on path
sys.path.insert(0, os.path.split(sys.argv[0])[0])

from idp13.comms import message_pb2 as pb


logger = logging.getLogger('idp13.{}'.format(__name__))
try:
    logger.setLevel(logging.VERBOSE)
except AttributeError:
    logger.setLevel(logging.DEBUG)
    logger.verbose = logger.debug


class Motors:
    """Control both motors."""

    def __init__(self, left=None, right=None, response=False):
        """Store the attributes."""
        self.left = left
        self.right = right
        self.response = response


class Servos:
    """Control both servos."""

    def __init__(self, flipper=None, door=None, response=False):
        """Store the attributes."""
        self.flipper = flipper
        self.door = door
        self.response = response


class Leds:
    """Control the LEDs."""

    def __init__(self, red=None, amber=None, green=None, response=False):
        """Store the attributes."""
        self.red = red
        self.amber = amber
        self.green = green
        self.response = response


class Request:
    """Blank request."""

    def __init__(self):
        """Setup."""
        self.response = True


class Motor:
    """Motor message.

    Args:
        wheel (:obj:`pb._MOTORS_WHEEL`):
            * pb.Motors.LEFT
            * pb.Motors.RIGHT
        speed (:obj:`int`): Between -255 and 255. Positive for forward
        direction (:obj:`pb._MOTORS_DIRECTION`, optional):
            * pb.Motors.STOP
            * pb.Motors.FORWARD
            * pb.Motors.BACKWARD

    """

    _wheel_options = [None] + [_w.number for _w in pb._MOTORS_WHEEL.values]

    def __init__(self, wheel=None, speed=None, direction=None):
        """Create the :obj:`Motor`."""
        self.wheel = wheel
        self.speed = speed
        self.direction = direction

    @property
    def wheel(self):
        """:obj:`pb._MOTORS_WHEEL`: Get or set the wheel."""
        return self._wheel

    @wheel.setter
    def wheel(self, value):
        if value not in self._wheel_options:
            logger.warning('Unexpected wheel option ({})'.format(value))
        self._wheel = value

    @property
    def speed(self):
        """:obj:`int`: Get or set the speed."""
        return self._speed

    @speed.setter
    def speed(self, value):
        if value is None:
            self._speed = None
        else:
            if (value > 255 or value < -255):
                logger.debug(
                    'Motor speed clipped at 255 '
                    '({}: {})'.format(self.wheel, value)
                )
                value = min(255, max(-255, value))
            self._speed = value

    @property
    def direction(self):
        """:obj:`pb._MOTORS_DIRECTION`: Get or set the direction."""
        if self.speed is None:
            return None
        elif self.speed == 0:
            return pb.Motors.STOP
        elif self.speed > 0:
            return pb.Motors.FORWARD
        else:
            return pb.Motors.BACKWARD

    @direction.setter
    def direction(self, value):
        if value is None:
            return
        elif value == pb.Motors.STOP:
            self.speed = 0
        elif value == pb.Motors.FORWARD:
            self.speed = abs(self.speed)
        elif value == pb.Motors.BACKWARD:
            self.speed = -abs(self.speed)
        else:
            logger.warning(
                'Unexpected direction option '
                '({}: {})'.format(self.wheel, value)
            )

    @property
    def msg(self):
        """:obj:`pb.Motors.Motor`: Get the message."""
        if self.wheel is None or self.speed is None:
            return None
        self._msg = pb.Motors.Motor()
        self._msg.wheel = self.wheel
        self._msg.speed = int(abs(self.speed))
        self._msg.direction = self.direction
        return self._msg

    def reverse(self):
        """Reverse the direction of this motor."""
        self.speed = -self.speed


class Servo:
    """Servo message.

    Args:
        option (:obj:`pb._SERVOS_OPTION`):
            * pb.Motors.FLIPPER
            * pb.Motors.DOOR
        angle (:obj:`int`): Angle to set servo to

    Todo:
        Store the limiting angles permitted for the servos

    """

    _option_options = [None] + [_s.number for _s in pb._SERVOS_OPTION.values]

    def __init__(self, option=None, angle=None):
        """Create the :obj:`Servo`."""
        self.option = option
        self.angle = angle

    @property
    def option(self):
        """:obj:`pb._SERVOS_OPTION`: Get or set the servo to control."""
        return self._option

    @option.setter
    def option(self, value):
        if value not in self._option_options:
            logger.warning('Unexpected servo option ({})'.format(value))
        self._option = value

    @property
    def angle(self):
        """:obj:`int`: Get or set the angle."""
        return self._angle

    @angle.setter
    def angle(self, value):
        # TODO limit the servo angle
        # if (value > 255 or value < -255):
        #     logger.warning(
        #         'Servo angle stopped at 180 '
        #         '({}: {})'.format(self.wheel, value)
        #     )
        self._angle = value

    @property
    def msg(self):
        """:obj:`pb.Servos.Servo`: Get the message."""
        if self.option is None or self.angle is None:
            return None
        self._msg = pb.Servos.Servo()
        self._msg.option = self.option
        self._msg.angle = self.angle
        return self._msg


class Led:
    """LED message.

    Args:
        led (:obj:`pb._LEDS_OPTION`):
            * pb.Leds.RED
            * pb.Leds.AMBER
            * pb.Leds.GREEN
        value (:obj:`bool`): True to turn LED on

    """

    led_options = [None] + [_d.number for _d in pb._LEDS_OPTION.values]

    def __init__(self, led=None, value=None):
        """Create the :obj:`Led`."""
        self.led = led
        self.value = value

    @property
    def led(self):
        """:obj:`pb._LEDS_OPTION`: Get or set the LED to control."""
        return self._led

    @led.setter
    def led(self, value):
        if value not in self.led_options:
            logger.warning('Unexpected LED option ({})'.format(value))
        self._led = value

    @property
    def value(self):
        """:obj:`bool`: Get or set the value."""
        return self._value

    @value.setter
    def value(self, val):
        if (val is True) or (val == pb.Leds.HIGH):
            self._value = True
        elif (val is False) or (val == pb.Leds.LOW):
            self._value = False
        else:
            logger.warning(
                'Unexpected LED value '
                '({}: {})'.format(self.led, val)
            )
            self._value = val

    @property
    def msg(self):
        """:obj:`pb.Leds.Led`: Get the message."""
        if self.led is None or self.value is None:
            return None
        self._msg = pb.Leds.Led()
        self._msg.option = self.led
        self._msg.value = self.value
        return self._msg


def handshake(status):
    """Create a Handshake message.

    Args:
        status (:obj:`pb._STATUS`): The type of handshake
            * pb.NONE
            * pb.ASKING
            * pb.RESPONDING
            * pb.COMPLETED
            * pb.INVALID

    Returns:
        :obj:`pb.Message`

    """
    if status not in [s.number for s in pb._STATUS.values]:
        logger.warning('Unexpected handshake option ({})'.format(status))
    msg = pb.Message()
    msg.handshake.type = status
    return msg


def check_handshake(msg, status):
    """Check if a handshake is of a certain type.

    Args:
        msg (:obj:`pb.Message`): The message to check
        status (:obj:`pb._STATUS`): The expected status
            * pb.NONE
            * pb.ASKING
            * pb.RESPONDING
            * pb.COMPLETED
            * pb.INVALID

    Returns:
        bool: True if ``msg`` is a handshake message with ``status``

    """
    if msg is None:
        return False
    else:
        return (
            msg.WhichOneof('type') == 'handshake'
            and msg.handshake.type == status
        )


def _assemble(poss_list, other_args):
    """Assemble a single list of all args passed in."""
    if not isinstance(poss_list, list):
        poss_list = [poss_list]
    if other_args is not None:
        poss_list.extend(list(other_args))
    return poss_list


def motors(ms, response=False, *args):
    """Create a Motors message.

    Args:
        ms (:obj:`Motor` or :obj:`array` of :obj:`Motor`): One or more
            motor objects to be controlled
        *args (:obj:`Motor`): Further motor objects (added to ``ms``)

    Returns:
        :obj:`pb.Message`

    """
    ms = _assemble(ms, args)
    msg = pb.Message()
    msg.call.status = pb.RESPONDING if response else pb.ASKING
    for m in ms:
        if m.msg is not None:
            msg.call.motors.motors.extend([m.msg])
    return msg


def servos(ss, response=False, *args):
    """Create a Servos message.

    Args:
        ss (:obj:`Servo` or :obj:`array` of :obj:`Servo`): One or more
            servo objects to be controlled
        *args (:obj:`Servo`): Further servo objects (added to ``ss``)

    Returns:
        :obj:`pb.Message`

    """
    ss = _assemble(ss, args)
    msg = pb.Message()
    msg.call.status = pb.RESPONDING if response else pb.ASKING
    for s in ss:
        if s.msg is not None:
            msg.call.servos.servos.extend([s.msg])
    return msg


def leds(ds, response=False, *args):
    """Create a LEDs message.

    Args:
        ds (:obj:`Led` or :obj:`array` of :obj:`Led`): One or more
            LED objects to be controlled
        *args (:obj:`Led`): Further LED objects (added to ``ds``)

    Returns:
        :obj:`pb.Message`

    """
    ds = _assemble(ds, args)
    msg = pb.Message()
    msg.call.status = pb.RESPONDING if response else pb.ASKING
    for d in ds:
        if d.msg is not None:
            msg.call.leds.leds.extend([d.msg])
    return msg


def request():
    """Create a blank message to request a sensor reading.

    Returns:
        :obj:`pb.Message`

    """
    msg = pb.Message()
    msg.call.status = pb.RESPONDING
    return msg
