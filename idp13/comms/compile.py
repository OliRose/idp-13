"""Compile the latest `message.proto` file.

Can use command line args, manual entry or a config file to specify the
path to a virtualenv with requirements.txt installed and the path to
a nanopb installation

.compile-config.ini
-------------------
[PATHS]
env=<path/to/env/>
nanopb=<path/to/nanopb/>

"""

__author__ = 'Oli Rose'

import configparser
import logging
import os
import subprocess
import sys


logger = logging.getLogger('idp13.{}'.format(__name__))
logging.basicConfig(level=logging.INFO)


def compile():
    """Compile."""
    env = None
    nanopb = None

    if len(sys.argv) > 3:
        env = os.path.abspath(sys.argv[1])
        nanopb = os.path.abspath(sys.argv[2])
        logger.info('Using paths provided on command line')
        logger.info('    env: {}'.format(env))
        logger.info('    nanopb: {}'.format(nanopb))
    else:
        try:
            config = configparser.ConfigParser()
            config.read('.compile-config.ini')
            env = os.path.abspath(config['PATHS']['env'])
            nanopb = os.path.abspath(config['PATHS']['nanopb'])
            logger.info('Using paths from config file:')
            logger.info('    env: {}'.format(env))
            logger.info('    nanopb: {}'.format(nanopb))
        except KeyError as e:
            logger.error('Invalid config file:')
            logger.error('    missing key: {}'.format(e.args[0]))
    if env is None:
        logger.debug('Using manual env path entry')
        env = input('Path to venv: ')
        if env == '':
            sys.exit(0)
        env = os.path.abspath(env)
    if nanopb is None:
        logger.debug('Using manual nanopb path entry')
        nanopb = os.path.abspath(input('Path to /nanopb: '))

    env = os.path.join(env, 'Scripts', 'activate.bat')
    nanopb_proto = os.path.join(nanopb, 'generator', 'proto')
    nanopb = os.path.join(nanopb, 'generator', 'nanopb_generator.py')

    logger.info('Creating python module')
    subprocess.run(
        ['protoc', '--python_out=.', 'message.proto',
         '-I.', '-I{}'.format(nanopb_proto)],
        shell=True
    )
    logger.info('Creating arduino code')
    subprocess.run(
        ['protoc', '-o../../arduino/src/comms/message.pb', 'message.proto',
         '-I.', '-I{}'.format(nanopb_proto)],
        shell=True
    )
    logger.info('Creating nanopb files')
    subprocess.run(
        '"{}"&cd "../../arduino/src/comms"&python '
        '{} message.pb'.format(env, nanopb)
    )


if __name__ == '__main__':
    compile()
