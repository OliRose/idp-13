"""Serial communications with the arduino.

This module implements the communication with the arduino using
the protocol defined in `message.proto`.

"""

__author__ = 'Oli Rose'

import logging
import os
import serial
import sys
import threading
import time

from idp13 import utils

# Make sure nanopb_pb2 on path
sys.path.insert(0, os.path.split(sys.argv[0])[0])

from idp13.comms import (
    message,
    message_pb2 as pb
)
from idp13.strategy import path_follower


logger = logging.getLogger('idp13.{}'.format(__name__))
try:
    logger.setLevel(logging.VERBOSE)
except AttributeError:
    logger.setLevel(logging.DEBUG)
    logger.verbose = logger.debug

MIN_WRITE_TIME = 0.01


class Link:
    """PC <-> arduino comms link.

    Args:
        port (:obj:`str`): COM port of arduino (e.g. ``'COM4'``)
        baud_rate (:obj:`int`)

    """

    def __init__(self, port='COM3', baud_rate=57600):
        """Create the :obj:`Link`."""
        self._last_write = time.time()
        self._open(port, baud_rate)

    def _open(self, port, baud_rate):
        """Open the serial link."""
        self._ser = serial.Serial(port, baud_rate)
        time.sleep(1)

    def close(self):
        """Close the serial link."""
        try:
            self._ser.close()
        except AttributeError:
            pass
        self._ser = None

    def __enter__(self):
        """Enter the context manager."""
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        """Exit the context manager."""
        self.close()

    def read(self, timeout=None):
        """Read a Message from the arduino.

        Args:
            timeout (:obj:`int`): Timeout for Serial read (in seconds)

        Returns:
            :obj:`pb.Message` if valid else ``None``

        Raises:
            Protobuf errors

        Todo:
            Determine potential errors raised

        """
        logger.debug('Reading message:')
        try:
            if timeout:
                self._ser.timeout = timeout
            data = []
            try:
                length = ord(self._ser.read())
            except TypeError:
                logger.debug('    TIMED OUT: no length')
                return None
            for _ in range(0, length):
                data.append(self._ser.read())
            if len(data) == length:
                msg = pb.Message()
                try:
                    msg.ParseFromString(b''.join(data))
                except Exception:
                    logger.exception('')
                else:
                    logger.debug('    {}'.format(msg))
                    return msg
            else:
                logger.debug('    TIMED OUT: {}/{}'.format(len(data), length))
                return None
        except Exception:
            # A number of failure states possible - TODO
            logger.exception('')
            return None

    def _write(self, msg):
        """Write a Message to the arduino.

        Args:
            msg (:obj:`pb.Message`)

        Raises:
            Protobuf errors

        Todo:
            Determine potential errors raised

        """
        while time.time() - self._last_write < MIN_WRITE_TIME:
            # Make sure we don't write too many messages too soon
            pass
        logger.debug('Writing message: {}'.format(msg))
        try:
            data = msg.SerializeToString()
            length = len(data)
            self._ser.write(bytes(chr(length), 'utf-8'))
            self._ser.write(data)
        except Exception:
            # A number of failure states possible - TODO
            logger.exception('')
            return False
        self._last_write = time.time()
        return True

    def write(self, msg):
        """Write a message until it succeeds."""
        passed = self._write(msg)
        while not passed:
            logger.warning('Write to arduino failed')
            passed = self._write(msg)

    def handshake(self, timeout=5, serial_timeout=1):
        """Perform a handshake operation with the arduino.

        Block until a handshake operation achieved as follows:
            >>> ASKING
            <<< RESPONDING
            >>> COMPLETED
            <<< COMPLETED

        Args:
            serial_timeout (:obj:`int`): Timeout for Serial read (in seconds)
            timeout

        """
        end_time = time.time() + timeout
        logger.info('Attempting handshake')
        while time.time() < end_time:
            while time.time() < end_time:
                logger.info('>>> ASKING')
                self.write(message.handshake(pb.ASKING))
                response = self.read(serial_timeout)
                if message.check_handshake(response, pb.RESPONDING):
                    logger.info('<<< RESPONDING')
                    break
                else:
                    logger.info('<!< RESPONDING')
            logger.info('>>> COMPLETED')
            self.write(message.handshake(pb.COMPLETED))
            response = self.read(serial_timeout)
            if message.check_handshake(response, pb.COMPLETED):
                logger.info('<<< COMPLETED')
                break
            else:
                logger.info('<!< COMPLETED')

    def control_motors(self, left=None, right=None, response=False):
        """Control the motors.

        Args:
            left (:obj:`int`, optional): Speed for the left wheel
            right (:obj:`int`, optional)

        """
        right = message.Motor(pb.Motors.RIGHT, right)
        left = message.Motor(pb.Motors.LEFT, left)
        msg = message.motors(left, right, response=response)
        self.write(msg)

    def control_servos(self, flipper=None, door=None, response=False):
        """Control the servos.

        Args:
            flipper (:obj:`int`, optional): Angle for the flipper
                (from default)
            door (:obj:`int`, optional)

        """
        flipper = message.Servo(pb.Servos.FLIPPER, flipper)
        door = message.Servo(pb.Servos.DOOR, door)
        msg = message.servos(flipper, door, response=response)
        self.write(msg)

    def control_leds(self, red=None, amber=None, green=None, response=False):
        """Control the LEDs.

        Args:
            red (:obj:`bool`, optional): True to turn red LED on
            amber (:obj:`bool`, optional)
            green (:obj:`bool`, optional)

        """
        leds = [
            message.Led(pb.Leds.RED, red),
            message.Led(pb.Leds.AMBER, amber),
            message.Led(pb.Leds.GREEN, green),
        ]
        self.write(message.leds(leds, response=response))

    def send_request(self):
        """Send an empty message to request a sensor reading."""
        self.write(message.request())

    def get_response(self, timeout=1):
        """Get and process a response from the arduino."""
        response = self.read(timeout=timeout)
        if response is None:
            logger.warning('get_response timed out')
            return None
        elif response.WhichOneof('type') != 'response':
            # Invalid message
            logger.warning('get_response got an invalid message')
            return None
        else:
            logger.debug('get_response: Safe: {}, Active: {}'.format(
                response.response.safe, response.response.active
            ))
            return response.response.safe, response.response.active


class LinkThread(utils.StoppableThread):
    """Control the arduino from a deque of commands."""

    def __init__(self, link, command_q=None):
        """Create the :obj:`LinkThread`."""
        self.link = link
        # Setup the safe and active values (how many have been detected)
        self.safe = 0
        self.active = 0
        # Track whether the last read passed
        self.last_read = None
        if command_q is not None:
            self.q = command_q
        self._pause_event = threading.Event()
        self._single_command_completed = threading.Event()
        self._single_command_completed.set()
        self._single_command = None
        super().__init__(name='LinkThread')

    def setup(self):
        """Setup the link."""
        self.link.handshake()

    def loop(self):
        """Main loop."""
        if not self._single_command_completed.is_set():
            logger.verbose('Single command to run')
            # We have a single command to run
            command = self._single_command
            self._single_command_completed.set()
            self._single_command = None
        elif self._pause_event.is_set():
            logger.verbose('Paused in comms loop')
            return
        else:
            command = self.q.pop()
        response = False
        response_requested = False
        if command is None:
            # Nothing to send
            return
        elif command is False:
            # Request to stop the robot
            self.stop_robot()
        elif isinstance(command, path_follower.Output):
            self._path_follower_cmd(command)
        else:
            response_requested = self._other_cmd(command)
        if response_requested:
            response = self.link.get_response()
            if response is None:
                self.last_read = False
            else:
                # Store the data from the arduino
                self.last_read = True
                self.safe, self.active = response

    def stop(self):
        """Override stop."""
        self.pause()
        time.sleep(0.1)     # Make sure everything's finished
        super().stop()
        self.link.close()

    def _path_follower_cmd(self, command):
        """Process a direct output from the path follower."""
        if self._pause_event.is_set():
            self.unpause()
        # TODO
        logger.debug('Path follower command: {}'.format(command))
        self.link.control_motors(
            left=command.left,
            right=command.right,
        )

    def _other_cmd(self, command):
        """Process a different control command."""
        try:
            response = command.response
        except AttributeError:
            return False
        if isinstance(command, message.Motors):
            self.link.control_motors(
                left=command.left, right=command.right, response=response
            )
        elif isinstance(command, message.Servos):
            self.link.control_servos(
                flipper=command.flipper, door=command.door, response=response
            )
        elif isinstance(command, message.Leds):
            self.link.control_leds(
                red=command.red, amber=command.amber,
                green=command.green, response=response
            )
        elif response:
            self.link.send_request()
        return response

    def pause(self):
        """Pause the command queue."""
        logger.debug('Pausing command queue control')
        self._pause_event.set()

    def unpause(self):
        """Unpause the command queue."""
        logger.debug('Unpausing command queue control')
        self._pause_event.clear()

    @property
    def paused(self):
        """Get or set whether the robot is paused."""
        return self._pause_event.is_set()

    @paused.setter
    def paused(self, value):
        if value:
            self._pause_event.set()
        else:
            self._pause_event.clear()

    def swap_command_q(self, q):
        """Swap the command queue for a new one."""
        logger.info('Swapping command queue')
        self.pause()
        self.q = q
        self.unpause()

    def single_command(self, cmd):
        """Run a single command."""
        self._single_command = cmd
        self._single_command_completed.clear()
        logger.debug('Running single command: {}'.format(type(cmd).__name__))
        self._single_command_completed.wait()

    def stop_robot(self):
        """Stop the robot."""
        logger.debug('Stopping the robot')
        self.single_command(message.Motors(0, 0))
        logger.debug('Robot stopped')

    def restart_robot(self):
        """Restart the robot."""
        logger.debug('Restarting the robot')
        self.unpause()

    def flipper(self, angle=0):
        """Move the flipper."""
        logger.info('Moving flipper to {}'.format(angle))
        self.single_command(message.Servos(flipper=angle))

    def open_door(self):
        """Open the door."""
        logger.info('Opening door')
        self.single_command(message.Servos(door=180))

    def close_door(self):
        """Close the door."""
        logger.info('Closing door')
        self.single_command(message.Servos(door=0))

    def leds(self, red=None, amber=None, green=None):
        """Control the LEDs."""
        self.single_command(message.Leds(red=red, amber=amber, green=green))

    def request(self):
        """Send a blank message and get a response."""
        self.pause()
        self.link.send_request()
        response = self.link.get_response()
        self.unpause()
        return response
