# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: message.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from idp13.comms import nanopb_pb2 as nanopb__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='message.proto',
  package='',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\rmessage.proto\x1a\x0cnanopb.proto\"h\n\x07Message\x12\x1f\n\thandshake\x18\x01 \x01(\x0b\x32\n.HandshakeH\x00\x12\x15\n\x04\x63\x61ll\x18\x02 \x01(\x0b\x32\x05.CallH\x00\x12\x1d\n\x08response\x18\x03 \x01(\x0b\x32\t.ResponseH\x00\x42\x06\n\x04type\"\"\n\tHandshake\x12\x15\n\x04type\x18\x01 \x01(\x0e\x32\x07.Status\"t\n\x04\x43\x61ll\x12\x17\n\x06status\x18\x01 \x01(\x0e\x32\x07.Status\x12\x19\n\x06motors\x18\x02 \x01(\x0b\x32\x07.MotorsH\x00\x12\x19\n\x06servos\x18\x03 \x01(\x0b\x32\x07.ServosH\x00\x12\x15\n\x04leds\x18\x04 \x01(\x0b\x32\x05.LedsH\x00\x42\x06\n\x04type\"(\n\x08Response\x12\x0c\n\x04safe\x18\x01 \x01(\x05\x12\x0e\n\x06\x61\x63tive\x18\x02 \x01(\x05\"\xe0\x01\n\x06Motors\x12*\n\x06motors\x18\x01 \x03(\x0b\x32\r.Motors.MotorB\x0b\x92?\x02\x10\x02\x92?\x03\x80\x01\x01\x1aZ\n\x05Motor\x12\x1c\n\x05wheel\x18\x01 \x01(\x0e\x32\r.Motors.Wheel\x12$\n\tdirection\x18\x02 \x01(\x0e\x32\x11.Motors.Direction\x12\r\n\x05speed\x18\x03 \x01(\x05\"\x1c\n\x05Wheel\x12\x08\n\x04LEFT\x10\x00\x12\t\n\x05RIGHT\x10\x01\"0\n\tDirection\x12\x08\n\x04STOP\x10\x00\x12\x0b\n\x07\x46ORWARD\x10\x01\x12\x0c\n\x08\x42\x41\x43KWARD\x10\x02\"\x8d\x01\n\x06Servos\x12*\n\x06servos\x18\x01 \x03(\x0b\x32\r.Servos.ServoB\x0b\x92?\x02\x10\x02\x92?\x03\x80\x01\x01\x1a\x36\n\x05Servo\x12\x1e\n\x06option\x18\x01 \x01(\x0e\x32\x0e.Servos.Option\x12\r\n\x05\x61ngle\x18\x02 \x01(\x05\"\x1f\n\x06Option\x12\x0b\n\x07\x46LIPPER\x10\x00\x12\x08\n\x04\x44OOR\x10\x01\"\xb2\x01\n\x04Leds\x12$\n\x04leds\x18\x01 \x03(\x0b\x32\t.Leds.LedB\x0b\x92?\x02\x10\x03\x92?\x03\x80\x01\x01\x1a?\n\x03Led\x12\x1c\n\x06option\x18\x01 \x01(\x0e\x32\x0c.Leds.Option\x12\x1a\n\x05value\x18\x02 \x01(\x0e\x32\x0b.Leds.Value\"\'\n\x06Option\x12\x07\n\x03RED\x10\x00\x12\t\n\x05\x41MBER\x10\x01\x12\t\n\x05GREEN\x10\x02\"\x1a\n\x05Value\x12\x07\n\x03LOW\x10\x00\x12\x08\n\x04HIGH\x10\x01*=\n\x06Status\x12\x08\n\x04NONE\x10\x00\x12\n\n\x06\x41SKING\x10\x01\x12\x0e\n\nRESPONDING\x10\x02\x12\r\n\tCOMPLETED\x10\x03\x62\x06proto3')
  ,
  dependencies=[nanopb__pb2.DESCRIPTOR,])

_STATUS = _descriptor.EnumDescriptor(
  name='Status',
  full_name='Status',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='NONE', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ASKING', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RESPONDING', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='COMPLETED', index=3, number=3,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=885,
  serialized_end=946,
)
_sym_db.RegisterEnumDescriptor(_STATUS)

Status = enum_type_wrapper.EnumTypeWrapper(_STATUS)
NONE = 0
ASKING = 1
RESPONDING = 2
COMPLETED = 3


_MOTORS_WHEEL = _descriptor.EnumDescriptor(
  name='Wheel',
  full_name='Motors.Wheel',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='LEFT', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RIGHT', index=1, number=1,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=480,
  serialized_end=508,
)
_sym_db.RegisterEnumDescriptor(_MOTORS_WHEEL)

_MOTORS_DIRECTION = _descriptor.EnumDescriptor(
  name='Direction',
  full_name='Motors.Direction',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='STOP', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='FORWARD', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BACKWARD', index=2, number=2,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=510,
  serialized_end=558,
)
_sym_db.RegisterEnumDescriptor(_MOTORS_DIRECTION)

_SERVOS_OPTION = _descriptor.EnumDescriptor(
  name='Option',
  full_name='Servos.Option',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='FLIPPER', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DOOR', index=1, number=1,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=671,
  serialized_end=702,
)
_sym_db.RegisterEnumDescriptor(_SERVOS_OPTION)

_LEDS_OPTION = _descriptor.EnumDescriptor(
  name='Option',
  full_name='Leds.Option',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='RED', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='AMBER', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GREEN', index=2, number=2,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=816,
  serialized_end=855,
)
_sym_db.RegisterEnumDescriptor(_LEDS_OPTION)

_LEDS_VALUE = _descriptor.EnumDescriptor(
  name='Value',
  full_name='Leds.Value',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='LOW', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='HIGH', index=1, number=1,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=857,
  serialized_end=883,
)
_sym_db.RegisterEnumDescriptor(_LEDS_VALUE)


_MESSAGE = _descriptor.Descriptor(
  name='Message',
  full_name='Message',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='handshake', full_name='Message.handshake', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='call', full_name='Message.call', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='response', full_name='Message.response', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='type', full_name='Message.type',
      index=0, containing_type=None, fields=[]),
  ],
  serialized_start=31,
  serialized_end=135,
)


_HANDSHAKE = _descriptor.Descriptor(
  name='Handshake',
  full_name='Handshake',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='type', full_name='Handshake.type', index=0,
      number=1, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=137,
  serialized_end=171,
)


_CALL = _descriptor.Descriptor(
  name='Call',
  full_name='Call',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='status', full_name='Call.status', index=0,
      number=1, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='motors', full_name='Call.motors', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='servos', full_name='Call.servos', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='leds', full_name='Call.leds', index=3,
      number=4, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='type', full_name='Call.type',
      index=0, containing_type=None, fields=[]),
  ],
  serialized_start=173,
  serialized_end=289,
)


_RESPONSE = _descriptor.Descriptor(
  name='Response',
  full_name='Response',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='safe', full_name='Response.safe', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='active', full_name='Response.active', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=291,
  serialized_end=331,
)


_MOTORS_MOTOR = _descriptor.Descriptor(
  name='Motor',
  full_name='Motors.Motor',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='wheel', full_name='Motors.Motor.wheel', index=0,
      number=1, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='direction', full_name='Motors.Motor.direction', index=1,
      number=2, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='speed', full_name='Motors.Motor.speed', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=388,
  serialized_end=478,
)

_MOTORS = _descriptor.Descriptor(
  name='Motors',
  full_name='Motors',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='motors', full_name='Motors.motors', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=_b('\222?\002\020\002\222?\003\200\001\001'), file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[_MOTORS_MOTOR, ],
  enum_types=[
    _MOTORS_WHEEL,
    _MOTORS_DIRECTION,
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=334,
  serialized_end=558,
)


_SERVOS_SERVO = _descriptor.Descriptor(
  name='Servo',
  full_name='Servos.Servo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='option', full_name='Servos.Servo.option', index=0,
      number=1, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='angle', full_name='Servos.Servo.angle', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=615,
  serialized_end=669,
)

_SERVOS = _descriptor.Descriptor(
  name='Servos',
  full_name='Servos',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='servos', full_name='Servos.servos', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=_b('\222?\002\020\002\222?\003\200\001\001'), file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[_SERVOS_SERVO, ],
  enum_types=[
    _SERVOS_OPTION,
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=561,
  serialized_end=702,
)


_LEDS_LED = _descriptor.Descriptor(
  name='Led',
  full_name='Leds.Led',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='option', full_name='Leds.Led.option', index=0,
      number=1, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='value', full_name='Leds.Led.value', index=1,
      number=2, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=751,
  serialized_end=814,
)

_LEDS = _descriptor.Descriptor(
  name='Leds',
  full_name='Leds',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='leds', full_name='Leds.leds', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=_b('\222?\002\020\003\222?\003\200\001\001'), file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[_LEDS_LED, ],
  enum_types=[
    _LEDS_OPTION,
    _LEDS_VALUE,
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=705,
  serialized_end=883,
)

_MESSAGE.fields_by_name['handshake'].message_type = _HANDSHAKE
_MESSAGE.fields_by_name['call'].message_type = _CALL
_MESSAGE.fields_by_name['response'].message_type = _RESPONSE
_MESSAGE.oneofs_by_name['type'].fields.append(
  _MESSAGE.fields_by_name['handshake'])
_MESSAGE.fields_by_name['handshake'].containing_oneof = _MESSAGE.oneofs_by_name['type']
_MESSAGE.oneofs_by_name['type'].fields.append(
  _MESSAGE.fields_by_name['call'])
_MESSAGE.fields_by_name['call'].containing_oneof = _MESSAGE.oneofs_by_name['type']
_MESSAGE.oneofs_by_name['type'].fields.append(
  _MESSAGE.fields_by_name['response'])
_MESSAGE.fields_by_name['response'].containing_oneof = _MESSAGE.oneofs_by_name['type']
_HANDSHAKE.fields_by_name['type'].enum_type = _STATUS
_CALL.fields_by_name['status'].enum_type = _STATUS
_CALL.fields_by_name['motors'].message_type = _MOTORS
_CALL.fields_by_name['servos'].message_type = _SERVOS
_CALL.fields_by_name['leds'].message_type = _LEDS
_CALL.oneofs_by_name['type'].fields.append(
  _CALL.fields_by_name['motors'])
_CALL.fields_by_name['motors'].containing_oneof = _CALL.oneofs_by_name['type']
_CALL.oneofs_by_name['type'].fields.append(
  _CALL.fields_by_name['servos'])
_CALL.fields_by_name['servos'].containing_oneof = _CALL.oneofs_by_name['type']
_CALL.oneofs_by_name['type'].fields.append(
  _CALL.fields_by_name['leds'])
_CALL.fields_by_name['leds'].containing_oneof = _CALL.oneofs_by_name['type']
_MOTORS_MOTOR.fields_by_name['wheel'].enum_type = _MOTORS_WHEEL
_MOTORS_MOTOR.fields_by_name['direction'].enum_type = _MOTORS_DIRECTION
_MOTORS_MOTOR.containing_type = _MOTORS
_MOTORS.fields_by_name['motors'].message_type = _MOTORS_MOTOR
_MOTORS_WHEEL.containing_type = _MOTORS
_MOTORS_DIRECTION.containing_type = _MOTORS
_SERVOS_SERVO.fields_by_name['option'].enum_type = _SERVOS_OPTION
_SERVOS_SERVO.containing_type = _SERVOS
_SERVOS.fields_by_name['servos'].message_type = _SERVOS_SERVO
_SERVOS_OPTION.containing_type = _SERVOS
_LEDS_LED.fields_by_name['option'].enum_type = _LEDS_OPTION
_LEDS_LED.fields_by_name['value'].enum_type = _LEDS_VALUE
_LEDS_LED.containing_type = _LEDS
_LEDS.fields_by_name['leds'].message_type = _LEDS_LED
_LEDS_OPTION.containing_type = _LEDS
_LEDS_VALUE.containing_type = _LEDS
DESCRIPTOR.message_types_by_name['Message'] = _MESSAGE
DESCRIPTOR.message_types_by_name['Handshake'] = _HANDSHAKE
DESCRIPTOR.message_types_by_name['Call'] = _CALL
DESCRIPTOR.message_types_by_name['Response'] = _RESPONSE
DESCRIPTOR.message_types_by_name['Motors'] = _MOTORS
DESCRIPTOR.message_types_by_name['Servos'] = _SERVOS
DESCRIPTOR.message_types_by_name['Leds'] = _LEDS
DESCRIPTOR.enum_types_by_name['Status'] = _STATUS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Message = _reflection.GeneratedProtocolMessageType('Message', (_message.Message,), dict(
  DESCRIPTOR = _MESSAGE,
  __module__ = 'message_pb2'
  # @@protoc_insertion_point(class_scope:Message)
  ))
_sym_db.RegisterMessage(Message)

Handshake = _reflection.GeneratedProtocolMessageType('Handshake', (_message.Message,), dict(
  DESCRIPTOR = _HANDSHAKE,
  __module__ = 'message_pb2'
  # @@protoc_insertion_point(class_scope:Handshake)
  ))
_sym_db.RegisterMessage(Handshake)

Call = _reflection.GeneratedProtocolMessageType('Call', (_message.Message,), dict(
  DESCRIPTOR = _CALL,
  __module__ = 'message_pb2'
  # @@protoc_insertion_point(class_scope:Call)
  ))
_sym_db.RegisterMessage(Call)

Response = _reflection.GeneratedProtocolMessageType('Response', (_message.Message,), dict(
  DESCRIPTOR = _RESPONSE,
  __module__ = 'message_pb2'
  # @@protoc_insertion_point(class_scope:Response)
  ))
_sym_db.RegisterMessage(Response)

Motors = _reflection.GeneratedProtocolMessageType('Motors', (_message.Message,), dict(

  Motor = _reflection.GeneratedProtocolMessageType('Motor', (_message.Message,), dict(
    DESCRIPTOR = _MOTORS_MOTOR,
    __module__ = 'message_pb2'
    # @@protoc_insertion_point(class_scope:Motors.Motor)
    ))
  ,
  DESCRIPTOR = _MOTORS,
  __module__ = 'message_pb2'
  # @@protoc_insertion_point(class_scope:Motors)
  ))
_sym_db.RegisterMessage(Motors)
_sym_db.RegisterMessage(Motors.Motor)

Servos = _reflection.GeneratedProtocolMessageType('Servos', (_message.Message,), dict(

  Servo = _reflection.GeneratedProtocolMessageType('Servo', (_message.Message,), dict(
    DESCRIPTOR = _SERVOS_SERVO,
    __module__ = 'message_pb2'
    # @@protoc_insertion_point(class_scope:Servos.Servo)
    ))
  ,
  DESCRIPTOR = _SERVOS,
  __module__ = 'message_pb2'
  # @@protoc_insertion_point(class_scope:Servos)
  ))
_sym_db.RegisterMessage(Servos)
_sym_db.RegisterMessage(Servos.Servo)

Leds = _reflection.GeneratedProtocolMessageType('Leds', (_message.Message,), dict(

  Led = _reflection.GeneratedProtocolMessageType('Led', (_message.Message,), dict(
    DESCRIPTOR = _LEDS_LED,
    __module__ = 'message_pb2'
    # @@protoc_insertion_point(class_scope:Leds.Led)
    ))
  ,
  DESCRIPTOR = _LEDS,
  __module__ = 'message_pb2'
  # @@protoc_insertion_point(class_scope:Leds)
  ))
_sym_db.RegisterMessage(Leds)
_sym_db.RegisterMessage(Leds.Led)


_MOTORS.fields_by_name['motors']._options = None
_SERVOS.fields_by_name['servos']._options = None
_LEDS.fields_by_name['leds']._options = None
# @@protoc_insertion_point(module_scope)
