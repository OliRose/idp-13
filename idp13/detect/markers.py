"""Find and analyse markers."""

__author__ = 'Oli Rose'

import itertools
import logging
import math

import cv2

from idp13.detect import vectors as vec

logger = logging.getLogger('idp13.{}'.format(__name__))
try:
    logger.setLevel(logging.VERBOSE)
except AttributeError:
    logger.setLevel(logging.DEBUG)
    logger.verbose = logger.debug

MAX_MARKER = 3
DISTANCE_SCALE = 4

DISTANCE_FROM_CENTRE = [0.665, 0.665, 1.1]


class Marker:
    """Robot marker."""

    tol = 0.6
    rules = {
        'outer': {
            'stacked': 2,
            'direct_children': 1,
            'total_children': range(3, 3 + MAX_MARKER),
            'area': 1,
            'peri': 1,
        },
        'inner': {
            'stacked': 1,
            'direct_children': range(2, 2 + MAX_MARKER),
            'area': 9 / 16,
            'peri': 3 / 4,
        },
        'bar': {
            'stacked': 0,
            'direct_children': 0,
            'area': 1 / 16,
            'peri': 5 / 16,
        },
        'idents': {
            'stacked': 0,
            'direct_children': 0,
            'area': 0.0123,
            'peri': 0.0982,
        }
    }

    def __init__(self, outer, inner=None, bar=None, idents=None):
        """Create the marker from the various Nodes."""
        self.outer = outer
        self.inner = inner
        self.bar = bar
        self.idents = idents
        # Internal values
        self._centre = None
        self._direction = None
        self._pose = None
        self._length = None

    def __str__(self):
        """Print."""
        try:
            return '{}.{}<{}>'.format(
                __name__, self.__class__.__name__, self.n,
            )
        except AttributeError:
            return '{}.{}<>'.format(__name__, self.__class__.__name__)

    def __repr__(self):
        """Print."""
        return self.__str__()

    def full_print(self):
        """Print the full info."""
        return '{}.{}<{}>({})'.format(
            __name__, self.__class__.__name__, self.n, self.pose
        )

    @property
    def centre(self):
        """Get the positional data of the marker."""
        if self._centre is None:
            try:
                self._find_pose()
            except AttributeError:
                pass
        return self._centre

    @property
    def direction(self):
        """Get the positional data of the marker."""
        if self._direction is None:
            try:
                self._find_pose()
            except AttributeError:
                pass
        return self._direction

    @property
    def pose(self):
        """Get the positional data of the marker."""
        if self._pose is None:
            try:
                self._find_pose()
            except AttributeError:
                pass
        return self._pose

    @property
    def length(self):
        """Get the positional data of the marker."""
        if self._length is None:
            try:
                self._find_pose()
            except AttributeError:
                pass
        return self._length

    @property
    def n(self):
        """Number of the marker."""
        try:
            return len(self.idents)
        except TypeError:
            return None

    def _check_rules(self, node, layer):
        """Check whether a node passes the rule for a layer."""
        res = {}
        for k, v in self.rules[layer].items():
            res[k] = getattr(node, k)
            if isinstance(v, str):
                v = res[v]
            if k in ['stacked', 'direct_children', 'total_children']:
                try:
                    if res[k] not in v:
                        return False
                except TypeError:
                    if res[k] != v:
                        return False
            else:
                # Test areas or perimeters
                r = res[k] / getattr(self.outer, k)
                if not math.isclose(r, v, rel_tol=Marker.tol):
                    return False
        return True

    def _find_bar(self, children):
        """Find possible bar contours."""
        children = children.copy()
        poss_bars = []
        for i, child in enumerate(children):
            if self._check_rules(child, 'bar'):
                poss_bars.append(i)
        if len(poss_bars) == 1:
            self.bar = children.pop(poss_bars[0])
            self.idents = children
        else:
            self.bar = None
            self.idents = None

    def _find_pose(self):
        """Find the pose of the marker."""
        self._centre = sum(
            [self.outer.centre, self.inner.centre], vec.Point()) / 2
        # Get the bar bounding rect to find direction
        r = vec.Rectangle(cv2.boxPoints(cv2.minAreaRect(self.bar.contour)))
        direction = r.long_side
        # Get the average centre of the idents to work out orientation
        idents_p = sum([i.centre for i in self.idents],
                       vec.Point()) / len(self.idents)
        vec_to_centre = self._centre - idents_p
        pos = (vec_to_centre - direction).length
        neg = (vec_to_centre + direction).length
        if pos < neg:
            self._direction = direction
        elif pos > neg:
            # Flip the direction around
            self._direction = - direction
        else:
            logger.warning('Symmetrical marker found, ignoring')
            self._direction = None
            self._pose = None
            return
        # Marker can be at most two marker widths from the centre of the robot
        r = vec.Rectangle(cv2.boxPoints(cv2.minAreaRect(self.outer.contour)))
        self._length = r.long_side.length
        self._pose = vec.LineSegment(
            self._centre, self._direction, DISTANCE_SCALE * self._length
        )
        return self

    def _check_centre(self, marker, p, scale=2):
        """Check that a point lies within scale * marker_width of both."""
        if p.distance(self.centre) > self.length * scale:
            return False
        elif p.distance(marker.centre) > self.length * scale:
            return False
        else:
            return True

    def find_centre_with_marker(self, marker):
        """Find the centre using this and another marker."""
        try:
            # Try and find an intersection
            p = self.pose.intersect(marker.pose)
        except AttributeError:
            # One of the markers doesn't have a pose/has not been found
            return 'none', None
        if p is not None:
            logger.verbose('INTERSECTION: {}, {}'.format(self.n, marker.n))
            return 'intersect', p
        # Use the average centre method
        p = sum([self.centre, marker.centre], vec.Point()) / 2
        if self._check_centre(marker, p):
            logger.verbose('AVERAGE: {}, {}'.format(self.n, marker.n))
            return 'average', p
        # Both methods failed
        logger.warning('Failed centre between:')
        logger.warning('    {}'.format(self.full_print()))
        logger.warning('    {}'.format(marker.full_print()))
        return 'none', None

    def find_centre_by_distance(self):
        """Find the centre of the robot based on one marker."""
        try:
            distance = DISTANCE_FROM_CENTRE[self.n-1] * self._length
        except (AttributeError, IndexError):
            return None
        else:
            return (self._centre + self._direction.unit() * distance)


class Markers:
    """Store and process the markers in the image."""

    def __init__(self, markers):
        """Sort and store the markers."""
        self.markers = [None] * (MAX_MARKER + 1)
        for m in markers:
            self.markers[m.n] = m

    def __iter__(self):
        """Yield the markers."""
        for m in self.markers:
            if m is not None:
                yield m

    def __getitem__(self, index):
        """Get a marker by index."""
        return self.markers[index]

    def __len__(self):
        """Get the number of markers."""
        return len(self.list)

    @property
    def list(self):
        """Return a list of the markers (not including None)."""
        return [m for m in self.markers if m is not None]

    def find_centre(self):
        """Find the centre from these markers."""
        centres = {
            'intersect': [],
            'average': [],
            'none': [],
        }
        if len(self.list) == 1:
            # Only one marker found
            return self.list[0].find_centre_by_distance()
        # More than one marker
        for comb in itertools.combinations(self.list, 2):
            t, c = comb[0].find_centre_with_marker(comb[1])
            centres[t].append(c)
        if len(centres['intersect']) == 0:
            if len(centres['average']) == 0:
                # All failed
                return None
            else:
                # Only averages
                sum_avg = sum(centres['average'], vec.Point())
                self.centre = (sum_avg / len(centres['average'])).int()
                return self.centre
        else:
            sum_int = sum(centres['intersect'], vec.Point())
            self.centre = (sum_int / len(centres['intersect'])).int()
            return self.centre


def find_markers(ch):
    """Find the markers from a contour hierarchy."""
    markers = []
    for node in ch.nodes:
        poss_m = Marker(node)
        # Check outer contour
        if not poss_m._check_rules(poss_m.outer, 'outer'):
            continue
        # Check inner contour
        poss_m.inner = node.children[0]
        if not poss_m._check_rules(poss_m.inner, 'inner'):
            continue
        poss_m._find_bar(poss_m.inner.children)
        if poss_m.bar is None:
            continue
        if poss_m.centre is not None:
            markers.append(poss_m)
    return Markers(markers)
