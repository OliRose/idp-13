"""Detect the arena layout."""

__author__ = 'Oli Rose'

import logging
import math

import cv2
import euclid3 as eu

from idp13 import utils
from idp13.detect import (
    contours,
    images,
    vectors as vec,
)


logger = logging.getLogger('idp13.{}'.format(__name__))
try:
    logger.setLevel(logging.VERBOSE)
except AttributeError:
    logger.setLevel(logging.DEBUG)
    logger.verbose = logger.debug

# Set some tolerances
CELL_LINE = 0.05
START_LINE = 0.8
START_CROSS = [0.15, 0.25]
ANGLE_TOL = 36      # (pi / ANGLE_TOL)


class Arena(utils.Queueable):
    """Store the object locations in the arena."""

    queue_attrs = [
        'cells', 'untested_cells', 'active_cells',
        'cell_line_targets', 'safe_area_targets', '__dict__'
    ]

    def __init__(self, safe_area=None, lines=None, cells=None):
        """Create the :obj:`Arena`."""
        if safe_area is None:
            self.safe_area = None
        else:
            self.safe_area = contours.RectangleContours()._get_corners(
                safe_area
            )
        if lines is None:
            self.lines = None
            self.red_line = None
            self._white_lines = None
            self.cell_line = None
            self._start_line = None
            self._start_cross = None
            self.start_zones = []
            self.edges = {
                'top': None,
                'left': None,
                'bottom': None,
                'right': None,
            }
        else:
            lines.get_specific_lines()
            self.red_line = eu.LineSegment2(
                eu.Point2(lines.red_line.x_at_y(0), 0),
                eu.Point2(lines.red_line.x_at_y(lines.img_y), lines.img_y)
            )
            self._white_lines = lines.white_lines
            self.cell_line = eu.LineSegment2(
                eu.Point2(lines.cell_line.x_at_y(0), 0),
                eu.Point2(lines.cell_line.x_at_y(lines.img_y), lines.img_y)
            )
            self._start_line = lines.start_line
            self._start_cross = lines.start_cross
            self.start_zones = [
                lines.start_line.intersect(sc).euclidify()
                for sc in lines.start_cross
            ]
            top_e = eu.LineSegment2(
                eu.Point2(0, 0), eu.Point2(lines.img_x, 0)
            )
            left_e = eu.LineSegment2(
                eu.Point2(0, 0), eu.Point2(0, lines.img_y)
            )
            bottom_e = eu.LineSegment2(
                eu.Point2(0, lines.img_y), eu.Point2(lines.img_x, lines.img_y)
            )
            right_e = eu.LineSegment2(
                eu.Point2(lines.img_x, 0), eu.Point2(lines.img_x, lines.img_y)
            )
            self.edges = {
                'top': top_e,
                'left': left_e,
                'bottom': bottom_e,
                'right': right_e,
            }
        if cells is None:
            self._cells = []
        else:
            self._cells = cells
        self._untested_cells = self._cells
        self._active_cells = []

    @property
    def cells(self):
        """Get a list of all cells as :obj:`euclid3.Point2` objects."""
        return [c.euclidify() for c in self._cells]

    @property
    def untested_cells(self):
        """Get a list of untested cells as :obj:`euclid3.Point2` objects."""
        return [c.euclidify() for c in self._untested_cells]

    @property
    def active_cells(self):
        """Get a list of active cells as :obj:`euclid3.Point2` objects."""
        return [c.euclidify() for c in self._active_cells]

    @property
    def safe_area_targets(self):
        """Find the points in the safe area to head for."""
        if self.safe_area is None:
            return None
        corners_x = [c[0] for c in self.safe_area]
        avg_x = sum(corners_x) / len(corners_x)
        corners_y = [c[1] for c in self.safe_area]
        min_y = min(corners_y)
        max_y = max(corners_y)
        return [
            # Centre of the top edge of the box
            eu.Point2(avg_x, max_y),
            # Centre of the box (to leave the cells)
            eu.Point2(avg_x, min_y),
            # Outside the box (to close door)
            eu.Point2(avg_x, 2 * min_y - max_y),
            # Outside the box (to close door)
            eu.Point2(avg_x, 0),
        ]

    @property
    def cell_line_targets(self):
        """Find the points for the cell line."""
        return [eu.Point2(70, 405), eu.Point2(70, 850)]

    def replace_cells(self, cells, active_detected):
        """Replace the existing cell lists with new ones."""
        logger.verbose('Replacing cells')
        if isinstance(cells, Cells):
            cells = cells.centres
        if cells is None:
            return
        # Process the new cells
        new_active = []
        new_untested = []
        new_unmatched = []
        cells_copy = cells.copy()
        for c in cells_copy:
            found = False
            for a_c in self._active_cells:
                if c.approx(a_c):
                    new_active.append(c)
                    found = True
                    break
            if not found:
                for u_c in self._untested_cells:
                    if c.approx(u_c):
                        new_untested.append(c)
                        found = True
                        break
            if not found:
                new_unmatched.append(c)
        if len(new_unmatched) == 0:
            # No new cells
            active_expected = active_detected
        elif len(new_unmatched) <= active_detected:
            logger.debug('{} new cells (expected)'.format(len(new_unmatched)))
            # New cells found (not more than expected)
            new_active.extend(new_unmatched)
            # Number of active cells expected next is reduced
            active_expected = active_detected - len(new_unmatched)
        else:
            logger.debug('{} new cells (unexpected)'.format(
                len(new_unmatched))
            )
            # Too many new cells detected
            new_untested.extend(new_unmatched)
            # Discard the expectation of more active cells
            active_expected = 0
        self._cells = new_active + new_untested
        self._active_cells = new_active
        self._untested_cells = new_untested
        logger.verbose('    active:   {}'.format(self.active_cells))
        logger.verbose('    untested: {}'.format(self.untested_cells))
        return active_expected


class Cells:
    """Detector object for cells."""

    def __init__(self, img):
        """Create the detector."""
        if img is None:
            self.cells = None
        else:
            self.img = img
            self.hsv = cv2.cvtColor(self.img, cv2.COLOR_BGR2HSV)
            self.colour()

    def colour(self):
        """Detect the cells based on colour."""
        logger.verbose('Finding cells based on colour')
        self.blue = images.colour_filter(self.hsv, 'blue')
        self.centres = contours.CircleContours(
            self.blue, min_size=100, max_size=1000
        ).centres
        return self

    def draw(self, img=None, centres=None, colour=(255, 0, 0), thickness=2):
        """Draw the detected cells on an image."""
        img = img if img is not None else self.img
        centres = centres if centres is not None else self.centres
        logger.verbose('    - Drawing cell marker circles')
        contours.CircleContours().draw(img=img, centres=centres,
                                       colour=colour, thickness=thickness)
        return img


class Lines:
    """Detector object for the lines on the arena."""

    def __init__(self, img):
        """Create the detector."""
        self.red_line = None
        self.white_lines = []
        self.start_line = None
        self.start_cross = []
        self.cell_line = None
        self.img = img
        self.img_x, self.img_y = img.shape[1], img.shape[0]
        self.hsv = cv2.cvtColor(self.img, cv2.COLOR_BGR2HSV)
        self._angle_tol = math.pi / 36    # 5 degrees
        self.colour()

    def colour(self):
        """Detect the lines based on colour."""
        logger.debug('Finding the lines based on colour')
        self._find_red_line()
        self._find_white_lines()

    def _find_red_line(self):
        """Find the red line."""
        logger.debug('    - Finding red line')
        self.red = images.colour_filter(self.hsv, 'red')
        for i in range(30, 0, -10):
            lines = contours.LineContours(self.red, n=30, thresh=10*i).lines
            if len(lines) != 0:
                break
        self.red_line = self._find_vert_lines(lines)[0]

    def _find_white_lines(self):
        """Find the white lines."""
        logger.debug('    - Finding white lines')
        self.white = images.colour_filter(self.hsv, 'white')
        white_lines = contours.LineContours(self.white, n=10).lines
        old_white_lines = white_lines
        if self.red_line is not None:
            # Remove any lines that are like the red line (it may be detected)
            for line in old_white_lines:
                if line.approx(self.red_line):
                    white_lines.remove(line)
        # White lines are either vertical or horizontal
        self.white_lines = (self._find_vert_lines(white_lines)
                            + self._find_horiz_lines(white_lines))

    def _find_vert_lines(self, lines):
        """Find all the nearly vertical lines from a list of lines."""
        vertical_line = vec.Line(vec.Point(), vec.Vector(0, 1))
        return self._find_near_angle_lines(vertical_line, lines)

    def _find_horiz_lines(self, lines):
        """Find all the nearly horizontal lines from a list of lines."""
        horizontal_line = vec.Line(vec.Point(), vec.Vector(1, 0))
        return self._find_near_angle_lines(horizontal_line, lines)

    def _find_near_angle_lines(self, test_line, lines):
        """Find all the lines close in angle, the test line."""
        result = []
        for line in lines:
            if test_line.v.angle(line.v, acute=True) < self._angle_tol:
                # Close enough, vertical
                result.append(line)
        return result

    def draw_red_line(self, img=None, colour=(0, 0, 255), thickness=2):
        """Draw the detected red line on an image."""
        img = img if img is not None else self.img
        logger.debug('    - Drawing red line')
        self._draw_lines(img, self.red_line, colour, thickness)
        return img

    def draw_white_lines(self, img=None, colour=(128, 128, 0), thickness=2):
        """Draw the detected white lines on an image."""
        img = img if img is not None else self.img
        logger.debug('    - Drawing white lines')
        self._draw_lines(img, self.white_lines, colour, thickness)
        return img

    def _draw_lines(self, img, lines, colour, thickness):
        """Draw any lines."""
        contours.LineContours().draw(img=img, lines=lines,
                                     colour=colour, thickness=thickness)

    def draw(self, img=None):
        """Draw both red and white lines."""
        return self.draw_white_lines(self.draw_red_line(img))

    def merge(self, lines2):
        """Merge with another :obj:`Lines` keeping only unmoved lines."""
        if not self.red_line.approx(lines2.red_line):
            logger.warning('Red line detected differently between detections')
        old_white_lines = self.white_lines
        self.white_lines = []
        for w1 in old_white_lines:
            for w2 in lines2.white_lines:
                if w1.approx(w2):
                    # These two lines are in both objects
                    self.white_lines.append(w1)
        return self

    def get_specific_lines(self):
        """Extract the important lines."""
        for line in self._find_vert_lines(self.white_lines):
            if line.p.x < CELL_LINE * self.img_x:
                self.cell_line = line
            elif line.p.x > START_LINE * self.img_x:
                self.start_line = line
        for line in self._find_horiz_lines(self.white_lines):
            if (line.p.y > START_CROSS[0] * self.img_y
                    and line.p.y < START_CROSS[1] * self.img_y):
                self.start_cross.append(line)
            elif (line.p.y < (1 - START_CROSS[0]) * self.img_y
                    and line.p.y > (1 - START_CROSS[1]) * self.img_y):
                self.start_cross.append(line)


class SafeArea:
    """Detector object for the safe area."""

    def __init__(self, img):
        """Create the detector."""
        self.img = img
        self.hsv = cv2.cvtColor(self.img, cv2.COLOR_BGR2HSV)
        self.rect = None
        self.colour()

    def colour(self):
        """Detect the safe area based on colour."""
        logger.verbose('Finding safe area based on colour')
        self.green = images.colour_filter(self.hsv, 'green')
        rects = contours.RectangleContours(self.green).rects
        logger.verbose('    - Filtering {} rectangles'.format(len(rects)))
        img_x = self.img.shape[1]
        img_y = self.img.shape[0]
        logger.verbose('    - Image size: {}, {}'.format(img_x, img_y))
        for rect in rects:
            if rect.w > 0.03 * img_x and rect.h > 0.2 * img_y:
                if (rect.x > (0.85 * img_x)
                        and rect.y > 0.3 * img_y and rect.y < 0.7 * img_y):
                    logger.verbose('    - Rectangle found: {}'.format(rect))
                    self.rect = rect
                    break
        return self

    def draw(self, img=None, colour=(0, 255, 0), thickness=1):
        """Draw the detected safe box on an image."""
        img = img if img is not None else self.img
        logger.verbose('    - Drawing safe area')
        contours.RectangleContours().draw(img=img, rects=self.rect,
                                          colour=colour, thickness=thickness)
        return img


class ArenaThread(utils.StoppableThread):
    """Arena detection thread."""

    def __init__(self, camera_q, link_t):
        """Setup the thread."""
        self.camera_q = camera_q
        self.link_t = link_t
        self.arena = Arena()
        self.active_expected = 0
        self.stored = 0
        super().__init__(name='ArenaThread')

    def set_initial_arena(self, intial_arena):
        """Set the intial arena layout (lines etc)."""
        logger.debug('Setting intial arena')
        self.arena = intial_arena

    def loop(self):
        """Main loop."""
        # r = self.link_t.request()
        # try:
        #     self.stored += r[0]
        #     self.active_expected += r[1]
        # except (IndexError, TypeError):
        #     logger.exception('')
        # self.active_expected -= self.arena.replace_cells(
        #     Cells(self.camera_q[0]), self.active_expected
        # )
        self.q.append(self.arena.queue())
        pass


def detect(img1, img2):
    """Detect the features from two images."""
    logger.info('Detecting arena from two images')
    sa1 = SafeArea(img1).rect
    safe_area = sa1 if sa1 is not None else SafeArea(img2).rect
    logger.debug('Safe area: {}'.format(safe_area))
    lines = Lines(img1).merge(Lines(img2))
    print(Lines(img1).white_lines)
    logger.debug('Red line: {}'.format(lines.red_line))
    logger.debug('Cell line: {}'.format(lines.cell_line))
    logger.debug('Start crosses: {}'.format(lines.start_cross))
    # logger.debug('Edges: {}'.format(lines.edges))
    cells = Cells(img2).centres
    logger.debug('Cells: {}'.format(cells))
    return Arena(safe_area, lines, cells)


def get_default_arena(img):
    """Get a pre loaded arena data set (and detect the cells)."""
    ar = Arena()
    ar.red_line = eu.LineSegment2(
        eu.Point2(645, 0), eu.Point2(645, img.shape[0])
    )
    ar.cell_line = eu.LineSegment2(
        eu.Point2(80, 405), eu.Point2(80, 850)
    )
    ar.start_zones = [eu.Point2(1144, 131)]
    ar.safe_area = (eu.Point2(1186, 511), eu.Point2(1314, 757))
    ar.edges = {
        'top': eu.LineSegment2(
            eu.Point2(0, 0), eu.Point2(img.shape[1], 0)
        ),
        'left': eu.LineSegment2(
            eu.Point2(0, 0), eu.Point2(0, img.shape[0])
        ),
        'bottom': eu.LineSegment2(
            eu.Point2(0, img.shape[0]), eu.Point2(img.shape[1], img.shape[0])
        ),
        'right': eu.LineSegment2(
            eu.Point2(img.shape[1], 0), eu.Point2(img.shape[1], img.shape[0])
        )
    }
    ar._cells = Cells(img).centres
    ar._untested_cells = ar._cells
    ar._active_cells = []
    return ar
