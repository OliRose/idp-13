"""Placeholder for data classes."""

import euclid3 as eu


def point(x, y):
    """Create a euclid.Point2."""
    return eu.Point2(x, y)


class Arena:
    """Arena object locations."""

    def __init__(self):
        """Demo locations."""
        self.safe_area = (point(10, 10), point(20, 20))
        self.red_line = eu.LineSegment2(point(80, 50), point(80, 0))
        self.cell_line = eu.LineSegment2(point(20, 50), point(20, 0))
        self.start_zones = [
            (point(0, 0), point(1, 1)),
            (point(10, 10), point(20, 20)),
        ]
        self.edges = {
            'top': eu.LineSegment2(point(5, 5), point(80, 5)),
            'left': eu.LineSegment2(point(5, 5), point(5, 80)),
            'bottom': eu.LineSegment2(point(5, 80), point(80, 80)),
            'right': eu.LineSegment2(point(80, 5), point(80, 80)),
        }
        self.untested_cells = [point(10, 20), point(80, 90)]
        self.active_cells = [point(45, 50)]


class Robot:
    """Robot location and position."""

    def __init__(self):
        """Demo locations."""
        self.position = point(10, 10)
        self.direction = eu.Vector2((1, 10), (10, 0))
