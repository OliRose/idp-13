"""Wrapper of OpenCV functionality for cameras."""

__author__ = 'Oli Rose'

import logging
from collections import defaultdict

import cv2

from idp13 import utils


logger = logging.getLogger('idp13.{}'.format(__name__))
try:
    logger.setLevel(logging.VERBOSE)
except AttributeError:
    logger.setLevel(logging.DEBUG)
    logger.verbose = logger.debug


class Camera:
    """Camera class."""

    defaults = {
        # 'brightness': 40,
        # 'contrast': 200,
        # 'saturation': 128,
        'brightness': 128,
        'contrast': 128,
        'saturation': 128,
    }

    def __init__(self, n=0, flipped=False, left_margin=None,
                 right_margin=None, video_out=False, **kwargs):
        """Create a Camera object.

        Parameters
        ----------
        n : int
            Number of the camera to open (passed to cv2.VideoCapture)
        flipped : bool
            Whether or not to flip the image
        left_margin : float, optional
            How much to cut off the left edge (after flipping if flipped==True)
        right_margin : float, optional
        **kwargs
            Passed to self._open

        """
        logger.debug(
            'Setting camera {} up (flipped: {}, '
            'left_margin: {}, right_margin: {}'.format(
                n, flipped, left_margin, right_margin)
        )
        self.flipped = flipped
        self.left_margin = left_margin if left_margin is not None else 0
        self.right_margin = right_margin if right_margin is not None else 0
        kw = defaultdict(lambda: None, kwargs)
        self._brightness = kw['brightness']
        self._saturation = kw['saturation']
        self._contrast = kw['contrast']
        self._open(n, video_out=video_out, **kwargs)

    def _open(self, n=0, fourcc=None, res=None, video_out=False):
        """Open the camera."""
        logger.info('Opening camera {}'.format(n))
        self._vc = cv2.VideoCapture(n + cv2.CAP_DSHOW)
        if fourcc is not None:
            logger.debug('Setting codec to {}'.format(fourcc))
            self._vc.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*fourcc))
        if res is not None:
            logger.debug('Setting resolution to {}'.format(res))
            self._vc.set(cv2.CAP_PROP_FRAME_WIDTH, res[0])
            self._vc.set(cv2.CAP_PROP_FRAME_HEIGHT, res[1])
        self.brightness = self._brightness or self.defaults['brightness']
        self.saturation = self._saturation or self.defaults['saturation']
        self.contrast = self._contrast or self.defaults['contrast']
        # Setup the output
        if video_out:
            logger.debug('Setting up video writer to {}'.format(video_out))
            # Store the images as a video
            self.vw = cv2.VideoWriter(
                '{}.mp4'.format(video_out),
                cv2.VideoWriter_fourcc(*'MP4V'),
                10, (res[0], res[1])
            )
        else:
            self.vw = None

    def is_open(self):
        """Test if the camera is connected or not."""
        if self._vc is None or not self._vc.isOpened():
            logger.debug('Camera closed')
            return False
        else:
            logger.debug('Camera open')
            return True

    def _close(self):
        """Close the camera."""
        logger.info('Releasing camera')
        try:
            self._vc.release()
        except AttributeError:
            pass
        self._vc = None

    def __enter__(self):
        """Enter the context manager."""
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        """Exit the context manager."""
        self._close()

    @property
    def brightness(self):
        """Get brightness."""
        if self._vc and self.is_open():
            self._brightness = self._vc.get(cv2.CAP_PROP_BRIGHTNESS)
        logger.debug('Getting brightness: {}'.format(self._brightness))
        return self._brightness

    @brightness.setter
    def brightness(self, value):
        """Set the brightness on the camera."""
        logger.debug('Setting brightness to {}'.format(value))
        self._brightness = value
        if self._vc:
            self._vc.set(cv2.CAP_PROP_BRIGHTNESS, self._brightness)

    @property
    def saturation(self):
        """Get saturation."""
        if self._vc and self.is_open():
            self._saturation = self._vc.get(cv2.CAP_PROP_SATURATION)
        logger.debug('Getting saturation: {}'.format(self._saturation))
        return self._saturation

    @saturation.setter
    def saturation(self, value):
        """Set the saturation on the camera."""
        logger.debug('Setting saturation: {}'.format(value))
        self._saturation = value
        if self._vc:
            self._vc.set(cv2.CAP_PROP_SATURATION, self._saturation)

    @property
    def contrast(self):
        """Get contrast."""
        if self._vc and self.is_open():
            self._contrast = self._vc.get(cv2.CAP_PROP_CONTRAST)
        logger.debug('Getting contrast: {}'.format(self._contrast))
        return self._contrast

    @contrast.setter
    def contrast(self, value):
        """Set the contrast on the camera."""
        logger.debug('Setting contrast to {}'.format(value))
        self._contrast = value
        if self._vc:
            self._vc.set(cv2.CAP_PROP_CONTRAST, self._contrast)

    def read(self):
        """Read a single frame."""
        logger.verbose('Reading next frame')
        _, frame = self._vc.read()
        if self.vw:
            self.vw.write(frame)
        if frame is not None:
            if self.flipped:
                frame = cv2.flip(frame, 1)
            left_i = int(self.left_margin * frame.shape[1])
            right_i = int(self.right_margin * frame.shape[1])
            return frame[:, left_i:-right_i]
        else:
            return None


class CameraThread(utils.StoppableThread):
    """Get frames from the camera in a new thread."""

    def __init__(self, vc):
        """Setup the thread."""
        self.vc = vc
        super().__init__(name='CameraThread')

    def loop(self):
        """Main loop."""
        logger.verbose('Adding next frame to queue')
        self.q.append(self.vc.read())


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    i = 0
    with Camera(res=(1920, 1080)) as vc:
        img = vc.read()
        # vc.brightness = 80
        # vc.contrast = 80
        # vc.saturation = 80
        while True:
            img = vc.read()
            cv2.imshow(
                'Image',
                cv2.resize(img, (int(img.shape[1] / 2), int(img.shape[0] / 2)))
            )
            a = cv2.waitKey(1)
            if a & 0xFF == ord('q'):
                break
            elif a & 0xFF == ord('c'):
                i += 1
                cv2.imwrite('{}.png'.format(i), img)
