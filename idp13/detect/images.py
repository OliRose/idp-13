"""Utility functions to modify images."""

__author__ = 'Oli Rose'

import logging

import cv2
import numpy as np


logger = logging.getLogger('idp13.{}'.format(__name__))
try:
    logger.setLevel(logging.VERBOSE)
except AttributeError:
    logger.setLevel(logging.DEBUG)
    logger.verbose = logger.debug


def get_range(img, low, high):
    """Get the parts of an image of a certain colour."""
    logger.verbose('Image range between {} and {}'.format(low, high))
    return cv2.inRange(img, np.array(low), np.array(high))


def morph_open(img, n=3):
    """Erode image then dilate image to remove noise."""
    logger.verbose('Morph open, kernel size {}'.format(n))
    kernel = np.ones((n, n), np.uint8)
    return cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)


def colour_filter(img, colour):
    """Filter the image to one colour and perform opening.

    Parameters
    ----------
    img : np.array
        Standard cv2 image
    colour : str
        String representation of colour
        ('red', 'green', 'blue', 'white').
    """
    colour_ranges = {
        'red': [
            ((0, 80, 100), (60, 255, 255)),
            ((150, 80, 100), (180, 255, 255)),
        ],
        'green': [
            ((60, 50, 50), (90, 255, 255)),
        ],
        'blue': [
            ((90, 50, 50), (120, 255, 255)),
        ],
        'white': [
            ((0, 0, 180), (255, 255, 255))
        ],
    }
    logger.verbose('    - Filtering for colour: {}'.format(colour))
    try:
        c_ranges = colour_ranges[colour]
    except KeyError:
        logging.error('Attempting to filter using invalid '
                      'colour: {}'.format(colour))
        raise ValueError('Attempting to filter using invalid '
                         'colour: {}'.format(colour))
    for i, cr in enumerate(c_ranges):
        if i == 0:
            new_img = get_range(img, cr[0], cr[1])
        else:
            new_img += get_range(img, cr[0], cr[1])
    return morph_open(new_img)
