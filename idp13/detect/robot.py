"""Detect the robot location."""

__author__ = 'Oli Rose'

import logging

import cv2

from idp13 import utils
from idp13.detect import (
    contours,
    images,
    markers,
)


logger = logging.getLogger('idp13.{}'.format(__name__))
try:
    logger.setLevel(logging.VERBOSE)
except AttributeError:
    logger.setLevel(logging.DEBUG)
    logger.verbose = logger.debug

LEFT_MARKER = 1
RIGHT_MARKER = 2
REAR_MARKER = 3
WHITE_THRESH = [150, 200, 240]


class Robot(utils.Queueable):
    """Store the robot position."""

    queue_attrs = ['position', 'direction', '__dict__']

    def __init__(self, img=None, white_thresh=None):
        """Create the object."""
        self.img = img
        self._position = None
        self._direction = None
        self.white_thresh = white_thresh or WHITE_THRESH
        logger.verbose('White thresholds: {}'.format(white_thresh))
        # self.find()

    def find(self, img=None):
        """Find the robot in an image."""
        img = img if img is not None else self.img
        marker_sets = []
        for v in self.white_thresh:
            white = images.get_range(img, (0, 0, v), (255, 255, 255))
            ch = contours.find_contours_hierarchy(white)
            ms = markers.find_markers(ch)
            marker_sets.append(ms)
            logger.verbose('{} markers found at {} threshold'.format(
                len(ms), v
            ))
        self.markers = sorted(marker_sets, key=len, reverse=True)[0]
        self._position = self.markers.find_centre()
        logger.verbose('Centre found: {}'.format(self._position))
        for marker in self.markers:
            if marker.n == REAR_MARKER:
                self._direction = marker.direction
                break
            elif marker.n == LEFT_MARKER:
                self._direction = -marker.direction.perp()
            elif marker.n == RIGHT_MARKER:
                self._direction = marker.direction.perp()
            else:
                continue
        return self

    def draw(self, img=None):
        """Find and draw the contours."""
        if img is None:
            img = self.img
        self.find(img)
        cv2.drawContours(
            img, [m.outer.contour for m in self.markers], -1,
            color=(0, 255, 0), thickness=2
        )
        cv2.drawContours(
            img, [m.bar.contour for m in self.markers], -1,
            color=(0, 0, 255), thickness=2
        )
        if self.centre is not None:
            contours.CircleContours().draw(img, [self.centre], sz=4)
        return img

    @property
    def position(self):
        """:obj:`euclid3.Point2`: Get the robot position."""
        if self._position is None:
            return None
        else:
            return self._position.euclidify()

    @property
    def direction(self):
        """:obj:`euclid3.Vector2`: Get the robot direction."""
        if self._direction is None:
            return None
        else:
            return self._direction.unit().euclidify()


class RobotThread(utils.StoppableThread):
    """Thread allowing detection of robot location in a separate thread."""

    def __init__(self, camera_q, white_thresh=None):
        """Setup the thread."""
        logger.info('White thresholds: {}'.format(white_thresh))
        self.camera_q = camera_q
        self.robot = Robot(white_thresh=white_thresh)
        super().__init__(name='RobotThread')

    def loop(self):
        """Main loop."""
        frame = self.camera_q[0]
        if frame is None:
            return
        else:
            self.robot.find(frame)
            if (self.robot.position is not None
                    and self.robot.direction is not None):
                self.q.append(self.robot.queue())
                logger.verbose('Robot found: {}, {}'.format(
                    self.robot.position, self.robot.direction
                ))
            else:
                self.q.append(None)
                logger.verbose('Failed to find robot')

    def get_pose(self):
        """Get for the current frame."""
        return self.robot.find()
