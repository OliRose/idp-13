"""Vector storage and manipulation.

This module implements a number of basic 2D vector structures and
operations. It allows the creation and manipulation of various
geometric objects.

"""

__author__ = 'Oli Rose'

import logging

import euclid3 as eu
import numpy as np


logger = logging.getLogger('idp13.{}'.format(__name__))
try:
    logger.setLevel(logging.VERBOSE)
except AttributeError:
    logger.setLevel(logging.DEBUG)
    logger.verbose = logger.debug

APPROX_TOL = 1e-7
ANGLE_TOL = 1e-4


def _get_pair_by_type(p1, p2, instance_type):
    """Return self and p such that [0] is of instance_type."""
    if isinstance(p1, instance_type):
        return p1, p2
    elif isinstance(p2, instance_type):
        return p2, p1
    else:
        raise TypeError('Neither of type {}'.format(instance_type))


def _one_of_pair(p1, p2, instance_type):
    """Check if one or both of a pair are of type."""
    if isinstance(p1, instance_type) or isinstance(p2, instance_type):
        return True
    else:
        return False


class _Base:
    """Base class for other classes to inherit from."""

    def __str__(self):
        """Print any of the basic types with attributes x and y."""
        return '{}.{}<{},{}>'.format(
            __name__, self.__class__.__name__, self.x, self.y
        )

    __repr__ = __str__

    def _distance_point(self, p):
        """Find the minimum distance (at least one Point)."""
        p1, p2 = _get_pair_by_type(self, p, Point)
        if isinstance(p2, Point):
            # Point -> Point
            return (p1 - p2).length
        elif isinstance(p2, Line):
            # Point -> Line-like
            t = p2._adjust_t(p2.v.dot(p1 - p2.p) / p2.v.length ** 2)
            closest_p = p2.p + t * p2.v
            return (closest_p - p1).length
        else:
            raise TypeError

    def _distance_linesegment(self, p):
        """Find the minimum distance (at least one LineSegment)."""
        p1, p2 = _get_pair_by_type(self, p, LineSegment)
        if isinstance(p2, LineSegment):
            # LineSegment -> LineSegment (not intersecting)
            return min([p1.distance(p2.p1),
                        p1.distance(p2.p2),
                        p2.distance(p1.p1),
                        p2.distance(p1.p2)])
        elif isinstance(p2, Ray):
            # LineSegment -> Ray (not intersecting)
            return min([p1.p1.distance(p2),
                        p1.p2.distance(p2),
                        p2.p.distance(p1)])
        elif isinstance(p2, Line):
            # LineSegment -> Line (not intersecting)
            return min([p1.p1.distance(p2), p1.p2.distance(p2)])
        else:
            raise TypeError

    def _distance_ray(self, p):
        """Find the minimum distance (at least one Ray)."""
        p1, p2 = _get_pair_by_type(self, p, Ray)
        if isinstance(p2, Ray):
            # Ray -> Ray (not intersecting)
            return min([p1.p.distance(p2), p2.p.distance(p1)])
        elif isinstance(p2, Line):
            # Ray -> Line (not intersecting)
            return p1.p.distance(p2)
        else:
            raise TypeError

    def _distance_line(self, p):
        """Find the minimum distance (at least one Line)."""
        p1, p2 = _get_pair_by_type(self, p, Line)
        if isinstance(p2, Line):
            # Line -> Line
            return p1.p.distance(p2)
        else:
            raise TypeError

    def distance(self, p):
        """Find the minimum distance from this object to another.

        This function attempts to find a distance between any two
        :mod:`idp13.detect.vectors` objects.

        Args:
            p: The object to calculate the distance to.
        Returns:
            :obj:`float`: Distance from this object to `p`
        Raises:
            :obj:`TypeError`: If `p` is not object of type defined in
                :mod:`idp13.detect.vectors`

        """
        if _one_of_pair(self, p, Point):
            return self._distance_point(p)
        elif _one_of_pair(self, p, LineSegment):
            return self._distance_linesegment(p)
        elif _one_of_pair(self, p, Ray):
            return self._distance_ray(p)
        elif _one_of_pair(self, p, Line):
            return self._distance_line(p)
        else:
            raise TypeError


class _BaseVector(_Base):
    """Class to derive all other vectors from.

    Accepts values for x and y, or a single array-like object in x
    which will be split for x and y. Can also create new object
    from existing, similar object.

    Args:
        x (:obj:`float` or :obj:`array` of :obj:`float`): x-coordinate
        y (:obj:`float`, optional): y-coordinate
        tolerance (:obj:`int`, optional): The default tolerance for testing
            whether two objects are close

    """

    def __init__(self, x=(0, 0), y=None, tolerance=APPROX_TOL):
        """Create the object."""
        try:
            x, y = x.x, x.y
        except AttributeError:
            if y is None:
                x, y = x
        self._v = np.array([x, y], dtype=float)
        self.tolerance = tolerance

    @property
    def x(self):
        """:obj:`float`: Get or set the x coordinate."""
        return self._v[0]

    @x.setter
    def x(self, val):
        self._v[0] = val

    @property
    def y(self):
        """:obj:`float`: Get or set the y coordinate."""
        return self._v[1]

    @y.setter
    def y(self, val):
        self._v[1] = val

    def int(self):
        """Return object of same type but with integer values.

        Returns:
            :obj:`type(self)`:
                Equivalent object but with integer values (using int())

        """
        return type(self)([int(v) for v in self._v])

    def round(self):
        """Return object of same type but with rounded values.

        Returns:
            :obj:`type(self)`:
                Equivalent object but with integer values (using round())

        """
        return type(self)([round(v) for v in self._v])

    def __eq__(self, val):
        """Check for equality."""
        return (self._v == val._v).all()

    def __ne__(self, val):
        """Check for non equality."""
        return not self.__eq__(val)

    def __neg__(self):
        """Get the negative."""
        return type(self)(-1 * self._v)

    def __mul__(self, val):
        """Multiply by val."""
        if isinstance(val, _BaseVector):
            # Cross product - TODO
            raise NotImplementedError
        else:
            return type(self)(self._v * val)

    def __truediv__(self, val):
        """Divide (producing float)."""
        if isinstance(val, _BaseVector):
            raise NotImplementedError
        else:
            return type(self)(self._v / val)

    def __floordiv__(self, val):
        """Divide (producing int)."""
        if isinstance(val, _BaseVector):
            raise NotImplementedError
        else:
            return type(self)(self._v // val)

    def __rmul__(self, val):
        """Multiply by val."""
        return self.__mul__(val)

    def __getitem__(self, i):
        """Get indexed item."""
        return self._v[i]

    def x_at_y(self, y):
        """Evaluate the x value of this vector at a y value."""
        if abs(self.y) <= self.tolerance:
            # Horizontal
            return None
        else:
            return self.x * (y / self.y)

    def y_at_x(self, x):
        """Evaluate the y value of this vector at an x value."""
        if abs(self.x) <= self.tolerance:
            # Vertical
            return None
        else:
            return self.y * (x / self.x)

    def approx(self, p, tolerance=None):
        """Check if this object is close to ``p``."""
        tolerance = tolerance or self.tolerance
        p1 = Point(self)
        p2 = Point(p)
        return p1.distance(p2) <= tolerance


class Point(_BaseVector):
    """Point represented by x and y coordinates.

    Accepts values for x and y, or a single array-like object in x
    which will be split for x and y. Can also create new :obj:`Point`
    from an existing, similar object (e.g. :obj:`Point`, :obj:`Vector`).

    Args:
        x (:obj:`float` or :obj:`array` of :obj:`float`): x-coordinate
        y (:obj:`float`, optional): y-coordinate

    """

    @property
    def p(self):
        """:obj:`array` of :obj:`float`: Get or set the point."""
        return self._v

    @p.setter
    def p(self, val):
        self._v = np.array(val)

    def __add__(self, val):
        """Add."""
        if isinstance(val, _BaseVector):
            return Point(self.p + val._v)
        else:
            raise TypeError

    def __radd__(self, val):
        """Add."""
        return self.__add__(val)

    def __sub__(self, val):
        """Subtract."""
        if isinstance(val, Point):
            return Vector(self._v - val._v)
        elif isinstance(val, Vector):
            return Point(self._v - val._v)
        else:
            raise TypeError

    def __rsub__(self, val):
        """Subtract."""
        return -self.__sub__(val)

    def connect(self, p):
        """Connect this point to another point.

        Args:
            p (:obj:`Point`)

        Returns:
            :obj:`LineSegment`

        """
        return LineSegment(self, p)

    def euclidify(self):
        """Convert this to a :obj:`euclid3.Point2`.

        Returns:
            :obj:`euclid3.Point2`

        """
        return eu.Point2(self.x, self.y)


class Vector(_BaseVector):
    """2-D vector.

    Accepts values for x and y, or a single array-like object in x
    which will be split for x and y. Can also create new :obj:`Vector`
    from an existing, similar object (e.g. :obj:`Point`, :obj:`Vector`).

    Args:
        x (:obj:`float` or :obj:`array` of :obj:`float`): x-coordinate
        y (:obj:`float`, optional): y-coordinate

    """

    @property
    def v(self):
        """:obj:`array` of :obj:`float`: Get or set the vector."""
        return self._v

    @v.setter
    def v(self, val):
        self._v = np.array(val)

    @property
    def length(self):
        """:obj:`float`: Get the length of the vector."""
        return np.linalg.norm(self._v)

    def __add__(self, val):
        """Add."""
        if isinstance(val, _Base):
            return type(val)(self._v + val._v)
        else:
            raise TypeError

    def __radd__(self, val):
        """Add."""
        return self.__add__(val)

    def __sub__(self, val):
        """Subtract."""
        if isinstance(val, _Base):
            return type(val)(self._v - val._v)
        else:
            raise TypeError

    def __rsub__(self, val):
        """Subtract."""
        return -self.__sub__(val)

    def unit(self):
        """Get this as a unit :obj:`Vector`.

        Returns:
            :obj:`Vector`: Unit vector

        """
        norm = np.linalg.norm(self._v)
        if norm == 0:
            return Vector(self)
        return Vector(self._v / norm)

    def perp(self):
        """Get the perpendicular vector.

        Get a :obj:`Vector` of the same length but perpendicular
        in the direction of ``self.v`` cross ``k``.

        Returns:
            :obj:`Vector`: The perpendicular vector

        """
        return Vector(self._v[1], -self.v[0])

    def dot(self, v):
        """Get the dot product with another vector.

        Args:
            v (:obj:`Vector`): Second vector

        Returns:
            :obj:`float`: Dot product of the two vectors

        """
        return np.dot(self._v, v._v)

    def angle(self, v, acute=False):
        """Get the angle to another vector between 0 and pi.

        Args:
            v (:obj:`Vector`): Second vector
            acute (:obj:`bool`): Whether to only get the smallest angle
                (ignoring vector direction)

        Returns:
            :obj:`float`: Angle between the two vectors (rad)

        """
        if acute:
            return min(self.angle(v), self.angle(-v))
        else:
            return np.arccos(self.unit().dot(v.unit()))

    def euclidify(self):
        """Convert this to a :obj:`euclid3.Vector2`.

        Returns:
            :obj:`euclid3.Vector2`

        """
        return eu.Vector2(self.x, self.y)


class Line(_Base):
    """2-D line.

    Accepts a :obj:`Point` and a :obj:`Vector` or two points
    on the line.

    Args:
        p (:obj:`Point`): A point on the line
        v (:obj:`Vector` or :obj:`Point`): A vector along the
            line or another point on the line
        tolerance (:obj:`int`, optional): The default tolerance for testing
            whether two objects are close

    Attributes:
        p (:obj:`Point`): Start point
        v (:obj:`Vector`): Vector along the line

    """

    def __init__(self, p, v, tolerance=APPROX_TOL):
        """Create the :obj:`Line`."""
        self.p = Point(p)
        if type(v) == Point:
            v = v - p
        self.v = Vector(v)
        self.tolerance = tolerance

    def __str__(self):
        """Print in a useful format for logging."""
        return '{}.{}<{},{}>'.format(
            __name__, self.__class__.__name__, self.p, self.v
        )

    __repr__ = __str__

    def _check_intersect(self, s):
        """Check that a particular intersection is valid for this type."""
        # Any intersection is valid on an infinite line
        if s is None:
            return False
        else:
            return True

    def intersect(self, line2):
        """Find an intersection with another :obj:`Line` object.

        Args:
            line2 (:obj:`Line`) or similar: The line to find an
                intersection with

        Returns
            :obj:`Point` if intersection found, else ``None``

        """
        perp_u_v = self.v.perp().dot(line2.v)
        if abs(perp_u_v) < 1e-7:
            # Lines are parallel
            return None
        else:
            w = self.p - line2.p
            s = line2.v.perp().dot(w) / perp_u_v
            t = self.v.perp().dot(w) / perp_u_v
        if self._check_intersect(s) and line2._check_intersect(t):
            return self.p + s * self.v
        else:
            return None

    def angle(self, line2, acute=False):
        """Find the angle to another :obj:`Line`.

        Args:
            line2 (:obj:`Line` or subclass): Second line
            acute (:obj:`bool`): Whether to only get the smallest angle
                (ignoring ray or segment direction)

        Returns:
            :obj:`float`: Angle between the two vectors (rad)

        """
        if not isinstance(self, (Ray, LineSegment)):
            # Line does not have a defined direction
            acute = True
        if isinstance(line2, Vector):
            return line2.angle(self.v, acute=acute)
        else:
            return line2.v.angle(self.v, acute=acute)

    def _adjust_t(self, t):
        """Adjust a value of t to account for non-infinite lines."""
        return t

    def x_at_y(self, y):
        """Evaluate the x value of this line at a y value."""
        if abs(self.v.y) <= self.tolerance:
            # Horizontal
            return None
        else:
            return self.p.x + self.v.x * (y / (self.v.y - self.p.y))

    def y_at_x(self, x):
        """Evaluate the y value of this line at an x value."""
        if abs(self.v.x) <= self.tolerance:
            # Vertical
            return None
        else:
            return self.p.y + self.v.y * (x / (self.v.x - self.p.x))

    def approx(self, line, tolerance=None):
        """Check if this line is approximately the same as another."""
        tolerance = tolerance or self.tolerance
        return (self.v.approx(line.v, tolerance)
                and self.distance(line) <= tolerance)

    def euclidify(self):
        """Convert this to a :obj:`euclid3.Line2`.

        Returns:
            :obj:`euclid3.Line2`

        """
        return eu.Line2(self.p.euclidify(), self.v.euclidify())


class Ray(Line):
    """2-D ray.

    Created as for :obj:`Line` but starting at ``p`` and only in the
    direction of ``v``.

    Args:
        p (:obj:`Point`): Start point
        v (:obj:`Vector` or :obj:`Point`): Vector along the ray

    Attributes:
        p (:obj:`Point`): Start point
        v (:obj:`Vector`): Vector along the ray

    Inherits:
        ``euclidify``

    """

    def _check_intersect(self, s):
        """Check that a particular intersection is valid for this type."""
        # An intersect is valid on a ray if it is positive
        if s is None or s < 0:
            return False
        else:
            return True

    def _adjust_t(self, t):
        """Adjust a value of t to account for non-infinite lines."""
        return max(0, t)

    def flip(self):
        """Flip to point in the opposite direction."""
        return self.__class__(self.p, -self.v)


class LineSegment(Line):
    """2-D line segment.

    Can be created from:

        * Two :obj:`Point` objects as the two endpoints
        * A :obj:`Point` and a :obj:`Vector` (length defined by
          length of the vector)
        * A :obj:`Point`, a :obj:`Vector` and a length
        * A :obj:`Line` (length defined by length of vector)
        * A :obj:`Line` and a length

    Args:
        p1 (:obj:`Point` or :obj:`Line`)
        p2 (:obj:`Vector` or :obj:`Point`, optional)
        length (:obj:`float`, optional)

    Attributes:
        p1 (:obj:`Point`): Start point
        p2 (:obj:`Point`): End point
        p (:obj:`Point`): Start point
        v (:obj:`Vector`): Vector of the same length along the line

    """

    def __init__(self, p1, p2=None, length=None):
        """Create the :obj:`LineSegment`."""
        if isinstance(p1, Point):
            self.p1 = p1
            if isinstance(p2, Point):
                if length is not None:
                    logger.warning('length ignored (p1:Point,p2:Point)')
                self.p2 = p2
            elif isinstance(p2, Vector):
                if length is None:
                    self.p2 = p1 + p2
                else:
                    self.p2 = p1 + p2.unit() * length
            else:
                raise TypeError('p2 must be a Point or Vector')
        elif isinstance(p1, Line):
            if p2 is not None:
                logger.warning('p2 ignored (p1:Line)')
            self.p1 = p1.p
            if length is None:
                self.p2 = p1.p + p1.v
            else:
                self.p2 = p1.p + p1.v.unit() * length
        else:
            raise TypeError('p1 must be a Point or Line')
        self.p = self.p1
        self.v = self.p2 - self.p1

    def __str__(self):
        """Print in a useful format for logging."""
        return '{}.{}<{},{}>'.format(
            __name__, self.__class__.__name__, self.p1, self.p2
        )

    __repr__ = __str__

    @property
    def length(self):
        """:obj:`float`: Get the length of the line segment."""
        return self.v.length

    def _check_intersect(self, s):
        """Check that a particular intersection is valid for this type."""
        # Intersection is valid if it occurs within the length of the vector
        if s is None or s < 0 or s > 1:
            return False
        else:
            return True

    def _adjust_t(self, t):
        """Adjust a value of t to account for non-infinite lines."""
        return min(1, max(0, t))

    def extend(self, length, reverse=False):
        """Get this line segment extended.

        Args:
            length (:obj:`float`): The length to add on
            reverse (:obj:`bool`, optional): True to extend back from
                the start point

        Returns:
            :obj:`LineSegment`

        """
        if reverse:
            p1 = self.p1 - length * self.v.unit()
            p2 = self.p2
        else:
            p1 = self.p1
            p2 = self.p2 + length * self.v.unit()
        return self.__class__(p1, p2)

    def reverse(self):
        """Get this line segment reversed.

        Returns:
            :obj:`LineSegment`

        """
        return self.__class__(self.p2, self.p1)

    def euclidify(self):
        """Convert this to a :obj:`euclid3.LineSegment2`.

        Returns:
            :obj:`euclid3.LineSegment2`

        """
        return eu.LineSegment2(self.p1.euclidify(), self.p2.euclidify())


class Polygon:
    """2-D polygon.

    Args:
        vertices (:obj:`array` of :obj:`Point`): A list of vertex points

    Attributes:
        n (:obj:`int`): Number of vertices/sides
        vertices (:obj:`array` of :obj:`Point`): The vertices of the polygon
            (ordered in either direction around the polygon)
        sides (:obj:`array` of :obj:`LineSegment`): A list of
            :obj:`LineSegment` objects defining the sides of the polygon
        centre (:obj:`Point`): The centre of the polygon

    """

    def __init__(self, vertices):
        """Create the :obj:`Polygon`."""
        self.vertices = [Point(v) for v in vertices]
        self.n = len(self.vertices)
        self.sides = []
        for i in range(self.n):
            self.sides.append(LineSegment(
                self.vertices[i],
                self.vertices[(i + 1) % len(self.vertices)]
            ))
        self.centre = Point(sum([v._v for v in self.vertices]) / self.n)

    def intersect(self, line):
        """Find any intersections between this polygon and a line.

        Args:
            line (:obj:`Line` or subclass): Line to intersect with

        Returns:
            :obj:`array` of :obj:`Point`

        """
        intersects = [s.intersect(line) for s in self.sides]
        return [i for i in intersects if i is not None]

    def contains(self, point):
        """Check if this point is within the polygon.

        Args:
            point (:obj:`Point`)

        Returns:
            :obj:`bool`: ``True`` if ``point`` inside the polygon

        """
        if not isinstance(point, Point):
            point = Point(point.x, point.y)
        ray = Ray(point, (1, 0))
        return False if len(self.intersect(ray)) % 2 == 0 else True

    def _check_regular_angles(self, angle):
        """Check whether all sides are regularly separated."""
        for i in range(self.n):
            v1 = self.sides[i].v.unit()
            v2 = self.sides[(i + 1) % self.n].v.unit()
            if abs(v1.angle(v2) - angle) > ANGLE_TOL:
                return False
        return True


class Rectangle(Polygon):
    """2-D rectangle.

    Args:
        vertices (:obj:`array` of :obj:`Point`, `length = 4`): Vertices
            passed in to :obj:`Polygon` and checked to be corners
            of a rectangle
        check (:obj:`bool`): True to check that this is a rectangl

    """

    def __init__(self, vertices, check=False):
        """Create the :obj:`Rectangle`."""
        if len(vertices) != 4:
            raise ValueError(
                'Incorrect number of vertices ({})'.format(len(vertices))
            )
        super().__init__(self._sort_vertices([Point(v) for v in vertices]))
        if check and not self._check_regular_angles(np.pi / 2):
            raise ValueError(
                'Two sides are not at right angles'
            )
        self._sorted_sides = sorted(self.sides, key=lambda s: s.length)

    def _sort_vertices(self, vertices):
        """Sort the vertices into order around the rectangle."""
        v1 = vertices[0]
        options = sorted(
            [(i, LineSegment(v1, vertices[i])) for i in range(1, 4)],
            key=lambda s: s[1].length
        )
        return [v1] + [vertices[options[i][0]] for i in [0, 2, 1]]

    @classmethod
    def from_line(cls, line, width):
        """Create a rectangle from a line.

        Args:
            line (:obj:`LineSegment`): Centre line of the rectangle
            width (:obj:`float`): Width distributed either side of ``line``

        Returns:
            :obj:`Rectangle`

        """
        v1 = line.v.perp().unit() * width * 0.5
        v2 = line.v.perp().unit() * width * 0.5
        return cls([line.p1 + v1, line.p1 - v1, line.p2 - v2, line.p2 + v2])

    @property
    def long_side(self):
        """:obj:`Vector`: Get the direction of the long sides.

        Arbitrary direction of the longer sides of the rectangle.
        Undefined behaviour if this is a square.

        """
        return self.sides[-1].v

    @property
    def short_side(self):
        """:obj:`Vector`: Get the direction of the short sides.

        Arbitrary direction of the shorter sides of the rectangle.
        Undefined behaviour if this is a square.
        """
        return self.sides[0].v

    def get_opposite_corners(self):
        """Get a pair of non-adjacent corners.

        Returns:
            :obj:`tuple` of :obj:`Point`

        """
        return self.vertices[0], self.vertices[2]
