"""Contour detection and analysis."""

__author__ = 'Oli Rose'

import logging
from collections import namedtuple

import cv2
import numpy as np

from idp13.detect import vectors as vec


logger = logging.getLogger('idp13.{}'.format(__name__))
try:
    logger.setLevel(logging.VERBOSE)
except AttributeError:
    logger.setLevel(logging.DEBUG)
    logger.verbose = logger.debug

Rect = namedtuple('Rect', ['x', 'y', 'w', 'h'])


def find_contours(img):
    """Find the contours of an image."""
    logger.verbose('Finding contours')
    return cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]


def find_contours_hierarchy(img):
    """Find the contours including hierarchy."""
    logger.verbose('Finding contours and hierarchy')
    c, h = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    if h is None:
        return ContourHierarchy(c, None)
    else:
        return ContourHierarchy(c, h[0])


class _Node:
    """Represent a node in the hierarchy."""

    def __init__(self, n, contour, children=None, parent=None, siblings=None):
        """Create the tree."""
        self.n = n
        self.contour = contour
        self.area = cv2.contourArea(self.contour)
        self.peri = cv2.arcLength(self.contour, True)
        m = cv2.moments(self.contour)
        try:
            self.centre = vec.Point(int(m['m10'] / m['m00']),
                                    int(m['m01'] / m['m00']))
        except ZeroDivisionError:
            self.centre = None
        self.children = children or []
        self.parent = parent
        self.siblings = siblings or []

    @property
    def stacked(self):
        """Return the number of layers stacked below this node."""
        if self.children == []:
            return 0
        else:
            return max([c.stacked for c in self.children]) + 1

    @property
    def direct_children(self):
        """Return the number of direct children of this node."""
        return len(self.children)

    @property
    def total_children(self):
        """Return the total number of children below this node."""
        if self.children == []:
            return 0
        else:
            return (sum([c.total_children for c in self.children])
                    + len(self.children))


class ContourHierarchy:
    """Analyse a hierarchy of contours."""

    def __init__(self, contours, h):
        """Store and process the hierarchy."""
        self.h = h
        self.nodes = []
        if h is not None:
            for i, c in enumerate(h):
                self.nodes.append(_Node(i, contours[i]))
            for i, node in enumerate(self.nodes):
                children = [self.nodes[j] for j in self._find_children(i)]
                for child in children:
                    child.parent = node
                    siblings = children.copy()
                    siblings.remove(child)
                    child.siblings.extend(siblings)
                node.children.extend(children)

    def _find_children(self, n):
        """Find all the children of a certain contour."""
        if self.h[n][2] == -1:
            return []
        else:
            return self._find_siblings(self.h[n][2])

    def _find_siblings(self, n):
        """Find the siblings of a contour."""
        siblings = set([n])
        k = n
        while self.h[k][0] != -1:
            k = self.h[k][0]
            siblings.add(k)
        k = n
        while self.h[k][1] != -1:
            k = self.h[k][1]
            siblings.add(k)
        return siblings


class CircleContours:
    """Circular contours."""

    def __init__(self, img=None, **kwargs):
        """Create the contour object."""
        self.img = img
        self.centres = None
        if img is not None:
            self.find(**kwargs)

    def find(self, img=None, min_size=None, max_size=None):
        """Find the contours and store in the object."""
        img = img if img is not None else self.img
        self.contours = find_contours(img)
        self.centres = []
        logger.verbose('Finding circle contours')
        for c in self.contours:
            m = cv2.moments(c)
            cx = int(m['m10'] / m['m00'])
            cy = int(m['m01'] / m['m00'])
            logger.verbose('    contour size: {}'.format(m['m00']))
            if min_size is not None and m['m00'] < min_size:
                logger.verbose('    centre discarded (too small)')
            elif max_size is not None and m['m00'] > max_size:
                logger.verbose('    centre discarded (too large)')
            else:
                logger.verbose('    centre found: ({}, {})'.format(cx, cy))
                self.centres.append(vec.Point(cx, cy))
        return self.centres

    def draw(self, img=None, centres=None, sz=10,
             colour=(255, 0, 0), thickness=2):
        """Draw the circles on to image."""
        img = img if img is not None else self.img
        centres = centres or self.centres
        if centres is None:
            logger.verbose('No circles to draw')
        else:
            logger.verbose('Drawing circle contour centres on image')
            try:
                for c in centres:
                    cv2.circle(img, (int(c.x), int(c.y)),
                               sz, colour, thickness)
            except (AttributeError, TypeError):
                cv2.circle(img, (int(centres.x), int(centres.y)),
                           sz, colour, thickness)
        return img


class RectangleContours:
    """Rectangular contours."""

    def __init__(self, img=None, **kwargs):
        """Create the contour object."""
        self.img = img
        self.rects = None
        if img is not None:
            self.find(**kwargs)

    def find(self, img=None, n=None, **kwargs):
        """Find the contours."""
        img = img if img is not None else self.img
        self.contours = find_contours(img)
        self.rects = []
        logger.verbose('Finding rectangular contours')
        for c in self.contours:
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.04 * peri, True)
            rect = cv2.boundingRect(approx)
            logger.verbose('    rectangle found: {}'.format(rect))
            self.rects.append(self._create_rect(rect))
        if n is not None:
            self.rects = self.rects[:n]
        return self.rects

    def _create_rect(self, r):
        """Create a namedtuple rect."""
        return Rect(x=r[0], y=r[1], w=r[2], h=r[3])

    def _get_corners(self, r):
        """Get the corner points from a namedtuple rect."""
        try:
            return (int(r.x), int(r.y)), (int(r.x + r.w), int(r.y + r.h))
        except AttributeError:
            return ((int(r[0][0]), int(r[0][1])), (int(r[1][0]), int(r[1][1])))

    def draw(self, img=None, rects=None, colour=(0, 255, 0), thickness=2):
        """Draw the rectangles on to image."""
        img = img if img is not None else self.img
        rects = rects or self.rects
        if rects is None:
            logger.verbose('No rectangles to draw')
        else:
            try:
                for r in rects:
                    cv2.rectangle(img, *self._get_corners(r),
                                  color=colour, thickness=thickness)
                logger.verbose('Drawing rectangle contours on image')
            except (AttributeError, TypeError):
                cv2.rectangle(img, *self._get_corners(rects),
                              color=colour, thickness=thickness)
                logger.verbose('Drawing rectangle contour on image')
        return img


class LineContours:
    """Line contours."""

    def __init__(self, img=None, **kwargs):
        """Create the line detection object."""
        self.img = img
        self.lines = None
        if img is not None:
            self.find(**kwargs)

    def find(self, n=10, img=None, theta=np.pi/45, thresh=90):
        """Find the lines."""
        img = img if img is not None else self.img
        lines = cv2.HoughLines(img, 1, theta, thresh)
        self.lines = []
        logger.verbose('Finding lines')
        if lines is not None:
            for c in lines[:n]:
                for r, t in c:
                    a = np.cos(t)
                    b = np.sin(t)
                    x0 = a * r
                    y0 = b * r
                    x0 = int(a*r - 1000*b)
                    y0 = int(b*r + 1000*a)
                    x1 = int(a*r + 1000*b)
                    y1 = int(b*r - 1000*a)
                    logger.verbose('    line found: '
                                   '({}, {}), ({}, {})'.format(x0, y0, x1, y1))
                    self.lines.append(vec.Line(
                        vec.Point(x0, y0), vec.Point(x1, y1)
                    ))
        return self.lines

    def draw(self, img=None, lines=None, colour=(0, 0, 255), thickness=2):
        """Draw the lines on to image."""
        img = img if img is not None else self.img
        lines = lines or self.lines
        if lines is None:
            logger.verbose('No lines to draw')
        else:
            if not isinstance(lines, list):
                lines = [lines]
            for c in lines:
                p1 = c.p
                p2 = c.p + c.v
                cv2.line(
                    img, (int(p1[0]), int(p1[1])), (int(p2[0]), int(p2[1])),
                    colour, thickness
                )
            logger.verbose('Drawing line on image')
        return img
