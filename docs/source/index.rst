Code structure and algorithms
=============================

.. toctree::
   :maxdepth: 1

   structure

API documentation
=================

.. toctree::
   :maxdepth: 1

   idp13
   arduino

Indices and tables
==================
 
* :ref:`genindex`
* :ref:`modindex`
