idp13.control package
=====================

.. automodule:: idp13.control
    :members:
    :undoc-members:
    :inherited-members:
    :show-inheritance:

Submodules
----------

.. toctree::

   idp13.control.control
   idp13.control.gui
