idp13.detect package
====================

.. automodule:: idp13.detect
    :members:
    :undoc-members:
    :inherited-members:
    :show-inheritance:

Submodules
----------

.. toctree::

   idp13.detect.arena
   idp13.detect.camera
   idp13.detect.contours
   idp13.detect.data
   idp13.detect.images
   idp13.detect.markers
   idp13.detect.robot
   idp13.detect.vectors

