idp13.control.gui module
========================

.. argparse::
    :ref: idp13.utils.get_parser
    :prog: gui

.. automodule:: idp13.control.gui
    :members:
    :undoc-members:
    :inherited-members:
    :show-inheritance:
