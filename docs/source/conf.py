"""Configuration file for the Sphinx documentation builder."""

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import os
import subprocess
import sys

sys.path.insert(0, os.path.abspath('../..'))


# -- Project information -----------------------------------------------------

project = 'idp13'
copyright = '2019, IDP Team 13'
author = 'IDP Team 13'

# The short X.Y version
version = ''
# The full version, including alpha/beta/rc tags
release = '0.0'

# -- General configuration ---------------------------------------------------

extensions = [
    'breathe',
    'sphinxarg.ext',
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx.ext.todo',
    'sphinx.ext.viewcode',
    # 'sphinxcontrib.apidoc',
]

templates_path = ['_templates']

source_suffix = '.rst'
master_doc = 'index'

language = None
exclude_patterns = []

pygments_style = None

# -- Options for HTML output -------------------------------------------------

html_theme = 'sphinx_rtd_theme'
html_theme_options = {
    'canonical_url': '',
    'logo_only': False,
    'display_version': True,
    'prev_next_buttons_location': 'bottom',
    'style_external_links': False,
    # Toc options
    'collapse_navigation': False,
    'sticky_navigation': True,
    'navigation_depth': -1,
    'includehidden': True,
    'titles_only': True,
}

html_static_path = ['_static']

# -- Options for HTMLHelp output ---------------------------------------------

htmlhelp_basename = 'idp13doc'

# -- Options for manual page output ------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'idp13', 'idp13 Documentation',
     [author], 1)
]

# -- Extension configuration -------------------------------------------------

# apidoc_module_dir = '../../idp13'
# apidoc_output_dir = ''
# apidoc_excluded_paths = [
#     'comms/message_pb2.py',
#     'comms/nanopb_pb2.py',
#     'control/control.py',
#     'control/gui.py',
# ]
# apidoc_separate_modules = True
# apidoc_module_first = True
# os.environ['SPHINX_APIDOC_OPTIONS'] = (
#     'members,undoc-members,inherited-members,show-inheritance'
# )

autodoc_member_order = 'bysource'
autodoc_mock_imports = [
    'configparser',
    'cv2',
    'idp13.comms.message_pb2',
]

breathe_projects = {'idp13': '../xml'}
breathe_default_project = 'idp13'
breathe_default_members = ('members', 'undoc-members')

subprocess.call('doxygen ../Doxyfile', shell=True)

# -- Options for todo extension ----------------------------------------------

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True
