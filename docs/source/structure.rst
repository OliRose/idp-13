Code structure and algorithms
=============================

Setup and detection
-------------------

**Arena detection**

The arena layout is mainly hard-coded in order to speed up each run of the robot. The cells are detected using OpenCV. The first frame from the camera is converted to HSV and then filtered based on the hue value. The OpenCV contour detection is then used on this thresholded image and the contours are each checked based on area.

**Robot detection**

The robot detection is performed using three black and white markers on top. The markers were specifically designed to take into account the changes in lighting, the camera resolution, the cable that might end up over the robot and the fact that part of the arena was unseen to the camera. To find the markers in the image a threshold is applied and the contours detected as for detecting the cells. The contour hierarchy is sorted based on the known characteristics of the markers (number of child contours, areas and perimeters of contours). Each marker has a central bar showing its direction and a number of small circles (1, 2 or 3) to show which way it is pointing. The software knows the placement and direction of each marker so can calculate the robot position if any number of markers are detected.

**Pathfinder**

The path finder identifies a good path for the robot to follow to sweep up all of the cells. It begins by collecting those on the known line (collecting any that require only a minor detour as well). It then recursively searches for the minimum path required to collect those remaining (taking the width of the robot funnel into account) and to return to the drop-off zone. It also ensures that the robot doesn't get too close to the red line or to the edges of the camera view. The final cell collection has a risk that it could push an active cell across the line, so, if necessary, a path is planned to keep it away.

Main Python program
-------------------

The main Python program is a run on a PC. It uses a number of threads, each using queues with a max length of 1 for thread safe communication. Each thread is setup to run in a loop each 0.01 seconds in order to give time for the other threads to operate.

+-----------------+-----------------------------------------------------------------+
| GUI             | | The graphical user interface is used                          |
|                 | | to display various data.                                      |
+-----------------+-----------------------------------------------------------------+
| Control         | | The control thread handles the main control of the program    |
| Thread          | | including running the initial detection and path finding,     |
|                 | | spawning the other threads and working through the strategy.  |
|                 | | When the path is completed or when five minutes has passed    |
|                 | | since the first run began, this thread will cancel any        |
|                 | | further strategy and return the robot to home. A route is     |
|                 | | planned to take the robot through the drop-off zone and       |
|                 | | then return to its initial location.                          |
+-----------------+-----------------------------------------------------------------+
| Camera          | | The camera thread simply stores the latest frame read from    |
| Thread          | | the camera into a queue for the robot detection to be         |
|                 | | performed on.                                                 |
+-----------------+-----------------------------------------------------------------+
| Robot           | | The robot thread uses the latest frame from the camera and    |
| Thread          | | finds the robot in that frame. The position and direction     |
|                 | | are then added to a queue.                                    |
+-----------------+-----------------------------------------------------------------+
| Path            | | This thread processes the latest robot detection data and     |
| Follower        | | uses a PID controller to determine the necessary inputs to    |
| Thread          | | guide the robot to the target point. This thread ends when    |
|                 | | the robot reaches its destination                             |
+-----------------+-----------------------------------------------------------------+
| Link            | | The outputs from the path follower or from the main control   |
| Thread          | | thread are passed to the Arduino using this thread. The       |
|                 | | communication with the Arduino is implemented using a         |
|                 | | protocol buffers definition (see *messsage.proto*)            |
+-----------------+-----------------------------------------------------------------+

Arduino
-------

The Arduino code processes messages from the Python code and also handles the LEDs and cell detection, identification and processing.