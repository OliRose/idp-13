idp13 package
=============

.. automodule:: idp13
    :members:
    :undoc-members:
    :inherited-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    idp13.comms
    idp13.control
    idp13.detect
    idp13.strategy

Submodules
----------

.. toctree::

   idp13.utils

